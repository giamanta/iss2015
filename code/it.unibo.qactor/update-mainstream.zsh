#!/bin/zsh

ask() {
	echo -n "$@ [y/n] " ; read ans
	case "$ans" in
		y*|Y*) return 0 ;;
	*) return 1 ;;
esac
}

update-mainstream(){

	local prefix="[i] "
	local YN
	local files
	local f
	local arg
  local labstud="/data/uni/LM/ISSw/labstudISSDISI/it.unibo.qactor/"

	if (( ! $# )); then
		echo "Usage: $0 [file1 file2 ... fileN]"
		return 1
	fi

	for arg in "$@"; do
    # files should be "src/.."
		files=("${(f)$(< $arg)}")

		for f in $files; do
      # echo "$prefix normal diff ($f) ($labstud$f).."
			# diff -u $f $labstud$f
      # echo "$prefix diff without space ($f) ($labstud$f)"
			diff -w -u -E -B $f $labstud$f > $f.patch
      # TODO check if file is empty
      #if test $(du $f.patch|awk "{print $0}") -ne 0 ;then
        vim $f.patch
        if ask "$prefix Apply?";then
          patch $f < $f.patch
        fi
      #fi
      rm -fr $f.patch
		done
	done

}

update-mainstream "$@"
