package it.unibo.contactEvent.platform;

import java.util.Calendar;

public class ActionTimer {
	private long tStart  	= 0;
	private long tStop   	= 0;
	private int exectime    = 0;
	
	public static ActionTimer newTimer(){
		ActionTimer timer = new ActionTimer();
		timer.startTimer();
		return timer;
		
	}
	public void startTimer(){
		tStart = Calendar.getInstance().getTimeInMillis();		
		tStop  = 0;
	}
	public void stopTimer(){
		tStop = Calendar.getInstance().getTimeInMillis();	
	}
	public int getElapsedTime(  ) throws Exception{
		if( tStop==0 ) throw new Exception("timer not stopped");
		exectime = (int) (tStop-tStart);
		return exectime; 
	}
	public  String getElapsedTimeRep(   ){
  		long hr = 0;
 		long min = 0;
 		long sec = 0;
 		long msec = 0;
 		if( exectime < 1000  ){
 			msec = exectime;
  		}else if( exectime < 60 * 1000  ) {
  			sec = exectime / 1000 ;
  			msec = exectime - sec * 1000;
  		}
 		return exectime + "|" + hr+":"+min+":"+sec+":"+msec;		
	}
}
