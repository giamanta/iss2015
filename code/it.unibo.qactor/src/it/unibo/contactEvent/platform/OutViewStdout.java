package it.unibo.contactEvent.platform;
import it.unibo.is.interfaces.IOutputView;

public class OutViewStdout implements IOutputView{
private String outMmsg;
	@Override
	public String getCurVal() {
 		return outMmsg;
	}
	@Override
	public synchronized void addOutput(String msg) {
		outMmsg=msg;
		System.out.println(outMmsg);
		
	}
	@Override
	public synchronized void setOutput(String msg) {
		outMmsg=msg;
		System.out.println(outMmsg);
	}
}