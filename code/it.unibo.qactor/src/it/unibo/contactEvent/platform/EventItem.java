package it.unibo.contactEvent.platform;
import java.util.Hashtable;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.contactEvent.interfaces.ILocalTime;

public class EventItem implements IEventItem{
protected String eventId ;
protected String subj ;
protected ILocalTime time ;
protected String msg;
protected Hashtable<String, Object> at;
protected Struct msgStruct;

	public EventItem(String eventId, String msg, ILocalTime time, String subjId) throws Exception{
		//this(eventId,  msg,  time,  subjId, null );
		
		this("msg( EVID, MSGTYPE, SENDER, RECEIVER, MSG, TIME )".
				replace("EVID", eventId).
				replace("MSGTYPE", "event").
				replace("SENDER", subjId).
				replace("RECEIVER", "none").
				replace("MSG", msg).
				replace("TIME", Long.toString(time.getTheTime()) )		//TODO
				);
		
 	}
	
//	public EventItem(String eventId, String msg, ILocalTime time, String subjId, Hashtable<String, Object> at)  throws Exception{
//		this.eventId = eventId;
//		this.msg = msg ;
//		this.time = time;
//		this.subj = subjId;
//		this.at   = at;	
//	}
	public EventItem(String rep) throws Exception{
		//rep = "msg( EVID, MSGTYPE, SENDER, RECIVER, MSG, TIME )"
//  		System.out.println("		... EventItem " + rep);
  		msgStruct = (Struct) Term.createTerm(rep);
		this.eventId   = msgStruct.getArg(0).toString();
  		this.msg       = msgStruct.getArg(4).toString();//.replaceAll("'", "");
   		this.subj      = msgStruct.getArg(2).toString();
 		String timeStr = msgStruct.getArg(5).toString();
  		 try{
 			this.time    = new LocalTime( Integer.parseInt( timeStr ));
 		 }catch(Exception e){
 			this.time    = new LocalTime( 0 );
 		 }
  	}
	
	public String getEventId(){
		return eventId;
	}	
 	public String getMsg(){
		return msg;
	}
	public String getSubj(){
		return subj;
	}	
	public ILocalTime getTime(){
		return time;
	}
	public String getPrologRep(){
//		//msg( MSGID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM )
//		String timeStr = ( time != null ) ? ""+time.getTheTime() : "notime";
//		String msgPrologRep = "";
// 		if( msg.contains("'")){
// 			System.out.println("		... msg= " + msg);
// 			msgPrologRep = msg.replace("'", "\"");
//		}else 
//		msgPrologRep=msg;
//  		if( ! ( msg.startsWith("'") || msg.startsWith("\"") ) ) msgPrologRep = "'" + msgPrologRep + "'";
//		String	sout = "msg(" + eventId + "," + "event ," + subj  + ", none "+ "," + msgPrologRep + ",'" + timeStr + "')" ;
////		System.out.println("		... sout= " + sout);
//		 return sout;
		return msgStruct.toString();
	}	
	public String getDefaultRep(){
		Term t = Term.createTerm(getPrologRep());
//		System.out.println("		msgRep= " + t.toString());
		return t.toString();
//		String timeStr = ( time != null ) ? time.getTimeRep() : "notime";
//		String msgRep = "";
//		if(  msg.startsWith("'")  )  msgRep = msg.substring( 0,msg.length() );
//		else msgRep = msg;
//		System.out.println("		msgRep= " + msgRep);
//		return "msg(" + eventId + "," + "event," + subj  + ", none "+ "," + msgRep + ",'" + timeStr + "')" ;
	}

	@Override
	public Hashtable<String, Object> getArgTable() {
		return at;
	}
 }
