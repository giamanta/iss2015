package it.unibo.contactEvent.platform;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IMessage;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.system.SituatedSysKb;

/*
 * --------------------------------------------------------------------------------------------------
 * A TaskComponent FSM is always activated (via resume) by the associated TaskQueueEventHandler
 * The TaskQueueEventHandler reacts to the inputSymbolEventsSet of the FSM by resuming it
 * it it is NOT activated.
 * If the FSM is already activated the TaskQueueEventHandler simply stores the events 
 * (symbols of the inputSymbolEventsSet) a local queue.
 * The events stored in the local queue of the TaskQueueEventHandler has consumed by the
 * FSM during the transitions phase
 * 
 * CONSTRAINTS
 * The FSm must specify the transitions (nextStateSet) for the complete inputSymbolEventsSet
 * --------------------------------------------------------------------------------------------------
 */ 
public abstract class TaskComponent extends EventAbstractComponent {
protected TaskQueueEventHandler tqevh = null;
protected String[] inputSymbolEventsSet; 
protected String[] curguardsSet; 
protected String[] expectedInputSet = null;
protected String[] nextStateSet = null;
protected IEventItem interruptEvent = null;
protected boolean endOfCreation = false;
protected IEventItem currentEvent;
 
	public TaskComponent(
			String name, ActorContext myctx,  String[] inputSymbolEventsSet, IOutputEnvView view) throws Exception  {
		super(name, myctx, view);
		this.inputSymbolEventsSet=inputSymbolEventsSet;
		myctx.registerContactComponent(name, this) ;
		//create an handler to store the events of the task input set
		tqevh = new TaskQueueEventHandler( inputSymbolEventsSet, this, myctx, "tqevh", outEnvView);
  		isActivated=true;
//  		println("			TaskComponent "  + getName() + " CREATED ");
	}
  	@Override
	protected void activateTheActor(){
  		boolean b = this.isActivated();
//		println("		TaskComponent in MANY_thread:_scheduler:" + this.getName() + (b?":resumed":":activated") );	
		activate(SituatedSysKb.executorManyThread);
	}
	
//	protected void registerForInput(){
////		println("			TaskComponent "  + getName() + " registerForInput ");
//		for( int j=0; j<inputSymbolEventsSet.length; j++ ){
// 			println("			TaskComponent "  + getName() + " registerForInput " + inputSymbolEventsSet[j] );
//			platform.registerForEvent( getName(), inputSymbolEventsSet[j] ); 
//		}		
//	}
  	/*
	 * Entry point of the task (when (re)activated )
	 */
	@Override
	public void doJob() throws Exception {
		//Register the component for events
		while( ! endOfCreation ){
//			println("			TaskComponent "  + getName() + " waits for end of creation ");
			Thread.sleep(100);
		}
//  		println("			TaskComponent "  + getName() + " RUNNING " + this.curstate + " isActivated=" + isActivated() );
 		if(  isTerminated() ) return;
 		isStarted = true;
 		/*
 		 * The FSM continues to work while there are events to process
 		 * The TaskQueueEventHandler (like any handler) runs with priority with respect to a Task
 		 */
 		while( isActivated ) stateControl();
 		/*
 		 * Now the task is in a "waiting" state.
 		 * It will be resumed by the TaskQueueEventHandler when a new event 
 		 * of its inputSymbolEventsSet coours
 		 */
 		this.isActivated = false;
	}	
	
	/*
	 * To be defined at application level
	 */
	protected abstract void stateControl( ) throws Exception ;
	/*
	 * State transition for an input event
	 * While computing the transitions, the task does not perform any resume
	 */	 
	protected synchronized void transitions(String[] guards, String[] evId, String[] nextState ) throws Exception{
//  		println("			TaskComponen " + getName() + " transitions eventN= " +  evId.length + " nextStateN=" + nextState.length + " guardN=" + guards.length);
//		println("			TaskComponent " + getName() + " transitions curstate= " + curstate );
		curguardsSet 		  = guards;
 		expectedInputSet      = evId;
		this.nextStateSet     = nextState;
		//Check
		for( int i=0; i < evId.length; i++ ){
			boolean evisthefirst = onFirstEventQuery( evId[i] );
// 			println("			TaskComponen " + getName() + " transitions event " + 
//			  evId[i] + " " + nextState[i] + " guard=" + curguardsSet[i] + " evisthefirst="+evisthefirst);
			if(  evisthefirst  ){				
 				if( evalTheGuard(curguardsSet[i]) != null ){
					curstate     = nextState[i]; 
 					currentEvent = tqevh.getFirstEvent( );
//     				println("			TaskComponent TRANSIT during transitions at " + curstate + " for " + evId[i] );    				
//      				isActivated = true; //already true ??
 					return ; 
				}
			}
		}//for
//    		println("			TaskComponent " + getName() + " transitions; the FSM remains in curstate= " + curstate );
		this.isActivated = false;
	}
	

		
 	protected void transitions(String[] evId, String[] nextState ) throws Exception{
// 		println("			TaskComponent transitions event " +  evId.length + " " + nextState.length  );
		int n = evId.length;
 		String[] guards = new String[n];
 			for( int i=0; i<n; i++){
 				guards[i] = "!?true";
 			}
 			transitions(guards,evId,nextState );
 	}
 
	
	protected boolean onFirstEventQuery(String evId )throws Exception{
 	 	if( tqevh == null ) 	return false;
 		boolean b  = tqevh.checkFirstEvent( evId );
//  		println("			TaskComponent onFirstEventQuery checkEvH evId=" +  evId  + " b=" + b   );
  		return b ;
 	}
	
	@Override
	public boolean isPassive() { return true;	}
 
//	@Override
 	public synchronized void terminate() throws Exception {
//		println("			TaskComponent " + getName() + " TERMINATES "  );  
 		actorTerminated  = true;   
 		isActivated      = false;
  		notifyAll();
 	}
 	
 	/*
 	 * return true when has found something to do
 	 * returns false when the system iss inconsistent
 	 */
 	protected boolean skipEventUntilExpected( IEventItem ev ){
//		println("			TaskComponent " + getName() + " skipEventUntilExpected EVENT=" + (ev!=null?ev.getEventId():ev)    );
 		if( nextStateSet == null ){ //initial state
//			println("			TaskComponent initial state " + ev.getEventId() + " curstate =" + curstate  );
 			return true;
 		}
 			/*
 			 * consume the first event that should be equals to ev
 			 */
 		    IEventItem firstEv = tqevh.getFirstEvent( );
//		    	println("			TaskComponent skipEventUntilExpected firstEv= " + (firstEv!=null?firstEv.getEventId():ev)    );
 		    if( firstEv==null || firstEv != ev ){
 		    	println("			TaskComponent " + getName() + " WARNING!!! " + (ev!=null?ev.getEventId():ev) + " does not match with firstEv "  +  (firstEv!=null?firstEv.getEventId():ev)  );
 		    }
// 		    else{
// 		    	println("			TaskComponent !!!   firstEv= "  +firstEv.getDefaultRep()  );
// 		    }
 			int eventpos  = -1 ;
			for( int i=0; i < expectedInputSet.length; i++ ){
				if( expectedInputSet[i].equals(ev.getEventId()) ){
					eventpos = i;
					break;
				}
			}//for
			if( eventpos < 0 ){
//				println("			TaskComponent " + getName() + " WARNING inconsistent for  " + ev.getEventId()     );
	 			curstate = "error"; 
	 			return false;
			}else{
				curstate = nextStateSet[eventpos]; 
//  				println("			TaskComponent " + getName() + " skipEventUntilExpected has found " + ev.getEventId() + " curstate =" + curstate  );
				return true;
			}
 	}
 
	/*
	 * A task is resumed only when there is an input belonginh to inputSymbolEventsSet
	 * and it is in the current expectedInput set
	 * 
	 * While computing the resume , the task does not perform any transitions
	 */
	@Override
	public synchronized void resume( IEventItem ev )   {
//  		println("			TaskComponent " + getName() + "  resuming for " + 
//  				(ev!=null?ev.getEventId():ev) + " curstate=" + this.curstate + " isActivated=" + this.isActivated);
 		if( ev == null ) return ; // 
		/*
		 * ev must belong to the expectedInputSet and must be the first in the queue
		 */
		try{
			currentEvent = ev;
			if( skipEventUntilExpected( ev )  ){
				isActivated=true;
				this.activateTheActor();
			}
 		}catch(Exception e){
			e.printStackTrace();
		}
	}

/*
 * Called by handleFSMOutput of QActor	
 */
	public IEventItem getEventItem( ) {
		return currentEvent;
  	}
	
 
	public void showMessage(String comment, IMessage m) {
		if (m != null) {
			String msgEmitter = m.msgEmitter();
			String msgId = m.msgId();
			String msgContent = m.msgContent();
			String msgNum = m.msgNum();
			showMsg(this.getName() + " " + comment + ": " + m + "\n\t"
					+ "msgEmitter=" + msgEmitter + " msgId=" + msgId
					+ " msgContent=" + msgContent + " msgNum=" + msgNum);
		}
	}
	
	/*
	 * Intyroduced to debug
	 */
	protected String getActionRep(){
 		return "noaction";
	}

}
