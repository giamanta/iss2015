package it.unibo.contactEvent.platform;

import java.util.Hashtable;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.contactEvent.interfaces.ILocalTime;

public class EventItemVC implements IEventItem {
	protected String eventId;
	protected String subj;
	protected ILocalTime time;
	protected String msg;
	protected Hashtable<String, Object> at;
	protected Struct msgStruct;

	public EventItemVC(String rep) throws Exception {
		msgStruct = (Struct) Term.createTerm(rep);
		this.eventId = msgStruct.getArg(0).toString();
		this.msg = msgStruct.getArg(4).toString();
		this.subj = msgStruct.getArg(2).toString();
		String timeStr = msgStruct.getArg(5).toString().replace("'", "");
		try {
			this.time = new LocalTimeVC(Long.parseLong(timeStr, 16));
		} catch (Exception e) {
			this.time = new LocalTimeVC(0);
		}
	}

	public EventItemVC(String eventId, String msg, ILocalTime time, String subjId) throws Exception {
		this("msg( EVID, MSGTYPE, SENDER, RECEIVER, MSG, TIME )"
				.replace("EVID", eventId)
				.replace("MSGTYPE", "event")
				.replace("SENDER", subjId)
				.replace("RECEIVER", "none")
				.replace("MSG", msg)
				.replace("TIME", time.getTimeRep()));
	}

	@Override
	public String getEventId() {
		return this.eventId;
	}

	@Override
	public String getSubj() {
		return this.subj;
	}

	@Override
	public ILocalTime getTime() {
		return this.time;
	}

	@Override
	public String getMsg() {
		return this.msg;
	}

	@Override
	public String getPrologRep() {
		return msgStruct.toString();
	}

	@Override
	public String getDefaultRep() {
		Term t = Term.createTerm(getPrologRep());
		return t.toString();
	}

	@Override
	public Hashtable<String, Object> getArgTable() {
		return this.at;
	}

}
