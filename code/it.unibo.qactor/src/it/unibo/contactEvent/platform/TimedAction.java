package it.unibo.contactEvent.platform;
import java.util.Calendar;
import it.unibo.contactEvent.interfaces.IContactEventPlatform;
import it.unibo.iot.interfaces.ITimedAction;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.system.SituatedActiveObject;
import it.unibo.system.SituatedSysKb;

/*
 * A TimedAction is an active entity that executes an activity that must terminate
 * At the end of its execution a TimedAction emits the given event named terminationEvId 
 */
public abstract class TimedAction extends SituatedActiveObject  implements ITimedAction {
	protected IContactEventPlatform platform ;
	protected String terminationEvId;
	protected long tStart  = 0;
	protected long durationMillis;
	
	public TimedAction(String name, String terminationEvId, IOutputView outView) throws Exception {
		super( outView, name );
 		this.terminationEvId = terminationEvId;
		platform = ContactEventPlatform.getPlatform();
//		println("			%%% TimedAction " + name + " terminationEvId=" + terminationEvId   );
  	}
 
	protected  void activate() throws Exception{
//		this.run();  //DOES NOT WORK
		System.out.println("%%%TimedAction " + getName() + " STARTED"     );
 		SituatedSysKb.executorManyThread.submit(this);	//  .execute(this);		
	}
	@Override
	protected void startWork() throws Exception {}
	@Override
	protected void doJob() throws Exception {
		tStart = Calendar.getInstance().getTimeInMillis();
// 		println("			%%% TimedAction tStart " + TimerExecution.getTimeRep(tStart) + " terminationEvId=" + terminationEvId);		
		execAction();
		emitEvent();
	}
	protected void emitEvent(){
		long tEnd = Calendar.getInstance().getTimeInMillis();
		durationMillis =  tEnd - tStart ;	
//		println("			%%% TimedAction raiseEvent " + terminationEvId 
//				+ " exectime=" + TimerExecution.getTimeRep(durationMillis) );
  		platform.raiseEvent(getName(), terminationEvId, ""+durationMillis );		
	}
	@Override
	protected void endWork() throws Exception {}
	@Override
	public abstract void execAction() throws Exception; 
	@Override
	public long getExecTime() {
 		return durationMillis;
	}
	@Override
	public String getTerminationEventId() {
 		return terminationEvId;
	}
}
