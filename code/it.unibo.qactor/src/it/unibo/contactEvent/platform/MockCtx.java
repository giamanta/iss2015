package it.unibo.contactEvent.platform;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.qactors.ActorContext;

import java.io.InputStream;

public class MockCtx extends ActorContext{
 	
	public MockCtx(String name, IOutputEnvView outEnvView, InputStream inputStream, InputStream sysRulesStream ) throws Exception {
		super(name, outEnvView, inputStream, sysRulesStream );
  	}

	@Override
	public void configure() {
 	}

}
