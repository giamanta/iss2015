package it.unibo.contactEvent.platform;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;
import it.unibo.contactEvent.interfaces.IContactComponent;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.contactEvent.interfaces.ILocalTime;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.QActor;
 

public class EventLoopActor extends QActor {

	public static ILocalTime localTime = new LocalTime(0);	
	private IOutputView outView = EventPlatformKb.stdOutView;
 	private Vector<IEventItem> eventWaitQueue = new Vector<IEventItem>();
   	private boolean debug = false;
	private long tStart = 0;
//  	private IContactComponent curActiveSubj = null;
  	private IOutputView myOutView ;
  	
	public EventLoopActor(String actorId, ActorContext ctx, IOutputEnvView outEnvView) {
		super(actorId, ctx, outEnvView);
		myOutView = myCtx.getOutputView();
 	}
	
	@Override
//	public void preStart() {
	public void startWork() {
//		trace("preStart  ");
		tStart = Calendar.getInstance().getTimeInMillis();
	}
	@Override
	protected void doJob() throws Exception {
		myprintln("			%%% EventLoopActor " +  getName() + " STARTS "  );
		while(true){
			String msg = receiveMsg();
			onReceive(msg);
		}
 	}
	@Override
	//public void postStop() {
	public void endWork(){
		try {
			trace("postStop ");
//  			System.exit(0);			
		} catch( Exception e) {
			e.printStackTrace();
		}
 	}
	public void insertInEventWaitQueue(String subj, IEventItem ev)   {		
		try {
			if( subj == null || ev == null ) return;
			String evId = ev.getMsg().replace("'", "") ;
			//outView.addOutput("EventLoopActor insertInEventWaitQueue subj=" + subj  );
			//msg( MSGID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM )
			String wevStr = "msg(EV,event,SUBJ,noDest,noContent,0)".replace("SUBJ", subj ).replace("EV", evId);
// 			myOutView.addOutput("EventLoopActor insertInEventWaitQueue wevStr=" + wevStr );
 			if( ! isInEventWaitQueue(subj, evId ) ){
//				outView.addOutput("			%%% EventLoopActor insertInEventWaitQueue " + subj  + " for " + wevStr 
//							+ " | queesize=" + eventWaitQueue.size() );
				eventWaitQueue.add( new EventItem(wevStr) );
 			}
		} catch (Exception e) {
			myOutView.addOutput("			%%% EventLoopActor insertInEventWaitQueue ERROR " + e.getMessage() );
		}	 
		
	}
 	protected boolean isInEventWaitQueue(String subj, String evId) {
		Iterator<IEventItem> iterEWQ = eventWaitQueue.iterator();
		while (iterEWQ.hasNext()) {
			IEventItem curEv = iterEWQ.next();
			if (curEv.getEventId().equals(evId) && curEv.getSubj().equals( subj ) )
				return true;
		}
		return false;
	}
	public void removeFromEventWaitQueue(String subj, IEventItem ev) throws Exception {
		String evId = ev.getMsg().replace("'", "") ;
		IEventItem evitFound = null;
		Iterator<IEventItem> iter = eventWaitQueue.iterator();
		while (iter.hasNext()) {
			IEventItem evit = iter.next();
			if (evit.getEventId().equals(evId) && evit.getSubj() == subj) {
				evitFound = evit;
				break;
			}
		}
		if (evitFound != null){
//			outView.addOutput("EventLoopActor removeFromEventWaitQueue " + evitFound.getSubj().getName() + " for " + evId
//					+ " | queesize=" + eventWaitQueue.size() );
 			eventWaitQueue.removeElement(evitFound);
 		}
	}
	public void removeFromEventWaitQueue(String subj) throws Exception {
		Vector<IEventItem> evitFound = new Vector<IEventItem>();
		Iterator<IEventItem> iter = eventWaitQueue.iterator();
		while (iter.hasNext()) {
			IEventItem evit = iter.next();
			if ( evit.getSubj().equals( subj ) ) {
				evitFound.add( evit );
 			}
		}
//		if( evitFound.size() > 0 )
// 		outView.addOutput("			%%% EventLoopActor removeFromEventWaitQueueAll " + subj.getName() + " " + evitFound.size() );
		Iterator<IEventItem> itFound = evitFound.iterator();
		while (itFound.hasNext()){
			IEventItem toremove = itFound.next();
//			outView.addOutput("			%%% EventLoopActor removeFromEventWaitQueueAll " + toremove.getSubj().getName() + " for " + toremove.getEventId()
//					+ " | queesize=" + eventWaitQueue.size() );
 			eventWaitQueue.removeElement(toremove);
 		}
	}
 
//	@Override
	protected void onReceive(Object msg) throws Exception {
		localTime = new LocalTime( localTime.getTheTime()+1 );
// 		println("			%%% EventLoopActor onReceive : "+ msg);
		if(  msg instanceof String) {
			IEventItem ev = new EventItem( (String) msg);
			elab( ev );
		}else if (msg instanceof IEventItem){
			elab( (IEventItem)msg );
		}
	}
	
	protected void elab(IEventItem event) throws Exception{
// 		println( "			%%% EventLoopActor elab evId="  +  event.getEventId()  + "|" + event.getMsg() + "|" + event.getSubj() );
 		if( event.getEventId().equals(EventPlatformKb.endOfJob)){
			return;
		}
		//activate
		if( event.getEventId().equals( EventPlatformKb.activate) ){
 			IContactComponent subj = myCtx.getContactComponent(  event.getSubj() ) ;
// 			myprintln("			%%% EventLoopActor activates "  +  subj.getName()  );
 			EventPlatformKb.oneThreadExecutor.submit( (Runnable) subj );
			return;
		}
		//register
		if( event.getEventId().equals( EventPlatformKb.register) ){
			insertInEventWaitQueue(event.getSubj(), event  );
 			return;
		}
		//unregister
		if( event.getEventId().equals( EventPlatformKb.unregister) ){
			removeFromEventWaitQueue(event.getSubj(), event );
			return;
		}
		if( event.getEventId().equals( EventPlatformKb.unregisterAll) ){
			removeFromEventWaitQueue( event.getSubj()  );
			return;
		}
		//event => update local time
		elabEventWaitQueue(event);
		
	}

	
	protected void elabEventWaitQueue(IEventItem curEvent) throws Exception {
//		int nmsg = 0;
		myprintln( "			%%% EventLoopActor elabEventWaitQueue evId="  +  curEvent.getEventId()  + "|" + eventWaitQueue.size() );
		Iterator<IEventItem> iterEWQ = eventWaitQueue.iterator();
		while (iterEWQ.hasNext()) {
			IEventItem curWaiting = iterEWQ.next();
//			nmsg++;
//			println( "			%%% EventLoopActor " + nmsg + " curWaiting="  +  curWaiting.getSubj() + "|" + eventWaitQueue.size() );
			if (curWaiting.getEventId().equals(curEvent.getEventId())) {
//				println( "			%%% EventLoopActor evId="  +  curEvent.getEventId() + " RESUMING curWaiting for "  +  curWaiting.getEventId()  + "=" +   curWaiting.getSubj() );
 				IContactComponent subj = myCtx.getContactComponent(  curWaiting.getSubj()  ) ;
				if( subj == null ) return;
//  				println( "			%%% EventLoopActor evId="  +  curEvent.getEventId() + " RESUMING subj=" +  subj.getName() + " " + subj.isTerminated() );
 				if( ! subj.isTerminated()  ){
// 					if( subj instanceof EventHandlerComponent){
//  						println( "			%%% EventLoopActor ACTIVATES "  +  curWaiting.getSubj() + " curEvent=" + curEvent.getEventId() );						
						subj.resume(curEvent);
						/*
						 * Activate the subject
						 */
//						this.platform.activate( subj.getName() );
  						EventPlatformKb.oneThreadExecutor.submit(  (Thread)subj );
 					}
//					else if( ! subj.isActivated() ){ //if subj is running does not resume
//						subj.resume(curEvent);
// 						//EventPlatformKb.oneThreadExecutor.execute(  (Thread)subj );
//					}
 				}
// 			}
		}	
	}//elabEventWaitQueue

	/*
	 * UTILITY
	 */
	protected void loadWorldTheory() throws Exception{
		//does not load nay worldtheory
	}
	protected void trace(String msg){
		if(debug) outView.addOutput("%%% EventLoopActor: "+ msg);
	}
	protected void showExecutionTime(String msg, long tEnd) {
		long exectime = tEnd - tStart;
		outView.addOutput( getTraceMsg(msg +  EventPlatformKb.getTime(exectime)  ));
	}

	protected String getTraceMsg( String msg  ) {
		String qs = "";
 			qs =  " eventWaitQ("
					+ eventWaitQueue.size() + ")"  ;
		return "> " + msg + qs;
	}

	protected void myprintln( String msg ){
// 		myOutView.addOutput(msg);
	}
	
}
