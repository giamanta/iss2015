package it.unibo.contactEvent.platform;

import it.unibo.contactEvent.interfaces.IContactComponent;

public interface IContactEventComponent extends IContactComponent{
	public boolean isStared();
}
