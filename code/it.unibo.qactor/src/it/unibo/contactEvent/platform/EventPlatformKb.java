package it.unibo.contactEvent.platform;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.OutViewStdout;
import it.unibo.system.StandardOutEnvView;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
 

public class EventPlatformKb {
	public static final IOutputView stdOutView             = new OutViewStdout();
	public static final IOutputEnvView standardOutEnvView  = new StandardOutEnvView();
	private static ActorContext parentCtx       		   = null; //set by ContactPlatform
	public static final ScheduledExecutorService  oneThreadExecutor = Executors.newScheduledThreadPool(1);
//  	public static final ScheduledExecutorService  executor 	= Executors.newScheduledThreadPool(8);

	public static void setParentContext( ActorContext ctx )  {
		parentCtx = ctx;
	}
	public static ActorContext getParentContext( )  {
  		return parentCtx;
 	}	
	public static final String endOfJob 		= "endOfJob";
	public static final String activate 		= "activate";
	public static final String register 		= "register";
	public static final String unregister 	    = "unregister";
	public static final String unregisterAll 	= "unregisterAll";
	public static final String raiseEvent  		 = "raiseEvent";
 
	public static final String sensorEvent   = "sensorEvent";
	public static final String streamEvent   = "streamEvent";
	public static final String endOfStream   = "endOfStream";
 	
 	public static String getTime(long exectime) { 
 		long hr = 0;
		long min = 0;
		long sec = 0;
		long msec = 0;
		if (exectime < 1000) {
			msec = exectime;
		} else if (exectime < 60 * 1000) {
			sec = exectime / 1000;
			msec = exectime - sec * 1000;
		}
		return exectime + "|" + hr + ":" + min + ":" + sec + ":" + msec;
	}
	
}
