package it.unibo.contactEvent.platform;
import java.util.Vector;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
/*
 * The plaform calls the resume operation each time an expected event is detected
 */
public abstract class EventHandlerComponent extends EventAbstractComponent {
protected Vector<IEventItem> curEventItems = new Vector<IEventItem>();
protected Vector<IEventItem> curPriorityEventItems = new Vector<IEventItem>();

	public EventHandlerComponent( String name, ActorContext myctx, IOutputEnvView view ) { 
 		super(name,myctx,view); 
 	} 
 	public EventHandlerComponent( String name,  ActorContext myctx, String eventId, IOutputEnvView view ) throws Exception { 
 		super(name,myctx,view); 
    	myctx.registerContactComponent(name, this) ; 
 		platform.registerForEvent(name, eventId);
 		this.isStarted = true;
 	}
 	public EventHandlerComponent( String name, ActorContext myctx, String[] events, IOutputEnvView view ) throws Exception { 
 		super(name,myctx,view); 
  		myctx.registerContactComponent(name, this) ; 
		for( int i=0; i<events.length; i++){
			if( events[i].trim().length() > 0 ){
// 				println("EventHandlerComponent " + name + " register " + events[i] );
				platform.registerForEvent( name, events[i] );
			}
		}
 		this.isStarted = true;
 	}
 	@Override
 	public abstract void doJob() throws Exception;
 	@Override
  	public boolean isPassive(){return true;}
 
 	@Override
 	public void resume(IEventItem ev){ 
//  		println("=== EventHandlerComponent " + getName() +" RESUMING for " + ev.getEventId()  );  
  		curEventItems.add(ev);  
   	}  		
	protected IEventItem getEventItem() throws Exception{
		if( curEventItems.size() == 0 ) return null; //throw new Exception("no event to handle");
		else return curEventItems.remove(0);
	}
	protected void loadWorldTheory() throws Exception{
		//WE DO NOT LOAD TEH WORLD THEORY in a EventHandler
	}
   	@Override
	public void terminate() throws Exception {
   		println("EventHandlerComponent" + getName() + " SHOULD NOT TERMINATE "  ); 
//   		actorTerminated = true;
   	}
 }
