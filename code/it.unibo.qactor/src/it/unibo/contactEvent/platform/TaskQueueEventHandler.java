package it.unibo.contactEvent.platform;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import java.util.Vector;
  
public class TaskQueueEventHandler extends  EventHandlerComponent{
protected IContactEventComponent mySubj;
protected Vector<String> myevents = new Vector<String>();
 
	public TaskQueueEventHandler(  
			String[] events, IContactEventComponent subj, ActorContext ctx, String name,  IOutputEnvView view ) throws Exception {
		super(name+"_"+subj.getName() , ctx, events, view);
 		mySubj = subj;
 		for( int i=0; i<events.length; i++){
 			myevents.add( events[i] );
		}
  	}
	@Override
	public void doJob() throws Exception {
		//This handler must be used by a task (via getEvent)
//		println("			=== TaskQueueEventHandler " + getName() + " doJob curEventItems.size=" + curEventItems.size());
//		if( curEventItems.size() == 0 ) return;
//		if( mySubj.isStared() && ! mySubj.isActivated() ){
//	  		mySubj.resume( getFirstEventNoRemove() );   
// 		}
	}
	protected void loadWorldTheory() throws Exception{ 
//		println("TaskQueueEventHandler " + getName() + " does noy load loadWorldTheory");
	}   
	public synchronized boolean checkFirstEvent( String evId ){ //called by  TaskComponent
		if(curEventItems.size() > 0) {
			IEventItem ev = curEventItems.firstElement();
//			println("checkFirstEvent " + evId + " while first is " + ev.getEventId() );
			return(  ev.getEventId().equals(evId) );			
		}else return false;
 	}
	public IEventItem getFirstEvent(   ){ 
		if( curEventItems.size() > 0   ){
			return curEventItems.remove(0);	
		} else return null;
	}
	public IEventItem getFirstEventNoRemove(   ){  
		if( curEventItems.size() > 0   ){
			return curEventItems.elementAt( 0 );	
		} else return null;
	}
	
	@Override
 	public  void resume(IEventItem ev){  
 		if(mySubj.isTerminated()) return;
//   		println("			=== TaskQueueEventHandler " + getName() + " resume for: " + (ev!=null?ev.getEventId():ev) + 
//   				" paired with=" + mySubj.getName() + " isActivated=" + mySubj.isActivated() );
// 		println("			=== TaskQueueEventHandler " + getName() + " resume for: " + (ev!=null?ev.getEventId():ev) );
// 		addWithPriority( ev );
   		curEventItems.add(ev);
   		//Resume immediately (more efficient than delegating to dojob()
		if( mySubj.isStared() && ! mySubj.isActivated() ){
//			println("			=== TaskQueueEventHandler " + getName() + " resumes " + mySubj.getName() +
//					" event=" + getFirstEventNoRemove().getDefaultRep() );
	  		mySubj.resume( getFirstEventNoRemove() );   
 		}
   	}  	
	
	protected void addWithPriority( IEventItem ev ){
		if(isAPriorityEvent(ev)){
			if( curEventItems.size() == 0 ) {
				curEventItems.add(ev);
				return;
			}
			for( int i=0; i < curEventItems.size(); i++){
				IEventItem curEv = curEventItems.elementAt( i );
				if( ! isAPriorityEvent(curEv) ){
					curEventItems.add(i, ev);
					break;
				}
			}//for
		}else{
			curEventItems.add(ev);
		}
	}
	protected boolean isAPriorityEvent(IEventItem ev){
 		return ( ev.getEventId().contains("alarm") );
	}

	protected void outMsg(String msg){
 		showMsg( "��� " +getName()+ " for " + mySubj.getName() + " " + msg );
	}
}
