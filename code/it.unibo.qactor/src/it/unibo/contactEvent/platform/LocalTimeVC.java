package it.unibo.contactEvent.platform;

import java.util.Calendar;

import it.unibo.contactEvent.interfaces.ILocalTime;

public class LocalTimeVC implements ILocalTime {
	protected long time = 0;

	public LocalTimeVC() {
		this(Calendar.getInstance().getTimeInMillis());
	}

	public LocalTimeVC(long time) {
		try {
			setTime(time);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void setTime(long time) throws Exception {
		if (time < 0)
			throw new Exception("[LocalTimeVC] negative time not allowed");
		else
			this.time = time;
	}

	@Override
	public long getTheTime() {
		return this.time;
	}

	@Override
	public String getTimeRep() {
		return "'" + Long.toHexString(this.time) + "'";
	}

}
