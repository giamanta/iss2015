package it.unibo.contactEvent.platform;
import java.util.Hashtable;
import java.util.Iterator;

import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;

import it.unibo.contactEvent.interfaces.IContactEventPlatform;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.contactEvent.interfaces.ILocalTime;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.OutViewStdout;
import it.unibo.qactors.QActor;
import it.unibo.qactors.SenderObject;
 

public class ContactEventPlatform implements IContactEventPlatform{
	private static ContactEventPlatform myself = null; // To implement a singleton
	public static final IOutputView stdOutView = new OutViewStdout();
	public static final String locEvPrefix   ="local_";
//	public static final String locEvPrefix = locEvPrefix ;
 	protected Hashtable<String,QActor> eventLoopActor = null;
  	private ActorContext ctx;
  
	public static  IContactEventPlatform getPlatform( ) throws Exception{
		 if( myself == null ){
			 throw new Exception("No platform available");
 		 }
		 else return myself;
	}
	public static  IContactEventPlatform getPlatform(ActorContext ctx){
		 if( myself == null ){
 		 	myself  = new ContactEventPlatform(ctx);
		 }
		 return myself;
	}
	public static void reset(){
		myself = null;
	}
	public ActorContext getActorContext(){
		return ctx;
	}
	private ContactEventPlatform(ActorContext ctx){
  			this.ctx = ctx;
  			eventLoopActor = new Hashtable<String,QActor>();
    }
	/* 
	 * Several actors could call this operation "at the same time"
	 */
	private synchronized void startEventLoopActor(){
		if(  eventLoopActor.get(ctx.getName()) == null ){
			EventLoopActor evlpa = new EventLoopActor("evlpa"+ctx.getName(), ctx, EventPlatformKb.standardOutEnvView);	
			eventLoopActor.put( ctx.getName(), evlpa) ;
			//Check
//			QActor dest = ctx.getActor("evlpa"+ ctx.getName());
//			ctx.getOutputView().addOutput("### ContactEventPlatform registered "  + dest.getName()  );
 		}
	}
	
	@Override //IContactEventPlatform
	public ILocalTime getLocalTime() {
		return EventLoopActor.localTime;
	}
 
	@Override
	public  void activate(String subj){
		startEventLoopActor();
 		IEventItem msg = buildEvent( EventPlatformKb.activate, "activate_"+subj, subj );
		eventLoopActor.get(ctx.getName()).storeMsg( msg.getPrologRep()  );
	}
	@Override
	public void registerForEvent(String subj, String ev) {		 
		startEventLoopActor();
		IEventItem msg = buildEvent( EventPlatformKb.register, ev, subj);
 		eventLoopActor.get(ctx.getName()).storeMsg( msg.getPrologRep() );
	}
	@Override
	public void unregisterForEvent(String subj, String ev){
 		IEventItem msg = buildEvent(  EventPlatformKb.unregister, ev,   subj);
 		eventLoopActor.get(ctx.getName()).storeMsg(  msg.getPrologRep() );
	}
	@Override
	public void unregisterForAllEvents( String subj ){
 		IEventItem msg = buildEvent(  EventPlatformKb.unregisterAll, "nomsg",   subj);
 		eventLoopActor.get(ctx.getName()).storeMsg(  msg.getPrologRep() );
	}
	/*
	 * ONE AT THE TIME
 	 */
	@Override
	public synchronized void raiseEvent(String emitter, String evId, String evContent) {
//		raiseEvent( emitter,  evId,  evContent, null );
		startEventLoopActor();
		//ctx.getOutputView().addOutput("ContactEventPlatform raiseEvent: " + evId + " | " + evContent);
		try {
	  		IEventItem ev = buildEvent(  evId, evContent,  emitter );
	  		String evRep = ev.getPrologRep();
		  		//ctx.getOutputView().addOutput("ContactEventPlatform raiseEvent evRep: " + evRep );
	  		//Raise the event locally and to all the other contexts
				eventLoopActor.get(ctx.getName()).storeMsg( evRep );
			if( ! evId.startsWith(locEvPrefix)) propagateEvent(evRep);
		} catch (Exception e) {
			ctx.getOutputView().addOutput("ContactEventPlatform ERROR" + e.getMessage() );
		}
	}
	 
//	@Override
//	public void raiseEvent(String emitter, String evId, String evContent, Hashtable<String, Object> at) {
//		startEventLoopActor();
//  		//ctx.getOutputView().addOutput("ContactEventPlatform raiseEvent: " + evId + " | " + evContent);
// 		try {
//	  		IEventItem ev = buildEvent(  evId, evContent,  emitter, at);
//	  		String evRep = ev.getPrologRep();
//   	  		//ctx.getOutputView().addOutput("ContactEventPlatform raiseEvent evRep: " + evRep );
//	  		//Raise the event locally and to all the other contexts
// 			eventLoopActor.get(ctx.getName()).storeMsg( evRep );
//			if( ! evId.startsWith(locEvPrefix)) propagateEvent(evRep);
//		} catch (Exception e) {
//			ctx.getOutputView().addOutput("ContactEventPlatform ERROR" + e.getMessage() );
//		}
//		
//	}
	
	protected void propagateEvent(String evRep) throws Exception{
// 	    ctx.getOutputView().addOutput("ContactEventPlatform propagateEvent evRep: " + evRep );
		Prolog prologEngine = ctx.getPrologEngine();
		SolveInfo sol   = prologEngine.solve("getCtxNames( CTXNAMES ).");	
		Struct ctxList  = (Struct) sol.getVarValue("CTXNAMES");
		Iterator<? extends Term> it = ctxList.listIterator();
		while( it.hasNext() ){
			String curOtherCtx      = ""+it.next();
			SenderObject sa   = ctx.getSenderAgent("evlpa"+curOtherCtx);
			if( sa != null ) {
//				ctx.getOutputView().addOutput("ContactEventPlatform propagateEvent FOUND sa for " + "evlpa"+curOtherCtx + " " + evRep);
				sa.sendMsg(evRep);
			}
		}
	}
	protected IEventItem buildEvent( String evId, String evContent, String subj ){
  		try {
			return new EventItemVC( evId, evContent, new LocalTimeVC() , subj );
		} catch (Exception e) {
 			e.printStackTrace();
 			return null;
		}
	}
//	protected IEventItem buildEvent( String evId, String evContent, String subj, Hashtable<String, Object> at ){
//  		try {
//			return new EventItem( evId, evContent, EventLoopActor.localTime , subj, at );
//		} catch (Exception e) {
// 			e.printStackTrace();
//			return null;
//		}
//	}
}
