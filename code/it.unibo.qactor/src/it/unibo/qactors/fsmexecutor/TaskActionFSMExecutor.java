package it.unibo.qactors.fsmexecutor;
import java.util.Hashtable;

import it.unibo.contactEvent.platform.TaskComponent;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.action.IActorAction.ActionExecMode;
import it.unibo.qactors.action.IActorAction.ActionRunMode;
import it.unibo.qactors.action.TaskActionFSMExecutoResult;

public class TaskActionFSMExecutor extends TaskComponent{
// 	private QActor aactor ;
	private String endOfActionEvent = null; 
 	private IActorAction action = null;
	private long moveTime;
	private String[] myalarms ;
  	private String[] alarms ;
 	private String[] eventPlans;
 	private Boolean terminatedAction = null;
 	private TimerExecution timerMove ;
 	private long moveTimeNotDone ;
 	private String planTodo;		
 	private	String[] guards;
 	private	String[] events;
 	private String[] nextStates;
 	private String stateForNomalEnd = "cmdExecEnd";
 	private Hashtable<String,StateAction> actionTab = null;

 	protected static String[] createEventNameList(String name,String[] alarms, IActorAction action){
 		if( alarms.length==1 && alarms[0].trim().length()==0) return new String[0];
   		String[] vsa = new String[1+alarms.length];
		for( int i=0; i<alarms.length; i++ ) vsa[i]=alarms[i];
  		vsa[alarms.length]   = action.getTerminationEventId();
//  		System.out.println("createEventNameList ... " + vsa[vsa.length-1] );
 		return vsa;
 	}
 	
 	/*
 	 *  The action can be interrupted by myalarms
 	 *  and each alarm must put in execution the corresponding element in eventPlans
 	 */ 
 	public TaskActionFSMExecutor(String name, ActorContext ctx, IOutputEnvView outView, 
			IActorAction action, long moveTime, 
			String[] myalarms, String[] eventPlans ) throws Exception {
		super(name, ctx, createEventNameList(name,myalarms, action) , outView);
// 		this.aactor     = actor;
		this.action 	= action;
		this.moveTime 	= moveTime;
		this.myalarms   = myalarms;
		this.eventPlans = eventPlans;
   		initForActorAction();
		/*
		 * SET A FLEAG (don't forget)
		 */
		endOfCreation   = true;
//		activateTheActor();
	}

	protected void loadWorldTheory() throws Exception{
		//We do not load any world theory
	}
 	
	@Override
	protected void stateControl() throws Exception {
// 		if( actionTab == null  ) initForActorAction();
// 		println("&&& TaskActionFSMExecutor stateControl curstate:" +  curstate + " isTerminated=" + isTerminated() + " isActivated=" + isActivated);
 		if( ! this.isTerminated() ){	//NO MORE WHILE		
			if( curstate.equals("cmdExecInit")){ 
				cmdExecInit();  
			}else if( curstate.equals(stateForNomalEnd) ){ 
 				terminate();
 			}else{
				StateAction stateAction = actionTab.get(curstate);
				if(stateAction!=null) {
// 	 				println("&&& TaskActionFSMExecutor stateControl urstate:" +  curstate + " stateAction:" +  stateAction.toString());
					stateAction.call(); 
					isActivated=false;
				}
			}
 		}else{
 			isActivated=false;
 		}
 	}
 	
	/*
	 * For each alarm  event we must create a state 
	 * Each state is represented by an object (StateAction) put in a table
	 * When the alarm event occurs, stateControl is called that in its turn calls the StateAction
	 * The StateAction is like a callback (endOfAction) related to the current FSM 
	 */
	protected void initForActorAction(  ){
//  		println("%%% TaskActionExecutor initForActorAction " + action.getActionName() + " " + myalarms.length +"/" + eventPlans.length);
		alarms    = inputSymbolEventsSet;
   		curstate  = "cmdExecInit";    			
		actionTab = new Hashtable<String,StateAction>();
    	for( int i=0; i < myalarms.length; i++){
    		if(  myalarms.length == 1 && eventPlans.length == 0  ) //react when event
    			actionTab.put( myalarms[i], new StateAction(action, this, myalarms[i], false,  0, "dummyPlan" ) );  //false for terminating
    		else
    			actionTab.put( myalarms[i], new StateAction(action, this, myalarms[i], false,  0, eventPlans[i] ) );  
    	}
    	if( myalarms.length == 0 ) 
    		actionTab.put(stateForNomalEnd, new StateAction(action, this, stateForNomalEnd, true,  0, "") );//CRITICAL
    	else 
    		/*
    		 * There are alarms: we must interrupt the action when one of them occurs
    		 */
    		actionTab.put(stateForNomalEnd, new StateAction(action, this, stateForNomalEnd, false,  0, "") );
 	}
	
	/*
	 * RETURN:
	 * 0 => command done without "interruptions"
	 * -1 => command interrupted by a superStop
	 * >0 => command interrupted by an event
	 * >0 ha the further answer: planTodo
	 */
	public synchronized long  waitForTemrmination() throws Exception{
 		timerMove = new TimerExecution(moveTime);
//  		println("%%% TaskActionFSMExecutor " + getName() + " waitForTemrmination action=" + action.getActionName() );
 		//we must wait for the termination of the action called in asynch mode
// 		action.getActionAndResultRep();
		while( terminatedAction == null ) wait();
 		action.waitForTermination();  //pleonastic?
// 		println("%%% TaskActionFSMExecutor " + getName() + " waitForTemrmination RESUMES moveTimeNotDone=" + moveTimeNotDone + " action=" + action.getActionName());
 		return moveTimeNotDone;
 	}
	public String getPlanTodo(){
 		return planTodo;
	}
	public long getTimeNotDone(){
		return moveTimeNotDone;
	}
	public TaskActionFSMExecutoResult getResult(){
		return new TaskActionFSMExecutoResult( getEventItem( ), planTodo, this.getTimeNotDone() );
	} 
	protected void cmdExecInit()  throws Exception{
//		First prepare and then call the action (that could generate an event)
		endOfActionEvent = action.getTerminationEventId() ;
		prepareTheTransitions();
//		println("&&& TaskActionFSMExecutor transitions events=" + events.length + " nextStates=" + nextStates.length);
 		/*
		 * --------------------------------------------------
		 * Execute the action starts
		 * --------------------------------------------------
		 */
//  		println("&&& TaskActionFSMExecutor execute the action " + action.getActionRep() + " nextStates=" + nextStates.length ); 
  		action.execASynch(); 
		/*
		 * --------------------------------------------------
		 * Attempt to perform a transition
		 * --------------------------------------------------
		 */
		transitions( guards, events, nextStates );	

 	}
 	protected void prepareTheTransitions() throws Exception{
//  		println("&&& TaskActionFSMExecutor prepareTheTransitions " + this.curstate + " alarmsN=" + alarms.length + " endOfMoveEvent=" + endOfActionEvent);
		if( alarms.length == 0 ){ //no user-defined event
			guards 	   = new String[]{  "true" };
			events 	   = new String[]{  endOfActionEvent };
			nextStates = new String[]{  stateForNomalEnd };	 			
		}else{
			guards 	= new String[myalarms.length+1]; 
			events 	= new String[myalarms.length+1]; 
			//We need a state for each alarm => A GENERATOR or AN OBJECT-BASED FSM
			for( int i=0; i < myalarms.length; i++){
				guards[i] =  "true" ;
				events[i] = myalarms[i];
			}
			guards[myalarms.length] = "true" ;
			events[myalarms.length] = endOfActionEvent;
			
			nextStates = new String[myalarms.length+1];
			//A state for each alarm  
			for( int i=0; i < eventPlans.length; i++){
				nextStates[i] =  myalarms[i];
			}
			nextStates[eventPlans.length] = stateForNomalEnd;
		}
		/*
		 * WE DO NOT REGISTER ...
		 */
//  		for( int i=0; i<events.length; i++ ){
//			platform.registerForEvent( getName(), events[i] ); 
//		}
	}
	public synchronized void endOfAction(String msg, boolean terminated, int dt, String planTodo) throws Exception{
// 		println("&&& TaskActionFSMExecutor " + getName() + " endOfAction for " + action.getActionRep() + " event="+msg + " terminated " + terminated  + " planTodo=" + planTodo );		
		platform.unregisterForAllEvents(this.getName());
 
   		this.planTodo = planTodo;
		if( planTodo.length() > 0 ) {
			action.suspendAction();
			terminatedAction = true;
		}
		else terminatedAction = terminated;
   		if( dt == 0 && timerMove!=null) moveTimeNotDone = timerMove.getRemainingTime();
   		else moveTimeNotDone = dt;
//		notifyAll();
 		if(terminatedAction) 
			terminate();	
	}
 	public synchronized void terminate() throws Exception {
 		terminatedAction = true;
 		super.terminate();
 	}
	
	protected String getActionRep(){
		if( ! terminatedAction)
			return action.getActionRep();
		else return "action terminated";
	}
	


}


