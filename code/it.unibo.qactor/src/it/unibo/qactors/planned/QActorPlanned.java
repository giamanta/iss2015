package it.unibo.qactors.planned;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Theory;
import alice.tuprolog.Var;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.QActor;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.action.AsynchActionResult;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.action.IActorAction.ActionExecMode;

public  class QActorPlanned extends QActor{
//protected Hashtable<String, Vector<PlanActionDescr> > planTable;
protected long previousTime = 0;
protected long resultOfAction = 0;
protected String defaultPlan;
protected Vector<PlanActionDescr> curPlan;
protected int nRepeat = 0;
protected String planFilePath = null;
protected String worldTheoryPath = null;
protected AsynchActionResult lastActionResult;

protected Stack<PlanActionDescr>  actionStack; 

	public QActorPlanned(String actorId, ActorContext myCtx,String planFilePath,String worldTheoryPath,
			IOutputEnvView outEnvView, String defaultPlan) {
		super(actorId, myCtx, outEnvView);
		//WARNING: the actor is activated before the following two statements
		this.defaultPlan     = defaultPlan;
		this.worldTheoryPath = worldTheoryPath;
		this.planFilePath    = planFilePath;
		actionStack       = new Stack<PlanActionDescr>();
		myloadWorldTheory();  
//  		println("		### QActorPlanned  CREATED " + planFilePath  );
 	}

	@Override
	protected  void doJob() throws Exception{
		initThePlan();
		executeThePlan(defaultPlan);		
	}
	/*
	 * ----------------------------------------	
	 * WORLD THEORY  
	 * ----------------------------------------	
	*/  
	@Override
	protected void loadWorldTheory() throws Exception{
		//We avoid the execution of the inherited operation
	 }
	protected void myloadWorldTheory()  {
		try{
//			println("		### QActorPlanned "+ getName() + " loadWorldTheory  : "  + worldTheoryPath);
		   	Theory worldTh = new Theory( new FileInputStream(worldTheoryPath) );
	  		pengine.addTheory(worldTh);
//	  		println(getName() + " loadWorldTheory done "   );	 		
//	 		alice.tuprolog.SolveInfo sol = pengine.solve("test.");
 		}catch( Exception e){
 			println("		### QActorPlanned "+getName() + " loadWorldTheory WARNING: "  + e.getMessage() );
 		}		
	}
	
	protected void initThePlan() throws Exception{
//		println("		### QActorPlanned  STARTS " + planFilePath  );
		while( planFilePath == null ) this.sleep(10);
		buildPlanTable();		
	}
	protected void buildPlanTable() throws Exception{
		try{
// 	 		println("buildPlanTable " + planFilePath);
			planTable = new Hashtable<String, Vector<PlanActionDescr> >();
	  		InputStreamReader isr   	 = openPlanFile();
			BufferedReader bufferedIn    = new BufferedReader(isr);
			String lineStr = bufferedIn.readLine().trim();
			while( lineStr != null){
				if( lineStr.length() > 0 ){
					if( lineStr.startsWith("pa(")  ) {
						PlanActionDescr pa  = PlanActionDescr.createDescr( lineStr );
						String planOfAction = pa.getPlanName();
						insertInPlanTable(planOfAction,pa);
					}
				}
				lineStr = bufferedIn.readLine();
			}	
			bufferedIn.close();
//			System.out.println("buildPlanTable done " + planFilePath);
		}catch(Exception e){
			println("ERROR " + e.getMessage());
//			bufferedIn.close();
			throw e;
		}
	}
	
	protected InputStreamReader openPlanFile() throws Exception{
		println("openPlanFile " );
		planTable = new Hashtable<String, Vector<PlanActionDescr> >();
  		InputStreamReader isr  = new InputStreamReader( new FileInputStream(planFilePath) );
 		return isr;
	}
	
	protected void insertInPlanTable(String planOfAction, PlanActionDescr pa){
		Vector<PlanActionDescr> plan = planTable.get(planOfAction);
		if( plan == null ) {
			plan = new Vector<PlanActionDescr>();
			planTable.put(planOfAction, plan);
		}
// 		System.out.println("		### QActorPlanned added pa=" + pa.getDefStringRep() + " in " + planOfAction);
		plan.add(pa);		
	}
	protected AsynchActionResult executeThePlan( String planName ) throws Exception{	
//		println("		### QActorPlanned executeThePlan " + planName  );
		if( planTable == null )
			return new AsynchActionResult(null,0,normalEnd,continueWork,"",null);
 		curPlan        = planTable.get(planName);
		if( curPlan != null ){
			curPlanInExec  = planName;
//			AsynchActionResult aar = executePlanInterpreted( curPlan );
//			return aar;
			
//TO AVOID RECURSION			
	 		Iterator<PlanActionDescr> it = curPlan.iterator();
			while( it.hasNext() ){
				PlanActionDescr action = it.next();
				lastActionResult = executePlanAction( action );
//  				println("		### QActorPlanned executeThePlan " + action.getCommand() + " lastActionResult= " + lastActionResult );
 				if( ! lastActionResult.getGoon() ){
// 					println("		### QActorPlanned executeThePlan " + planName + " " +action.getCommand() + " breaks"   );
 					return lastActionResult;
 				}
 			}
			return lastActionResult;
		}else return new AsynchActionResult(null,0,normalEnd,continueWork,"",null); //no plan => continue your work (STARNGE)
	}

	/*
	 * defined public to allow Prolog execution
 	 */
	public AsynchActionResult switchToPlan( String planName ) throws Exception{
  		//println("		### QActorPlanned switchToPlan " + planName  );
  		if( planName.equals("dummyPlan") || planName.equals("continue"))
  			 return new AsynchActionResult(null,0,normalEnd,continueWork,"",null ); 
 		planStack.push( curPlanInExec );
		iterStack.push( nPlanIter );
		nPlanIter = 0;
		curPlanInExec = planName ;
		if( planTable != null ){ //interpreted			
//			lastActionResult = executePlanInterpreted(  planTable.get(planName) );
//TO AVOID RECURSIONK			
			
	 		Iterator<PlanActionDescr> it = planTable.get(planName).iterator();
			while( it.hasNext() ){
				PlanActionDescr action = it.next();
				lastActionResult = executePlanAction( action );
// 				println("		### QActorPlanned  " + action.getCommand() + " lastActionResult= " + lastActionResult );
 				if( ! lastActionResult.getGoon() ){
// 					println("		### QActorPlanned switchToPlan " + action.getCommand() + " breaks"   );
 					return lastActionResult;
 				}
 			}
			
			
			
//   			println("		### QActorPlanned switchToPlan " + planName + " ends result=" + lastActionResult);
			if( planStack.size() > 0 ){
				curPlanInExec = planStack.pop();	
				nPlanIter     = iterStack.pop();
			}
 			return lastActionResult;
		}else{ //CODE GEN
 			 boolean goon =  execByReflection(this.getClass(), planName  );
 			if( planStack.size() > 0 ){			
 				curPlanInExec = planStack.pop();	
 	 			nPlanIter     = iterStack.pop();
			}
//			println("		### QActorPlanned switchToPlan " + planName + " ends result=" + lastActionResult);
			 if( goon == suspendWork ){
				 return new AsynchActionResult(null,-1,interrupted,suspendWork,"",null );
			 }else{
// 	  		     println(" --- Entity executeActionWithEvents 1 " + action.getActionName()   );
				 return new AsynchActionResult(null,0,normalEnd,continueWork,"",null );  
			 }
 
		}
		
	}
	protected AsynchActionResult repeatPlan(  int limit  ) throws Exception{
//   		println("		### QActorPlanned repeatPlan " + curPlanInExec +" nPlanIter=" + nPlanIter + "/" + limit  );
		 
		if( nPlanIter <= limit || limit==0 ){
//			nPlanIter = nPlanIter + 1; //DONE BY THE INTERPRETER OR BY THE CODE
			if( planTable != null ){ //interpreted => RECURSION
				AsynchActionResult aar =  executeThePlan(curPlanInExec);
//				println("		### QActorPlanned repeatPlan " + curPlanInExec + " result " + aar);
				return aar ;
			//CODE GEN: we do nothing since the while(true)
			}else{ //planTable == null
//				println("		### QActorPlanned repeatPlan nPlanIter=" + nPlanIter + "/" + limit);
				return new AsynchActionResult(null,0,normalEnd,continueWork,"",null);
			}
		}
		return new AsynchActionResult(null,0,normalEnd,suspendWork,"",null);
 	}
	protected AsynchActionResult resumeLastPlan(    ) throws Exception{
 		return new AsynchActionResult(null,0,normalEnd,continueWork,"",null);
	}
	protected AsynchActionResult interruptPlan(    ) throws Exception{
// 			println("		### QActorPlanned interruptPlan " + curPlanInExec);
 		return new AsynchActionResult(null,0,normalEnd,suspendWork,"",null);
 	}
	protected AsynchActionResult executePlanInterpreted( Vector<PlanActionDescr> plan) throws Exception{
	 		Iterator<PlanActionDescr> it = plan.iterator();
			while( it.hasNext() ){
				PlanActionDescr action = it.next();
				lastActionResult = executePlanAction( action );
// 				println("		### QActorPlanned executePlanInterpreted " + action.getCommand() + " lastActionResult= " + lastActionResult );
 				if( ! lastActionResult.getGoon() ){
  					println("		### QActorPlanned executePlanInterpreted " + action.getCommand() + " breaks"   );
 					return lastActionResult;
 				}
 			}
//			println("		### QActorPlanned executePlanInterpreted RETURNS:" + lastActionResult );
 			return lastActionResult; 
	}
	

	/*
	 * The execution based on an interpreter takes advantage form a PlanActionDescr
	 * In a code-based approach we do not have any plan description but only the code
	 */
	public AsynchActionResult executePlanAction( PlanActionDescr pa) throws Exception{
//   		println("		### QActorPlanned executePlanAction " + pa.getDefStringRep() );
		/*
		 * 1) Guard evaluation.  If the guard is true:
		 * 2) Guard variable bindings
		 * 3) Action execution
		 */
 		Struct gt = (Struct) Term.createTerm(pa.getGuard()); //guard(volatile,domove(M))
 		String guardType = gt.getArg(0).toString();
 		String guardBody = gt.getArg(1).toString();
 		List<Var> guardVars = evalTheGuard(guardBody,guardType);
// 		println("executePlanAction guard=" + guardBody + " of type=" + guardType + " guardVars=" + guardVars );	
		if( guardVars == null ) return new AsynchActionResult(null,0,normalEnd,continueWork,"",null);
		
		bindVars(pa,guardVars);
		
// 		println("executePlanAction guard var list " + guardVars);
  		AsynchActionResult aar = executeAction(pa);
  		//RESTORE original args
  		pa.resetArgs();
    	return aar;
	}
 	
	protected List<Var> evalTheGuard( String guard, String guardType ) throws Exception{
//		println("		### QActorPlanned evalTheGuard=" + guard + " of type=" + guardType );	
		
		if( guardType.equals("event")) {
			AsynchActionResult res = execDummyActionForGuardWait( guard );
//			println("		### QActorPlanned  evalTheGuard event "  );
			//msg(usercmd,event,ex3emitter, none ,'usercmd(  "Hello event"   )','0|0:0:0:0')
 			String msg = currentEvent.getDefaultRep();
			this.addRule(msg);
			SolveInfo sol = pengine.solve("msg(EV, event, EVEMITTER, DEST, BODY, TIME).");
			if( sol.isSuccess() ){
				removeRule(msg);
			}
			return sol.getBindingVars();
		}
// 		println("### QActorPlanned executePlanAction evalTheGuard " + guard  );
 		/*
 		 * If the pengine is engaged in solving a goal: the caller Thread waits
 		 */
		SolveInfo sol = pengine.solve("evalGuard("+guard+").");
//   		println("### QActorPlanned executePlanAction evalTheGuard " + guard + " sol=" + sol);	
		if( sol.isSuccess() ){
			if( guardType.equals(guardVolatile)){
				removeRule(guard);
			}
			return sol.getBindingVars();
		}
		else return null;
	}
	
	protected void bindVars( PlanActionDescr pa, List<Var> guardVars ){
		Iterator<Var> it = guardVars.iterator();
		while( it.hasNext() ){
			Var v=it.next();
			String varName  = v.getOriginalName();
			String varValue = v.getTerm().toString();
// 			println("### QActorPlanned bindVars varOriginaleName=" + varName + " varValue=" + varValue);
 			//SUBSTITUTE
			pa.setInDuration(varName, varValue);
  			pa.setInCommand(varName, varValue);
  			pa.setInArgs(varName, varValue); 			
		}
	}

 
	
/*
* -------------------------------------------
* Execution of basic actions
* -------------------------------------------
*/
		public AsynchActionResult executeAction( PlanActionDescr pa) throws Exception{
//  			println("		### QActorPlanned executeAction  " + pa.getDefStringRep() );
//  		println("		### QActorPlanned executeAction  " + pa.getCommand() );
			String paType   = pa.getType().toString();
			String paCmd 	= pa.getCommand();
			String paArgs   = pa.getArgs();
			
//  			println("		&&& executeAction paType="  + paType + " paCmd="+ paCmd +  " paArgs=" + paArgs ) ;
		try{
			if( paType.equals("solve") ){
// 					println("		&&& solve "  +  paCmd +  " paArgs=" + paArgs ) ;
					int actionMaxTime = Integer.parseInt(pa.getDuration());
					lastActionResult = solveGoal( paCmd,actionMaxTime, "",pa.getEvents(), pa.getPlans() );
// 					println("		&&& solve lastActionResult= "  + lastActionResult.getResult()  + " plans="  + paArgs) ;
					if( lastActionResult.getResult().equals("failure") && paArgs.length() > 0){
 						lastActionResult = switchToPlan(paArgs);
					}
 					return lastActionResult; 
			}
			
			else if( paType.equals( "application" ) ){
//  				println("		### QActorPlanned execute application " + paCmd + " args " + paArgs.length() +" in " + getClass().getName() );
 				int actionMaxTime = Integer.parseInt(pa.getDuration());
 				String arg1, arg2;
 				if( paArgs.length() == 0 ){
 					arg1="";
 					arg2="";
 				}else{
//   	  				println("		### QActorPlanned execute application " + paCmd + " args " + paArgs  );
	 				Struct targs = (Struct) Term.createTerm(paArgs) ;
	 				arg1 = targs.getArg(0).toString();
	 				arg2 = targs.getArg(1).toString();
// 	 				println("		### QActorPlanned execute application " + paCmd + " arg1=" + arg1 + " arg2=" + arg2 );
 				}
 				if( actionMaxTime ==  0 ){
	 				boolean b = execApplicationActionByReflection( getClass(), paCmd, arg1,arg2  );
	 				if( b )
	 					lastActionResult = new AsynchActionResult(null,0,normalEnd,continueWork,"",null);
	 				else
	 					lastActionResult = new AsynchActionResult(null,0,normalEnd,suspendWork,"",null);
	 				return lastActionResult;
 				}else{ //action with time a perhaps react
 					lastActionResult = executeActionAsFSM( new it.unibo.qactors.action.ActionApplication( outEnvView, actionMaxTime , this, 
 							paCmd, arg1,arg2 ), pa.getEvents(), pa.getPlans(), ActionExecMode.synch );
 					return lastActionResult;				
 				}
   			}
			else if( paType.equals( "forward" ) ){
 				//args('msg(mSGID,MSG)')
 				Struct st    = (Struct) Term.createTerm(paArgs);
 				String msgId = st.getArg(0).toString();
 				String msg   = st.getArg(1).toString();
 				if( paCmd.equals("replyToCaller") ) replyToCaller(msgId,msg);
 				else forward( msgId,  paCmd,  msg);
 				lastActionResult = new AsynchActionResult(null,0,normalEnd,continueWork,"",null);
// 				println("		### QActorPlanned forward RESULT= " + lastActionResult );
  	 			return lastActionResult;
 			}
 			else if( paType.equals( "emit" ) ){
// 				println("		### QActorPlanned execute emit " + paCmd + " in " + this.getName() );
				this.platform.raiseEvent(this.getName(), paCmd, paArgs);
				lastActionResult = new AsynchActionResult(null,0,normalEnd,continueWork,"",null);
				return lastActionResult;
 			}
			else if( paType.equals( "basic" ) ){
				if( paCmd.equals( "print" )){
	 				println( paArgs.replace("'", ""));
				}else if( paCmd.equals( "printCurrentEvent" )){ //internal events are ignored
					if(paArgs.equals("memo")) printCurrentEvent(true);
					else printCurrentEvent(false);
				}else if( paCmd.equals( "printCurrentMessage" )){
					if(paArgs.equals("memo")) printCurrentMessage(true);
					else printCurrentMessage(false);
				}else if( paCmd.equals( "memoCurrentEvent" )){ //internal events are ignored
					memoCurrentEvent( );
				}else if( paCmd.equals( "memoCurrentMessage" )){
					memoCurrentMessage();
				}else if( paCmd.equals( "sound" )){
// 					println("		### QActorPlanned  sound "  +  paArgs ) ;
					int time = Integer.parseInt(pa.getDuration());
					//args(answerEvent, fileName )
					Struct argsT = (Struct) Term.createTerm(paArgs); 
					String answerEvent = argsT.getArg(0).toString().replace("'", "");
					String fileName	   = argsT.getArg(1).toString().replace("'", "");
					lastActionResult = playSound(fileName, time, answerEvent, pa.getEvents(), pa.getPlans() );
//					println("		### QActorPlanned sound RESULT= " + lastActionResult );
 					return lastActionResult;
 				}else if( paCmd.equals( "endplan" )){
 					println( paArgs );
 					lastActionResult = new AsynchActionResult(null,0,interrupted,suspendWork,"",null);
 					return lastActionResult ;
  				}else if( paCmd.equals( "switchplan" )){
   					return switchToPlan(paArgs);
 				}else if( paCmd.equals( "repeatplan" )){
 					int limit = Integer.parseInt( paArgs );
 					nPlanIter = nPlanIter + 1; 
 					lastActionResult = repeatPlan( limit );
// 					println("		### QActorPlanned repeatPlan RESULT= " + lastActionResult );
 					return lastActionResult;
 				}else if( paCmd.equals( "resumeplan" )){ 					
 					lastActionResult = this.resumeLastPlan();
//  					println("		### QActorPlanned resumeLastPlan RESULT= " + lastActionResult );
 		 			return lastActionResult;
				}else if( paCmd.equals( "interruptplan" )){					
					lastActionResult = this.interruptPlan();
// 					println("		### QActorPlanned interruptplan RESULT= " + lastActionResult );
 		 			return lastActionResult;
				}else if( paCmd.equals( "addrule" )){
  					this.addRule(paArgs);
 				}else if( paCmd.equals( "removerule" )){
  					this.removeRule(paArgs);					
 				}else if( paCmd.equals( "receiveMsg" )){
 					int actionMaxTime = Integer.parseInt(pa.getDuration());
 					lastActionResult = receiveAMsg( actionMaxTime, "" , "" );
   					return lastActionResult;
 				}else if( paCmd.equals( "receiveTheMsg" )){  
// 					println("		### QActorPlanned execute receiveTheMsg  " + pa.getDefStringRep() );
  					int time = Integer.parseInt(pa.getDuration());
//  					println(" +++ QActorPlanned receiveTheMsg " + paArgs );
 					lastActionResult = receiveMsg( paArgs, time,pa.getEvents(), pa.getPlans());
 					//receiveMsg SET currentMessage using msg(none,none,none,none,none,0) if there is no msg
  					return lastActionResult;
 				}else if( paCmd.equals( "msgselect" )){				
 					lastActionResult = receiveMsgAndSwitch( 
				      		paArgs, pa.getEvents() , pa.getPlans(),
				      		Integer.parseInt( pa.getDuration() )
	         		);
	         		return lastActionResult;
 				}else if( paCmd.equals( "msgswitch" )){
   	 				Struct st             = (Struct) Term.createTerm(paArgs);
 	 				String msgId          = st.getArg(0).toString();
					if( currentMessage.msgId().equals(msgId)){
						Term contentList 	  = st.getArg(1) ;	//should be a list of strings
//	 					println("msgswitch contentList= " + contentList );
	 					Term planList    	  = st.getArg(2) ;
	 					lastActionResult =  msgswitch( contentList,planList);
	 					return lastActionResult;
					}//else println("msgswitch  " + currentMessage.msgId() );
 				}else if( paCmd.equals( "eventswitch" )){
   	 				Struct st             = (Struct) Term.createTerm(paArgs);
 	 				String eventId        = st.getArg(0).toString();
// 					println("eventswitch " + eventId + " " + currentExternalEvent.getEventId());
					if( currentExternalEvent != null && currentExternalEvent.getEventId().equals(eventId)){
						Term contentList 	  = st.getArg(1) ;	//should be a list of strings
	// 					println("eventwitch contentList= " + contentList );
	 					Term planList    	  = st.getArg(2) ;
	 					lastActionResult =  eventswitch( contentList,planList);
	 					return lastActionResult;
					}
 				}else if( paCmd.equals( "senseEvent" )){
 					Struct st = (Struct) Term.createTerm(paArgs);
 					String eventsListStr = st.getArg(0).toString().trim();
 					String events  = eventsListStr.substring(1,eventsListStr.length()-1); //remove []
 					String planListStr = st.getArg(1).toString().trim();
 					String plans = planListStr.substring(1,planListStr.length()-1); //remove []
//					println("senseEvent events=" + events + " plans="+plans + " time= " + pa.getDuration());
 					int tout = Integer.parseInt( pa.getDuration());
 					AsynchActionResult aar = 
 							senseEvents( tout, events,plans, pa.getEvents(), pa.getPlans(),ActionExecMode.synch );
 					return aar;
 				}else if( paCmd.equals( "delay" )){
   					String events = pa.getEvents();
 					String plans  = pa.getPlans();
// 					println("		### QActorPlanned delay events=" + events + " plans="+plans + " time= " + pa.getDuration() );
 					AsynchActionResult aar = delayReactive(Integer.parseInt( pa.getDuration() ),events , plans);
 					return aar;
 				}
			}//basic
			lastActionResult = new AsynchActionResult(null,0,normalEnd,continueWork,"",null);
//			println("		### QActorPlanned " + paCmd + " RESULT= " + lastActionResult );
 			return lastActionResult;
 		}catch(Exception e){
 			println("		### QActorPlanned executeAction ... " + paCmd + " ERROR " + e.getMessage() );
			e.printStackTrace();
			throw e;
		}
		}

/*
 * 	Return the plan to be executed when a message is received from a set of messages
 */
		public AsynchActionResult receiveMsgAndSwitch( String msgselect, String events, String reactplans, int tout  ) throws Exception{
			//msgselect = msgselect( msgs, plass )
// 			println("		### QActorPlanned receiveMsgAndSwitch  msgselect= " + msgselect   );
			Struct st 		  = (Struct) Term.createTerm(msgselect);
			String msgListStr = st.getArg(0).toString().trim();
			String msgs       = msgListStr.substring(1,msgListStr.length()-1); //remove []
			String planList   = st.getArg(1).toString().trim();
			String plans      = planList.substring(1,planList.length()-1); //remove []
			return receiveMsgAndSwitch(tout,msgs,plans,events,reactplans);
		}
		/*
		 * We could receive a msg not included in msgs
		 * We first look in the WorldTheory
		 * If no matching msg is found, we call receiveMsg/4 (that should fined something in the actor queue)
		 */
 	
	 	public AsynchActionResult receiveMsgAndSwitch( int tout, String msgs, String plans, String events, String reactplans) throws Exception{
  	 		AsynchActionResult aar = null;
  	 		boolean msgFound = false;
  	 		/*
  	 		 * First we look at the WordTheory		
  	 		 */
  	 		String msg = checkInWorld(msgs);
  	 		if( msg != null ){
  	 			currentMessage  = new QActorMessage( msg );
  	 			return execThePlan(0, msg, msgs, plans);
  	 		}
  	 		/*
  	 		 * If no msg is found in the WordTheory , we use receiveMsg/4 
  	 		 * that looks (via ActionReceiveAsynch) at the WorldThery but does not find any wanted nmsg!!!
  	 		 */
	 		while( ! msgFound ){
//	 			println("		### QActorPlanned receiveMsgAndSwitch  msgs= " + msgs + " plans=" + plans + " events=" + events + " reactplans=" + reactplans );
//				println("		### QActorPlanned receiveMsgAndSwitch waits for a message ...  "  );
		 		aar = receiveMsg("msg( MID, MSGTYPE, SENDER,"+ getName() +", CONTENT, SEQNUM  )",tout, events, reactplans);
// 		 		println("receiveMsgAndSwitch receiveMsg aar.getEvent=  " + aar.getEvent() );
		 		if( aar.getInterrupted() || aar.getTimeRemained() == 0 ){
		 			/*
		 			 * We must remember to restore messages previously extracted form the WordTheory
		 			 */
		 			restoreTempMsgs();
		 			return aar;
		 		}
				//Here we have received a message
				msg = aar.getResult();
				QActorMessage foundMsg  = new QActorMessage( msg );
				String  curMsgId = foundMsg.msgId();
// 				println("		### QActorPlanned receiveMsgAndSwitch received  " + curMsgId);
				if( ! msgs.contains( curMsgId ) ){
					/*
					 * We have found a message but it is not in the set of
					 * the expected messages, Thus, we store the msg in a temporary structure
					 */
					String mr = "tempmsg("+msg+")";
					//println("		### QActorPlanned store  " + mr);
					addRule( mr );
  				}else{
   					currentMessage  = foundMsg;
 					println("		### QActorPlanned has found not in World " + foundMsg.msgContent());
  					msgFound = true;
		 		}
 	 		}//while
	 		/*
	 		 * We have found an expected msg. 
	 		 * Then we restore messages previously extracted form the WordTheory
	 		 * and execute the plan
	 		 */
	 		restoreTempMsgs();
	 		aar = execThePlan(aar.getTimeRemained(),msg,msgs, plans);//
	 		return aar;
 	 	}
	 	
	 	protected void restoreTempMsgs() throws Exception{
	 		SolveInfo sol ;
 	 		while(true){
	 			sol = pengine.solve( "retract( tempmsg(R) ).");
	 			if( sol.isSuccess() ) { 
	 				Term msg = sol.getVarValue("R");
 	 				println("		### QActorPlanned restoring " + msg);
	 				this.addRule(msg.toString());
	 			}else break;
 	 		} 	 		
	 	}
	 	protected AsynchActionResult execThePlan(long trest, String msg, String msgs, String plans) throws Exception{
 	 		String planToDo  = null;
			Term tmsgs  = Term.createTerm("["+msgs+"]");
			Term tplans = Term.createTerm("["+plans+"]");
// 				println("execThePlan tmsgs  " + tmsgs  + " curMsgId=" + curMsgId );				
			//Check if the received message is in the message set
			planToDo = msgToPlan(msg, tmsgs, tplans );
// 			println("execThePlan planToDo=" + planToDo + " msg=" + msg + " trest=" + trest);
			if( planToDo.equals("dummyPlan")){
				//throw new Exception("receiveMsgAndSwitch "  );
				return null;
			}
 			 //We execute the plan todo
			return executeThePlanTodo(planToDo,trest);	 		
	 	}
	 		 	
	 	/*
	 	 * Check if one of the msgs is in the WorldTheory
	 	 */
	 	protected String checkInWorld( String msgs ) throws Exception{
	 		String[] msga = msgs.split(",");
	 		for( int i=0; i<msga.length; i++){
//	 			println("--- checkAMsgInWorld checking " +  msga[i] );
	 			String mt="msg( MID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM  )".replace("MID", msga[i]);
	 			String b = checkAMsgInWorld( mt );
//	 			println("--- checkAMsgInWorld " + msgs + " found " + b );
	 			if( b != null){
// 		 			println("--- checkAMsgInWorld found in WorldTheory " +  msga[i] );	 				
	 				return b ;
	 			}
	 		} 
	 		return null;
	 	}
		protected String checkAMsgInWorld( String msgTermStrToReceive ) throws Exception{
// 			Term termToReceive         = Term.createTerm(msgTermStrToReceive);
//			println("--- checkAMsgInWorld termToReceive " +  termToReceive );
			SolveInfo sol = pengine.solve(msgTermStrToReceive+".");
//	    	println("---  checkAMsgInWorld " +  sol.isSuccess() );
			if( sol.isSuccess() ){
//				println("---  checkAMsgInWorld removing " +  sol.getSolution() );
				pengine.solve( "removeRule( " + sol.getSolution() + " ).");
  				return sol.getSolution().toString();
			}
			return null;
		}
 	 	
	 	
	 	protected AsynchActionResult executeThePlanTodo(String planToDo, long timeRemained) throws Exception{
			if( this.planTable != null ){
				AsynchActionResult aaar = executeThePlan(planToDo);
				//TOCHECK
				return aaar;  //new AsynchActionResult(null,aar.getTimeRemained(),interrupted,continueWork,"", null);
			}//exec by code
			boolean goon = execByReflection( this.getClass(), planToDo  );
			if( goon == suspendWork ){
				return new AsynchActionResult(null,-1,interrupted,suspendWork,"", null);
			}else{
				return new AsynchActionResult(null,timeRemained,interrupted,continueWork,"", null);  
			}	 		
	 	}
		protected AsynchActionResult msgswitch(  Term contentList, Term planList ) throws Exception{
//  			println("		### QActorPlanned msgswitch " + contentList + " " + planList + " currentMessage=" + this.currentMessage.getDefaultRep());
			String planTodo = msgContentToPlan(currentMessage.getDefaultRep(), contentList, planList );
 			return switchToPlan(planTodo);
		}
		/*
		 * Events are perceived at user level as xxx(yyy)
		 */
		protected AsynchActionResult eventswitch(  Term contentList, Term planList ) throws Exception{
//  			println("		### QActorPlanned eventswitch " + contentList + " " + planList + " currentEvent=" + this.currentEvent.getPrologRep());
  			String planTodo = msgContentToPlan( "event("+currentExternalEvent.getMsg()+")", contentList, planList );
 			return switchToPlan(planTodo);
		}		
	 	protected String msgContentToPlan( String msg, Term tmsgs, Term tplans ) throws Exception{
			String goal = "msgContentToPlan("+ msg +" , "+ tmsgs + "," + tplans +", RES)";
// 			println("		### QActorPlanned goal " + goal );
			SolveInfo sol = myCtx.getPrologEngine().solve(goal+"."); //msgContentToPlan is defined in sysRule.pl
			if( sol.isSuccess() ){					
				String planToDo = sol.getVarValue("RES").toString();
// 				println("		### QActorPlanned planToDo " + planToDo );
				return planToDo;
			}else //inconsistent
				throw new Exception("receiveMsgAndSwitch inconsistent ");	
	 	}

	 	protected String msgToPlan( String msg, Term tmsgs, Term tplans ) throws Exception{
			String goal = "checkMsg("+ msg +" , "+ tmsgs + "," + tplans +", RES)";
			SolveInfo sol = myCtx.getPrologEngine().solve(goal+"."); //checkMsg is defined in sysRule.pl
			if( sol.isSuccess() ){					
				String planToDo = sol.getVarValue("RES").toString();
				return planToDo;
  			}else //inconsistent
				throw new Exception("receiveMsgAndSwitch inconsistent ");	
	 	}
	 	
	 	protected QActorMessage getCurrentMessage(){
	 		return currentMessage;
	 	}
 /*
 * 		
 */

	 	 protected int waitForUserCommand( )  {		
	  		try {
	  			int inp;
	  			int ch;
	  			System.out.println("USER>: to end press 'e'" );
	  			ch = System.in.read();
	  			System.out.println("user:" + ch);
	  			do{
	  				inp = System.in.read();
	  			}while( inp != 10 );
	  			return ch;			
	  		} catch (Exception e) {
	  			System.out.println("USER>: ERROR" );
	  			return 'e';
	  		}
	  	}

}
