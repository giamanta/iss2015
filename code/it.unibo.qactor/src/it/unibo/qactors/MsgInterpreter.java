package it.unibo.qactors;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.system.SituatedPlainObject;

public class MsgInterpreter extends SituatedPlainObject{
private static MsgInterpreter mySelf;
private ActorContext ctx;
/*
 * SINGLETON
 */
	public static MsgInterpreter getInstance(ActorContext ctx, IOutputView outView){
		if( mySelf == null ) mySelf = new MsgInterpreter(ctx,outView);
		return mySelf;
	}	
	public MsgInterpreter(ActorContext ctx, IOutputView outView){
		super(outView);
		this.ctx = ctx;
	}
	public  void elab(String msg ) throws Exception{
		//msg( MSGID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM )
  		msg = msg.replaceAll("&", ","); 
//    	println("MsgInterpreter of " + ctx.getName() + " elab: " + msg  );
		if( ctx.getMsgType(msg).contains("event")){
//			println("MsgInterpreter elab event " + msg + " in ctx=" + ctx.getName() );
			QActor dest = ctx.getActor("evlpa"+ctx.getName());
//			println("MsgInterpreter sends event " + msg +" to dest="  + dest  );
			if( dest != null )  dest.storeMsg(msg);
			return;
		}
		/*
		 * The message is not an event
		 */
 		QActor dest = ctx.getReceiverActor(msg);
 		if( dest == null ){
			for(int i=1; i<=10; i++){
				Thread.sleep(500);	//perhaps not yet registered ...
				dest = ctx.getReceiverActor(msg);
 				println("MsgInterpreter ... " + msg +" to dest="  + dest   );
				if(  dest != null)  break;
			}
 		}
		if( dest != null ){
// 			println("MsgInterpreter sends " + msg +" to dest="  + dest.getName()  );
			dest.storeMsg(msg);
		}

// 		System.out.println( "MsgInterpreter elab ENDS"    );
	}
}
