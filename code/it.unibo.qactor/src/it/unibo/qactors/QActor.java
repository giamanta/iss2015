package it.unibo.qactors;
import java.lang.reflect.Method;
import java.util.Hashtable;
import java.util.ListIterator;
import java.util.Stack;
import java.util.Vector;

import alice.tuprolog.Library;
import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Theory;
import alice.tuprolog.Var;
import alice.tuprolog.lib.InvalidObjectIdException;
import it.unibo.contactEvent.interfaces.IContactEventPlatform;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.contactEvent.platform.ArgTable;
import it.unibo.contactEvent.platform.ContactEventPlatform;
import it.unibo.contactEvent.platform.EventHandlerComponent;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.qactors.action.ActionDummy;
import it.unibo.qactors.action.ActionReceiveAsynch;
import it.unibo.qactors.action.ActionSolveGoal;
import it.unibo.qactors.action.ActionSound;
import it.unibo.qactors.action.ActionUtil;
import it.unibo.qactors.action.AsynchActionResult;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.action.TaskActionFSMExecutoResult;
import it.unibo.qactors.action.IActorAction.ActionExecMode;
import it.unibo.qactors.planned.PlanActionDescr;
import it.unibo.qactors.web.GuiUiKb;
import it.unibo.system.SituatedActiveObject;
import it.unibo.system.SituatedSysKb;
 

public abstract class QActor extends SituatedActiveObject{
protected static int nameCount 		  		 = 0;	 //to avoid  built-in componebts with the same name  
public static final String sep="%"; 
public static final String guardVolatile  	 = "volatile";
public static final String guardPermanent 	 = "permanent";
public static final boolean suspendWork      = false;
public static final boolean continueWork     = true;
public static final boolean interrupted      = true;
public static final boolean normalEnd        = false;
 

	protected boolean actorTerminated 			 = false;
	protected String myId;
	protected ActorContext myCtx;
	protected Vector<String> msgQueue ;
	protected Prolog pengine ;
	protected IContactEventPlatform platform ;
	protected int numOfreceive = 1;
	protected int nPlanIter = 0;
 	protected Stack<String>  planStack; 
	protected Stack<Integer> iterStack;
	protected String curPlanInExec;	
	protected IEventItem currentEvent = null;
	protected IEventItem currentExternalEvent = null; //see executeActionAsFSM
	protected QActorMessage currentMessage = null;
	
	//Defined here to be set in QActrPlanned	
	protected Hashtable<String, Vector<PlanActionDescr> > planTable;
	
	public QActor(String actorId, ActorContext myCtx, IOutputEnvView outEnvView ) {
		super( outEnvView , actorId );
		myId  		= actorId;
		this.myCtx 	= myCtx;
		actorTerminated = false;
		planStack   = new Stack<String>();
		iterStack   = new Stack<Integer>();
		//The actor could share the same theory with the other actors of the same context (?)
		//myCtx.getPrologEngine();
		//The actor has its own world theory and rules
		pengine     = new Prolog(); 
		msgQueue 	= new Vector<String>();
//		Register => activate
		try {
 			/*
			 * Load the theory of the (initial) world
			 */
  			loadWorldTheory();
 			platform = ContactEventPlatform.getPlatform(myCtx);
		} catch (Exception e) {
			e.printStackTrace();
		}
		myCtx.registerActor(actorId, this);
		activateTheActor(); 
	} 
	public String getCurrentPlan() {
		return curPlanInExec;
	} 	
	public ActorContext getContext(){
		return myCtx;
	}
	public IOutputView getOutputView(){
		return outEnvView;
	}
	public Hashtable<String, Vector<PlanActionDescr> > getPlanTable(){
		return planTable;
	}
	protected void activateTheActor(){
		try { 
			/*
			 * Register the QActor in the TuProlog environment
			 */
//  			registerActorInProlog17();
  			registerActorInProlog18();
			activate(SituatedSysKb.executorManyThread);
//  		System.out.println("QActor ACTIVATED in MANY_thread:_scheduler:" + myCtx.actorTable.get(myId).getName() );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void delayNaive(int dt) throws InterruptedException{
		//println("delayNaive " + dt);
		Thread.sleep(dt);
	}

//	protected void registerActorInProlog17() {
//		try {
//			Library lib = pengine.getLibrary("alice.tuprolog.lib.JavaLibrary");
//			println("QActor Registering in TuProlog 17... " + getName() ); 
//			((alice.tuprolog.lib.JavaLibrary)lib).register(new Struct( getName() ), this);
//		} catch (InvalidObjectIdException e) {
//			println("registerActorInProlog17 ERROR " + e.getMessage() );
//			e.printStackTrace();
//		}		
//	}
	 	
	protected void registerActorInProlog18() throws InvalidObjectIdException{  
//		println("QActor Regsitering in TuProlog ... " + this.getName()  ); 
		Library lib = pengine.getLibrary("alice.tuprolog.lib.OOLibrary");
//		println("QActor Registering in TuProlog18 ... " + lib ); 
		((alice.tuprolog.lib.OOLibrary)lib).register(new Struct( getName() ), this); 
//		println("QActor Registered in TuProlog18"); 
	}
	public void registerActorInProlog18(QActor a) throws InvalidObjectIdException{  
//		println("QActor Regsitering in TuProlog ... " + this.getName()  ); 
		Library lib = pengine.getLibrary("alice.tuprolog.lib.OOLibrary");
//		println("QActor Registering in TuProlog18 ... " + lib ); 
		((alice.tuprolog.lib.OOLibrary)lib).register(new Struct( a.getName() ), 	a); 
//		println("QActor Registered in TuProlog18"); 
	}
 
    protected void initSensorSystem(){
    	//doing nothing in a QActor
    }

	 
 	public boolean isTerminated(){ return actorTerminated; }

	public int  getQueueSise( ) {
		return msgQueue.size();
	}
    public void testForProlog(String arg) throws Exception{
    	if( arg.equals("xxx")) throw new Exception("error xxx");
    	else println("QActor testForProlog: " + arg  );
    }
	public void sendMsg( String msgID, String destActorId, String msgType, String msg ) throws Exception{
 		//If the context of destActorId is the current context => local call
 		 
 		QActor dest = myCtx.getActor(destActorId);
//  		println("QActor sendMsg: " + msg + " to " + destActorId + " dest=" + dest);
 		if( dest != null ) //the dest actor is local
 			localCall( msgID, destActorId,  msgType,  msg);
 		else{
 			SenderObject sa = myCtx.getSenderAgent(destActorId);
   			while( sa == null){ //dynamic system not yet updated
	 			println(destActorId + " not yet started (remoteCall) , sleep for a while .. "  );
	 			Thread.sleep(1000);
	 			sa = myCtx.getSenderAgent(destActorId);
			}
 			sa.sendMsg(this, destActorId, msgID, msgType,  msg );	
 		}
 	}	
 	protected void localCall(String msgID, String destActorId, String msgType, String msg){
 		QActor dest = null;
 		while( dest == null ){
			dest = myCtx.getActor(destActorId);
			if( dest != null ) {
				//println("localCall to " + destActorId );
				int msgNum = myCtx.newMsgnum();
		 		String mout = "msg(" + msgID + ","+ msgType + "," + getName() + ","+ destActorId +","+ msg+","+ msgNum + ")";
				//println("QActor localcall mout: " + mout );
				dest.storeMsg(mout);	
			}else{
				try {
					println(destActorId + " not yet started (localCall) , sleep for a while .. "  );
					Thread.sleep(1000);
	 			} catch (InterruptedException e) {
	 				e.printStackTrace();
				}
			}//else
		}//while
 	}
	
	/*
	 * Basic primitive to store a message in the Qactor message queue
	 */
	public synchronized void  storeAMsg( String msg ){ 
//     		println( "QActor " + getName() + " storeMsg:" + msg  + " msgQueue size=" + msgQueue.size() );
		/*
		 * See removeMsg
		 */
		msgQueue.add(msg);
		notifyAll();
	}
	/*
	 * Basic primitive to remove a message from the Qactor message queue
	 */
	protected synchronized String removeMsg( ) throws Exception{
//  		println("QActor " + getName() + " removeMsg queue.size=" + msgQueue.size());
		while( msgQueue.size() == 0){
			wait();
		}
// 		println("QActor " + getName() + " removeMsg RESUMES "   );
		String msg = msgQueue.remove(0);
//		println("QActor " + getName() + " removeMsg RESUMES " + msg );
		return msg;
 	}
	
	public String receiveMsg( ) throws Exception{
		return removeMsg();
	}
	public  void  storeMsg( String msg ){
		storeAMsg( msg );
	}
	
	public void printCurrentMessage(boolean withMemo){
		String msgStr = currentMessage.getDefaultRep();
		println("--------------------------------------------------------------------------------------------");
		if(currentMessage != null){
			println(getName() + " currentMessage=" + msgStr );
		}
		else println(getName() + " currentMessage IS null"  );
		println("--------------------------------------------------------------------------------------------");
		if( withMemo ) addRule(msgStr);
	}
	public void printCurrentEvent(boolean withMemo){
		String eventStr = currentEvent.getDefaultRep();		
		println("--------------------------------------------------------------------------------------------");
		if(currentEvent != null){
			println(getName() + " currentEvent=" + eventStr );
		}
		else println(getName() + " currentEvent IS null"  );
		println("--------------------------------------------------------------------------------------------");
		if( withMemo ) addRule(eventStr);
	}
	public void memoCurrentMessage() throws Exception{
		String msgStr = currentMessage.getDefaultRep();		
//		println(getName() + " 			memoCurrentMessage:" +msgStr );		 
//		addRule(msgStr);
		this.pengine.solve("asserta("+msgStr +").");
 	}
	public String getCurrentMessageRep(){
		return currentMessage.getDefaultRep();
 	}
	public Term getCurrentMessageAsTerm(){
		Term mt = currentMessage.getTerm();
		println("getCurrentMessageAsTerm " + mt);
		return mt;
 	}
	public void memoCurrentEvent() throws Exception{
 		try{
//			Term t = Term.createTerm(currentEvent.getDefaultRep());
//			println(getName() + " 			memoCurrentEvent currentEvent:" + currentEvent );
			String eventStr = currentEvent.getPrologRep();	
// 	 		println(getName() + " 			memoCurrentEvent:" + eventStr );		 
	 		this.pengine.solve("asserta("+eventStr +").");
		}catch( Exception e){
			println("memoCurrentEvent ERROR " + e.getMessage() );
  		}
	}

	/*
	 * High level primitive to be used in applications
	 * where when some message receive with template is used 
	 */
	protected AsynchActionResult receiveAMsg( int tout ) throws Exception{
//		println(" *** receiveAMsg tout=" + tout);
		return receiveAMsg(tout,"","");	 	
	}
	/*
	 * Receives a message with a specific template within a time-interval
	 */
 	protected AsynchActionResult receiveAMsg( int tout, String events, String plans ) throws Exception{
 		currentMessage=null;
		return receiveMsg("msg( MID, MSGTYPE, SENDER,"+ getName() +", CONTENT, SEQNUM)",tout, events, plans);
	}
	protected AsynchActionResult receiveMsg( String msgid, String msgtype, String msgsender, String msgreceiver, 
			String msgcontent, String msgseqnum, int tout, String events, String plans ) throws Exception{
		Term t = Term.createTerm( 
			    "msg( " + msgid + "," + msgtype + "," + msgsender + "," + msgreceiver + "," + msgcontent + "," + msgseqnum + ")" );
		return receiveMsg(t.toString(), tout, events, plans );		
	}
 	/*
	 * Delegates to an asynchAction ActionReceive the task to look for a message unifiable with  msgTermToReceive
	 * The action terminates before maxDuration when a message is recived.
	 * Otherwise the action ends with a time out: in this case there the message is msg(none,none,none,none,none,0)
	 */
	public AsynchActionResult receiveMsg( String msgTermToReceive, int tout, String events, String plans ) throws Exception{
//    	println("+++ QActor "+getName()+" receiveMsg " + msgTermToReceive + " tout=" + tout + " events=" + events + " plans=" + plans);
 		ActionReceiveAsynch action = new ActionReceiveAsynch("actionReceive"+nameCount++,this,
				msgTermToReceive, outEnvView, tout);	
 		AsynchActionResult aar = executeActionAsFSM( action, events, plans, ActionExecMode.synch );

//     	println("+++  QActor "+getName()+" receiveMsg aar=" + aar.toString()   ); 
// 		println("+++  QActor "+getName()+ " ACTION=" + action.getActionAndResultRep() ); 
//		println("+++  QActor "+getName()+ " action.getExecTime()=" + action.getExecTime() ); 
// 		if( aar.getTimeRemained() <= 0 ){
// 			println("WARNING: " + getName()+" receiveMsg ... timeout "   + aar.getTimeRemained()  ); 
//  		}
		if( aar.getInterrupted()   ){
// 			println("--- QActor receiveMsg interrupted aar=" + aar  ); 
			aar.setResult("result(noresult)");
 			currentMessage  = new QActorMessage( "msg(none,none,none,none,none,0)" );
		}
		else if(  aar.getTimeRemained()<=0 ){
			//println("WARNING: " + getName()+" receiveMsg timeout ... "   + aar.getTimeRemained() + " tout=" +  tout); 
			aar.setResult("msg(none,none,none,none,none,0)");
 			currentMessage  = new QActorMessage( "msg(none,none,none,none,none,0)" );
 			aar.setGoon( false ); //after a timeout we do not continue
		}else{
			String msg = action.getReceivedMsg();
//			msg = GuiUiKb.buildCorrectPrologString(msg);
//  			println("--- QActor "+getName()+" receiveMsg msg..." + msg  ); 
 			currentMessage  = new QActorMessage( msg );
//			println("--- QActor "+getName()+" receiveMsg currentMessage=" + currentMessage.getDefaultRep()  ); 
			aar.setResult(msg);
			aar.setGoon( true );
//			println("--- QActor "+getName()+" receiveMsg ok aar=" + aar  ); 
		}
//		println("--- QActor "+getName()+" receiveMsg/4 aar=" + aar  ); 
		return aar;		
	}
	//genymotion.com
	@Override
	protected void startWork() throws Exception {
	}
	@Override
	protected void endWork() throws Exception {
//		println("QACTOR " + getName() + " endWork");
//		actorTerminated = true;
	}
	
	public Prolog getPrologEngine(){
		return pengine;
	}
	
/*
 * =======================================================================
 * BUILT-In actions	
 * =======================================================================
 */
	
/*
 * --------------------------------------------- 
 * COMMUNICATION
 * --------------------------------------------- 
 */
	public void emit( String evId, String evContent ) throws Exception{
		try{
			evContent = evContent.replace("@", "");
			//println(" *** emit " + evId + ":" + evContent  ); 
			platform.raiseEvent(this.getName(), evId, evContent);
		}catch(Exception e){
			//println(" *** emit ERROR " + e.getMessage() ); 
		}
	}
	protected void emit( String evId, String evContent, ArgTable at ) throws Exception{
		platform.raiseEvent(this.getName(), evId, evContent);
	}
	protected void emitEventAsynch( final String eventId, final String evContent, final int dt ){
		new Thread(){
			public void run(){
				try {
					Thread.sleep(dt);
					println(" --- EMIT  EVENT " + eventId  );
					emit(  eventId, evContent ); 
				} catch (Exception e) {
 					e.printStackTrace();
				}
			}
		}.start();
	}
 	
 	public void forwardFromProlog( String msgId, String dest, String msg  ) throws Exception {
		dest = dest.replace("'", "");
		println( " forwardFromProlog  "  + msgId + " to " + dest + " " + msg );
		sendMsg(msgId, dest, ActorContext.dispatch, msg );
	}
 	protected void forward(String msgId, String dest, String msg) throws Exception{
		//println(getName() + " forward  " + msgId + " to " + dest );
		sendMsg(msgId, dest, ActorContext.dispatch, msg );		
	}
	protected void replyToCaller(String msgId, String msg) throws Exception{
		String caller = currentMessage.msgSender();
//		println(getName() + " replyToCaller  " + msgId + ":" + msg + " to " + caller );
		sendMsg(msgId, caller, ActorContext.dispatch, msg );		
	}
	
	protected void demand(String msgId, String dest, String msg) throws Exception{
		//println(getName() + " demand a request" );
		sendMsg(msgId, dest, ActorContext.request, msg  );				
	}

/*
* --------------------------------------------- 
* MULTI MEDIA
* ---------------------------------------------  
*/
	protected IActorAction buildSoundAction( IOutputEnvView outEnvView, int duration,  String fName) throws Exception{
		String terminationEvId = IActorAction.endBuiltinEvent+nameCount++;
 		IActorAction action = new ActionSound("sound", false, terminationEvId, "", outEnvView, duration, fName );  
  		return action;
  	}
	//playSound/5 : public to test its usage from Prolog
	public AsynchActionResult playSound(String fName, int duration, String answerEvent, String  alarmEvents, String recoveryPlans) throws Exception{
		if( answerEvent==null ) answerEvent="";
//		println("QActor playSound fName= " + fName);
		IActorAction action = ActionUtil.buildSoundAction(outEnvView,duration,answerEvent,fName);
   		ActionExecMode mode = (answerEvent.length()==0) ? ActionExecMode.synch : ActionExecMode.asynch ;
//		println("QActor playSound alarmEvents= " + alarmEvents + " recoveryPlans=" + recoveryPlans);
		AsynchActionResult res =  executeActionAsFSM( action, alarmEvents, recoveryPlans, mode );
 		return res;
 	}
	protected  AsynchActionResult senseEvents(int tout, String events, String plans, String  alarmEvents, String recoveryPlans, ActionExecMode mode) throws Exception{
		IActorAction action = new ActionDummy(outEnvView, tout );	 
		String mergedEvents = events + "," + alarmEvents;
		String mergedPlans  = plans + "," + recoveryPlans;
		AsynchActionResult aar = executeActionAsFSM( action, mergedEvents, mergedPlans, mode);
//		println("senseEvents event= " + aar.getEvent().getMsg());
 		if( ! aar.getGoon() || aar.getTimeRemained() <= 0){
			println("WARNING: sense time remained= " + + aar.getTimeRemained() + " tout=" + tout);
 			aar.setGoon( false ); //after a timeout we do not continue
		}
   		return aar;
	}

/*
 * -----------------------------------------------------------
 * UTILITY
 * -----------------------------------------------------------
 */
public void println(String msg){
	super.println("[" + this.myId + "] " + msg);
}

/*
 * -----------------------------------------------------------
 * EXECUTION of actions
 * -----------------------------------------------------------
 */
	protected void execActionAsynch(IActorAction action) throws Exception{
  		//Executes an action as a parallel activity (that emits events, see AsynchAction)
//		action.runTheAction("");
		action.execASynch();
	}

	protected String explainExecuteActionResult(long res){
  		String outS = "";
  			if( res == 0 ) outS += "action done without interruptions";
  			else if( res > 0 ) outS += "action interrupted by an event with plan todo=" + res;
  			else if( res == -1 ) outS += "action interrupted by a superStop";
  		return outS;
  	}
	
 
	
/*
 * ---------------------------------------------------
 * REFLECTION
 * ---------------------------------------------------
 */
 	
//	public <T> boolean execByReflection(Class<T> C, String methodName)  {
	public  boolean execByReflection(Class  C, String methodName  )  {
//		currentEvent = ev;
		Method method = null; 
		Class curClass = C;
		while( method==null )
		try{
			if( curClass == null ) return false;
			method = getByReflection(curClass,methodName);
			if( method != null ){
//   			  	  println("QActor execByReflection method: " +method + " in class " + curClass.getName());
			   	  Object[] callargs = null;
			  	  Object returnValue = method.invoke( this,  callargs );
// 			  	  println("QActor execByReflection returnValue: " +returnValue );
			  	  Boolean goon = (Boolean) returnValue;
			  	  return goon;				
			}else{
// 			  	println("QActor execByReflection " + methodName + " not found in " +curClass.getName() );
				curClass = curClass.getSuperclass();
			}
		}catch(Exception e){
			//If the method does not exist or does not return a boolean return false
			println("QActor execByReflection " + methodName + "  ERROR: " + e.getMessage() );
//			break;
		}	
		return false;
}
	public  boolean execApplicationActionByReflection(Class  C, String methodName, String arg1, String arg2  )  {
		Method method = null; 
		Class curClass = C;
		while( method==null )
		try{
			if( curClass == null ) return false;
 			method = getActionByReflection(curClass,methodName);
			if( method != null ){
//  			  	  println("QActor execByReflection method: " +method + " in class " + curClass.getName());
			   	  Object[] callargs = new Object[]{arg1,arg2 };
			  	  Object returnValue = method.invoke( this,  callargs );
// 			  	  println("QActor execByReflection returnValue: " +returnValue );
			  	  Boolean goon = (Boolean) returnValue;
			  	  return goon;				
			}else{
// 			  	println("QActor execByReflection " + methodName + " not found in " +curClass.getName() );
				curClass = curClass.getSuperclass();
			}
		}catch(Exception e){
			//If the method does not exist or does not return a boolean return false
			println("QActor execByReflection " + methodName + "  ERROR: " + e.getMessage() );
//			break;
		}	
		return false;
	}
protected IEventItem getPlanActivationEvent(){
	return currentEvent;
}
public Method getByReflection(Class C, String methodName)   {
	try {
		Class noparams[] = {};
		Method method = C.getDeclaredMethod(methodName, noparams);
  		return 	method;	
	} catch (Exception e) {
// 		println("QActor getByReflection ERROR: " + e.getMessage() );
		return null;
	}
}
public Method getActionByReflection(Class C, String methodName)   {
	try {
		Class twoparams[] = { String.class, String.class };
		Method method = C.getDeclaredMethod(methodName, twoparams);
  		return 	method;	
	} catch (Exception e) {
// 		println("QActor getByReflection ERROR: " + e.getMessage() );
		return null;
	}
}
public boolean dummyPlan(){
//	println(getName() + " ==== QActor dummyPlan called by reflection.");	
	return IActorAction.continuePlan;
}

/*
 * ----------------------------------------	
 * WORLD THEORY    
 * ----------------------------------------	
*/  
	protected void loadWorldTheory() throws Exception{
		try{
 	   		Theory worldTh = new Theory( getClass().getResourceAsStream("WorldTheory.pl") );
	  		pengine.addTheory(worldTh);
 	  		println(getName() + " loadWorldTheory done "   );	 		
//	 		alice.tuprolog.SolveInfo sol = pengine.solve("test.");
 		}catch( Exception e){
 			println(getName() + " loadWorldTheory WARNING: "  + e.getMessage() );
 		}
 	}

	/*
	* --------------------------------------------- 
	* GAURDS
	* --------------------------------------------- 
	*/	
	protected Hashtable<String,String> evalTheGuard( String guard ) throws Exception{
	Hashtable<String,String> htss = new Hashtable<String,String>();
//  			println("evalTheGuard " + guard    );
 	  		if( guard.equals("true") ) return htss;
 			boolean toremove = true;
 			boolean hasNot   = false;
 			guard = guard.trim();
 			if( guard.startsWith("not")){
 				hasNot = true;
 				guard = guard.substring(3).trim();
//  				println("evalGuard=" + guard    );
 			}
 			if( guard.startsWith("??")){
 				guard = guard.substring(2).trim();
 				toremove = true;
 			}else if( guard.startsWith("!?")){
 				guard = guard.substring(2).trim();
 				toremove = false; 				
			}else if( guard.startsWith("!!")){
// 				guard = guard.substring(0,guard.lastIndexOf('!'));
// 				toremove = false; 		
				guard = guard.substring(2);
				AsynchActionResult res = execDummyActionForGuardWait( guard );
				//We should insert the event args in the hss
				return htss;	//
 			}else{
// 				println("evalGuard guard prefix wrong"    );
 				throw new Exception("guard prefix wrong");
 			}

  			SolveInfo sol = pengine.solve(  "evalGuard("+guard +").");
  			  			
//   			println("evalGuard " + guard + " solution:" + sol.isSuccess() + " toremove=" + toremove );
			if( sol.isSuccess() ){
//     			println("evalGuard " + guard + "sole=" + sol.getSolution()  + " toremove=" + toremove );
				if(toremove){
		  			//The guard is removed, once evaluated true  
					removeRule( guard ); //we remove the solution sol.getSolution().toString()
 				} 
				if( hasNot ) return null;
				//we must replace , with sep into a binding value in order to allow RobotActr to split correctly 
				String bindVarsOk = "";
				ListIterator<Var> bvit= sol.getBindingVars().listIterator();
				while( bvit.hasNext() ){
					Var v = bvit.next();
					String varName = v.getName();
 					String val = v.getTerm().toString();//.replace("'", "");	//for Prolog rules
//					println("*** evalGuard  " + varName + " val=" + val );
 					htss.put(varName, val);
				}
 				return htss;
			}
			//guard not found
//  			println("evalGuard " + guard + " failiure with hasNot=" + hasNot  + " htss=" + htss  );
			if( hasNot ) return htss;
			else return null;
  	}
	
 	
	protected AsynchActionResult execDummyActionForGuardWait( String alarmEvents  ) throws Exception{
		IActorAction action = new ActionDummy(outEnvView, 60000*60 );	//wait for 1 hour	
		AsynchActionResult res = 
				executeActionAsFSM( action, alarmEvents, "dummyPlan", ActionExecMode.synch );
		return res;
	}
	protected AsynchActionResult execDummyActionForGuardWait( int tout, String alarmEvents, String plans  ) throws Exception{
		IActorAction action = new ActionDummy(outEnvView, tout );	//wait for 1 hour	
		AsynchActionResult res = 
//				new ActorActonExecutorFSM( action,outEnvView, this).executeActionAsFSM( alarmEvents, plans, ActionExecMode.synch);
				executeActionAsFSM( action, alarmEvents, plans, ActionExecMode.synch );
		return res;
	}	
	protected AsynchActionResult delayReactive(int time, String  alarmEvents, String recoveryPlans) throws Exception{
 		IActorAction action = new ActionDummy(outEnvView, time);
  		AsynchActionResult aar = 
//  				new ActorActonExecutorFSM( action,outEnvView, this).executeActionAsFSM( alarmEvents, recoveryPlans, ActionExecMode.synch);
  				executeActionAsFSM(action,alarmEvents , recoveryPlans , ActionExecMode.synch);
		return aar;
	}
	
/*
 * ========================================================================================	
 * executeActionAsFSM creates a TaskActionFSMExecutor that implements a FSM
 * that executes the given action by reacting to alarmEvents
 * ========================================================================================	
 */

	public AsynchActionResult executeActionAsFSM(IActorAction action, String  alarmEvents, String recoveryPlans, ActionExecMode mode) throws Exception {
 		TaskActionFSMExecutoResult res = 
 				new ActorActonExecutorFSM( action,outEnvView, this).executeActionFSM( alarmEvents,  recoveryPlans, mode );
//  		println( "QActor "+ getName() + "  executeActionAsFSM res event=" + res.getEventItem().getDefaultRep()	);	
		//Synchronous execution
		if( mode == ActionExecMode.synch){
	 		String nextPlan       	= res.getPlanTodo();
 			currentEvent            = res.getEventItem(); 
 			if( ! currentEvent.getEventId().startsWith(ContactEventPlatform.locEvPrefix))
 				currentExternalEvent = currentEvent;
// 				println( "QActor "+ getName() + "  executeActionAsFSM nextPlan=" + nextPlan  + " recoveryPlans="	+ recoveryPlans);			
			if( nextPlan == null && recoveryPlans.indexOf("continue") >= 0 ){  //Action (sense) timeout
	 			return new AsynchActionResult(action, res.getMoveTimeNotDone(), this.normalEnd, this.continueWork, "actionDone", res.getEventItem());				
 			}
	 		if(nextPlan != null && nextPlan.length()>0  ) 
	 			if( ! nextPlan.equals("continue") ){
//	 				println( "QActor "+ getName() + "  executeActionAsFSM A nextPlan=" + nextPlan + " " + res.getMoveTimeNotDone() );			
 	 		 		return execOtherPlan(action, nextPlan, res.getMoveTimeNotDone() );	
	 			}else{ //continue
//	 				println( "QActor "+ getName() + "  executeActionAsFSM B nextPlan=" + nextPlan  );			
	 				return new AsynchActionResult(action, res.getMoveTimeNotDone(), normalEnd, continueWork, "actionDone", res.getEventItem());
	 			}
	 		else{
		 	 	boolean goon =  ! action.isSuspended() ;//&& (res.getMoveTimeNotDone() > 0); //not suspended and not time elapsed
//	 	 		println( "QActor ******* "+ getName() + "  executeActionAsFSM action=" +  action.getActionName() + 
//	 	 				" suspended=" + action.isSuspended() + " time=" + res.getEventItem().getTime().getTimeRep() );
	 			return new AsynchActionResult(action,action.getMaxDuration()-action.getExecTime(),normalEnd, 
	 					goon,"actionDone",res.getEventItem());
	 		}
		}
		//Asynchronous execution terminates immediately		
		else{
			return new AsynchActionResult(action,0,normalEnd,continueWork,"actionDone",res.getEventItem());			
		}
 	}
	
	protected AsynchActionResult execOtherPlan(IActorAction action, String nextPlan, long moveTimeNotDone ) throws Exception{
		if(	nextPlan != null && nextPlan.length()>0 ){
// 			 println( " --- QActor " +  getName() + " we should execute via reflection: " +  nextPlan);
			 Class noparams[] = {};
				boolean goon=true;
			if( nextPlan.equals("dummyPlan") || this.planTable == null ){
//	 	 		println( " --- QActor " +  getName() + " executes by reflection in Java " +  nextPlan);
				planStack.push( curPlanInExec );
				iterStack.push( nPlanIter );
				nPlanIter = 0;
				goon = execByReflection( this.getClass(), nextPlan  );
				curPlanInExec = planStack.pop();	
				nPlanIter     = iterStack.pop();
			}else{
	 			//println( " --- QActor " +  getName() + " we should execute via reflection: " +  nextPlan);
	 			AsynchActionResult aar = switchToPlan( nextPlan );	 	
	 			if( aar.getGoon() ){ //all ok
	 				return new AsynchActionResult(action,moveTimeNotDone,normalEnd,continueWork,"",currentEvent );
	 			}
	 			else return aar;
			}
			 if( goon == suspendWork ){
				 return new AsynchActionResult(action,-1,interrupted,suspendWork,"",currentEvent );
			 }else{
// 	  		     println(" ---  executeActionWithEvents 1 " + action.getActionName()   );
				 return new AsynchActionResult(action,moveTimeNotDone,interrupted,continueWork,"",currentEvent );  
			 }
			}
		else{ // nextPlan.length()==0
//  		 println(" ---  executeActionWithEvents 2 " + action.getActionName() + " moveTimeNotDone=" + moveTimeNotDone );
			return new AsynchActionResult(action,moveTimeNotDone,normalEnd,continueWork,"",currentEvent); 
		}
		
	}

	
/*
 * This operation is called by ActonExecutorFSM that must belong to same QActor's package 
 * in order to call a protected methos
 */
	protected AsynchActionResult switchToPlan( String planName ) throws Exception{
		println("QActor shuoukd obe never here in switchToPlan");
		throw new Exception("switchToPlan in QActor");
	}
	
	/*
	* --------------------------------------------- 
	* RULES
	* --------------------------------------------- 
	*/	

		public  synchronized void addRuleTerm( Term rule  ){
			
		}
	 	public  synchronized void addRule( String rule  ){
	 		try{
//  	 			println("addRule:" + rule   );
	 			if( rule.equals("true")) return;
//  	 			println("addRule:" + rule   );
	  			SolveInfo sol = 
	  					pengine.solve( "addRule( " + rule + " ).");
//  	 			println("addRule:" + rule + " " + sol.isSuccess() );
 	  		}catch(Exception e){
	  			println("addRule ERROR:" + rule + " " + e.getMessage() );
	   		}
	   	}
	 	public  synchronized void removeRule( String rule  ){
	 		try{
	 			rule = rule.trim();
	 			if( rule.equals("true")) return;
	 			SolveInfo sol = pengine.solve( "removeRule( " + rule + " ).");
//  	 			println("removeRule:" + rule + " " + sol.isSuccess() );
	 	  	}catch(Exception e){
	  			println("removeRule ERROR:" + e.getMessage() );
	   		}	 		
	 	}

	 	public AsynchActionResult solveGoal( String goal, int duration, String answerEv, String events, String plans) throws Exception{
// 	 		println("solveGoal " + goal + " duration=" + duration +" eventnum=" + events.length() + " " + pengine.isHalted() );
	 		if( duration == 0 && ( answerEv==null || answerEv.length()==0) ){
 	 			SolveInfo sol =this.pengine.solve(goal+".");
// 	 	 		println("solveGoal " + goal + " sol=" + sol  + " " + pengine.isHalted() );
	 			//IActorAction action, long time,  boolean interrupted, boolean goon, String result, IEventItem interruptEvent
	 			
 	 			if( sol != null && sol.isSuccess() ){
 	 				this.pengine.solve("setResult(" + sol.getSolution()+").");
	 				return new AsynchActionResult(null,0,false,true,sol.getSolution().toString(),null);
	 			}else{
	 				this.pengine.solve("setResult( failure ).");
	 				return new AsynchActionResult(null,0,false,true,"failure",null);	 				
	 			}
	 		}
	 		ActionSolveGoal action = new ActionSolveGoal( outEnvView, duration , answerEv, this, goal );
	 		AsynchActionResult aar = executeActionAsFSM( action, events , plans , ActionExecMode.synch ); //asynch  
//   			println("solveGoal aar=" + aar );
	 		SolveInfo sol = action.getSolveResult();
  			
	 		if( sol != null && sol.isSuccess() ){
	 			this.pengine.solve("setResult(" + sol.getSolution()+").");
//	 			println("solveGoal sol=" + sol.getSolution() );
	 			aar.setResult(""+sol.getSolution());
	 			aar.setGoon(true);
//		 		System.out.println("solveGoal " + goal +" success execTime=" + action.getExecTime() );
	 		}
//	 		else if( sol != null && sol.isHalted() ){
//	 			aar.setResult("halted");
//	 			aar.setGoon(true);	 			
//	 		}
	 		else{
//	 			println("solveGoal FAILS " + sol );
	 			this.pengine.solve("setResult( failure ).");
	 			aar.setResult("failure");
	 			aar.setGoon(true);
		 		//println("solveGoal " + goal +" failure execTime=" + action.getExecTime() );
	 		}
 			//println("solveGoal aar=" + aar );
 	 		return aar;	
	 	}
	 	
/*
 * New operation to solve a sentence (originated by the Talk project)	 	
 */
	 	
	 	public AsynchActionResult solveSentence( String sentence ) throws Exception{
// 	 		println("QActor solveSentence " + sentence);
	 		Term guard , goal, dt ,  answerEv , events , plans ;

	 		Struct at = (Struct) Term.createTerm(sentence);
	 		//sentence6(true,fib(12,V_e0),1000,'',usercmd,endOfSound)
	 		int arity=at.getArity();
 	 		if( arity < 6 ){
 	 			return new AsynchActionResult(null,0,false,true,"failure",null);
 	 		}
		 		 guard = at.getArg(0);
		 		 goal  = at.getArg(1);
		 		 dt    = at.getArg(2);
		 		 answerEv  = at.getArg(3);
		 		 events    = at.getArg(4);
		 		 plans     = at.getArg(5);
	 		String ev =   events.toString();
	 		String pl =   plans.toString();
	 		if( ev.equals("''")) ev="";
	 		if( pl.equals("''")) pl="";
 	 		int duration = Integer.parseInt(""+dt);
// 	 		println("QActor solveSentence solveGoal "+ goal + " duration=" + duration);
 	 		AsynchActionResult aar = solveGoal( ""+goal, duration,  ""+answerEv, ev,  pl);
// 	 		println("QActor solveSentence result="+aar.getResult());
 	 		this.pengine.solve("setAnswer("+aar.getResult()+").");
	 		//Show the result in the user GUI
	 		//pengine.solve("actorPrintln("+aar.getResult()+").");
	 		return aar;
	 	}

	 	public String substituteVars(java.util.Hashtable<String, String> guardVars, String parg){
	 		String outS = parg;
			if( guardVars!= null ){
				java.util.Iterator<String> it = guardVars.keySet().iterator();
			    while( it.hasNext() ){
			    	String varin = it.next() ; 
			    	String valin = guardVars.get( varin );
					//println("		it: " +  varin + " " + valin) ;
					outS = outS.replace( varin,valin );
				}
			}
	 		return outS;
	 	}

}

 


/*
 * =======================================================================
 * Handlers of events generated when the receiveMsg action is terminated
 * It is defined just to show the event-driven programming at work
 * =======================================================================
 */
	class EndOfReceiveHandler extends EventHandlerComponent{

		public EndOfReceiveHandler(String name, ActorContext myctx, String eventId,  
				IOutputEnvView view) throws Exception {
			super(name, myctx, eventId, view);
			//println("EndOfReceiveHandler name=" + name );
 		}
		@Override
		public void doJob() throws Exception {
			IEventItem event = getEventItem();
			String msg = event.getEventId() + "|" + event.getMsg() + " from " + event.getSubj()   ;
			//showMsg( msg );
			println( ">>> "+msg );		
		}
		
	}

