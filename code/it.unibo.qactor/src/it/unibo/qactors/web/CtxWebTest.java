/*
 */
package it.unibo.qactors.web;
import java.awt.Color;
import java.io.FileInputStream;
import java.io.InputStream;
import it.unibo.baseEnv.basicFrame.EnvFrame;
import it.unibo.is.interfaces.IBasicEnvAwt;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.web.QActorHttpServer.EvhQActorHttpServer;

public class CtxWebTest extends ActorContext{
	protected QActorHttpServer qaserver  ;
	
	public CtxWebTest(String name,
			IOutputEnvView outEnvView, InputStream inputStream, InputStream sysRulesStream, QActorHttpServer qaserver ) throws Exception {
		super(name, outEnvView, inputStream, sysRulesStream );
		this.qaserver = qaserver;
  	}
 	@Override 	
	public void configure(){ 
		try {
			
// 				EvhHttpAppl evh = 
 						new EvhHttpAppl( qaserver, "evhhttp", this, "usercmd",outEnvView);
 
 		} catch (Exception e) {
			println(getName() + " ERROR " + e.getMessage() );
		}
	}	
//	public static void main(String[] args) throws Exception {
//		IBasicEnvAwt env = new EnvFrame("ctx2", null, Color.cyan, Color.black);
//		env.init();
//		env.writeOnStatusBar("ctx1" + " | working ... ", 14);
//		InputStream sysKbStream    = new FileInputStream(
//				"./src/it/unibo/qactors/example/schedule/sysKb.pl");
//		InputStream sysRulesStream = new FileInputStream("sysRules.pl");
//		new CtxWebTest("ctx1", env.getOutputEnvView(), sysKbStream, sysRulesStream, ).configure();
//	}
}
