package it.unibo.qactors.web;
import it.unibo.contactEvent.interfaces.IContactEventPlatform;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.contactEvent.platform.ContactEventPlatform;
import it.unibo.contactEvent.platform.EventHandlerComponent;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.system.SituatedSysKb;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import fi.iki.elonen.IWebSocketFactory;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.WebSocket;
import fi.iki.elonen.WebSocketFrame;
import fi.iki.elonen.WebSocketFrame.CloseCode;
import fi.iki.elonen.WebSocketResponseHandler;

public class QActorHttpServer extends NanoHTTPD{
protected IOutputEnvView outEnvView;
protected WebSocketResponseHandler responseHandler;
protected	IWebSocketFactory webSocketFactory;
protected IContactEventPlatform platform ;
protected String dirPath;
	public QActorHttpServer(IOutputEnvView outEnvView, String dirPath, int port) {
		super(port);
		this.dirPath = dirPath;
//		NanoHTTPD.SOCKET_READ_TIMEOUT=30000;
		webSocketFactory = new IWebSocketFactory() {		
			@Override
			public WebSocket openWebSocket(IHTTPSession handshake) {
					return new Ws(handshake);
			}
		};
		this.outEnvView = outEnvView;
		responseHandler = new WebSocketResponseHandler(webSocketFactory);
		try {
			platform = ContactEventPlatform.getPlatform();
		} catch (Exception e) {
 			e.printStackTrace();
		}
 		outEnvView.addOutput("		*** QActorHttpServer starts dirPath=" + dirPath);
	}
	
	protected String uri = null;
	protected NanoHTTPD.Response response;
	
	@Override
	public Response serve(IHTTPSession session) {
//  		outEnvView.addOutput("		=== QActorHttpServer serve: "  );
		 
		response = responseHandler.serve(session);
		if(response == null){
			uri = session.getUri();
			return respondToServe();
		}
		return response;
	}
	
	protected Response respondToServe(){
//  		outEnvView.addOutput("		=== QActorHttpServer serve: " + uri + " dirPath=" + dirPath);
	    try {
			platform = ContactEventPlatform.getPlatform();
		    FileInputStream fis = null;
				if (uri.equals("/")){
			    fis = new FileInputStream(dirPath+"/QActorWebUI.html");
			    return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK, "text/html", fis);
			}else if (uri.equals("/QActorWebUI.js")){
		    	fis = new FileInputStream(dirPath+"/QActorWebUI.js");
		    	return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK, "text/javascript", fis);
	 		}else if (uri.equals("/QActorWebUI.css")){
		    	fis = new FileInputStream(dirPath+"/QActorWebUI.css");
		    	return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK, "text/css", fis);			
	 		}else
	 			return new NanoHTTPD.Response("<html>NOT UDERSTAND</html>"); 
	    } catch (Exception e) {
//	    	outEnvView.addOutput("QActorHttpServe dirPath=" + dirPath + " ERROR " + e.getMessage() );
//			return new NanoHTTPD.Response("<html><body style='color:red;font-family: Consolas;'>hello, i am runing....</body></html>"); 
	    	return respondBuiltInToServe();
	    }
		
	}
	
	protected Response respondBuiltInToServe(){
	    try {
	 		 InputStream fis = null;
	 		 String dirPath = "./srcWeb";
	    	 outEnvView.addOutput("QActorHttpServe respondBuiltInToServe dirPath=" + dirPath  );
				if (uri.equals("/")){
			    fis = //new FileInputStream(dirPath+"/QActorWebUI.html");
			      QActorHttpServer.class.getResourceAsStream("QActorWebUI.html") ;
			    return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK, "text/html", fis);
			}else if (uri.equals("/QActorWebUI.js")){
		    	fis = //new FileInputStream(dirPath+"/QActorWebUI.js");
		    			QActorHttpServer.class.getResourceAsStream("QActorWebUI.js") ;
		    	return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK, "text/javascript", fis);
	 		}else if (uri.equals("/QActorWebUI.css")){
		    	fis = //new FileInputStream(dirPath+"/QActorWebUI.css");
		    			QActorHttpServer.class.getResourceAsStream("QActorWebUI.css") ;
		    	return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK, "text/css", fis);			
	 		}else
	 			return new NanoHTTPD.Response("<html>NOT UDERSTAND</html>"); 
	    } catch (Exception e) {
	    	outEnvView.addOutput("QActorHttpServe dirPath=" + dirPath + " ERROR " + e.getMessage() );
			return new NanoHTTPD.Response("<html><body style='color:red;font-family: Consolas;'>hello, i am runing....</body></html>"); 
	    }
		
	}
	
	/*
	 * This operations can be redefined by specialized classes
	 */
	protected void handleUserCmd(String cmd) throws Exception{
		try{
			String eventMsg;
			String moveToDo;
			//Syntax check
			Struct ct ;
			if( cmd.startsWith("i-")){ 
				/*
				 * New command user interface for input commands
				 */
				moveToDo = cmd.split("-")[1];
//				outEnvView.addOutput("		=== QActorHttpServer moveToDo="+moveToDo);
				eventMsg = inputToEventMsg(moveToDo);
				outEnvView.addOutput("		=== QActorHttpServer cmd: " + cmd + 
						" emits " + GuiUiKb.inputCmd +":"+eventMsg  );
				platform.raiseEvent("wsock", GuiUiKb.inputCmd, eventMsg);
				return;
			}//cmd.startsWith("i-")
			else{			
				/*
				 * Old command user interface for move (usercmd) commands
				 */
				//outEnvView.addOutput("		=== QActorHttpServer cmd="+cmd);
				ct = (Struct) Term.createTerm(cmd);
				if( ct.getName().toString().startsWith("e")) {
				/*
				 * RAISE IMMEDIALTELY AN EVENT
				 */
					outEnvView.addOutput("		=== QActorHttpServer EVENT: " + ct.getName()  );
 					platform.raiseEvent("wsock", "alarm", cmd );
 					return;
				}
				eventMsg = "usercmd(robotgui("+ct+"))";
			}
			outEnvView.addOutput("		=== QActorHttpServer cmd: " + cmd + 
					" emits " + GuiUiKb.terminalCmd +":"+eventMsg  );
			platform.raiseEvent("wsock", GuiUiKb.terminalCmd, eventMsg);
		}catch( Exception e){
			outEnvView.addOutput("		=== QActorHttpServer ERROR: " + e.getMessage() );
		}
	}
	
	/*
	 * From input
	 */
	public static String inputToEventMsg(String input){
		String eventMsg = null;
		String moveToDo = GuiUiKb.buildCorrectPrologString(input);
//  			System.out.println("		=== QActorHttpServer inputToEventMsg moveToDo="+moveToDo);
 			try{
  				Term mt = Term.createTerm(moveToDo);
  				Struct ms;
 				if( mt instanceof Struct){
					ms = (Struct) mt;
// 				 	System.out.println("		=== QActorHttpServer inputToEventMsg ms=" + ms);
					if( ms.getName().equals(",")){
	 				 	Term guard = getGuard( ms );
						/* The term must have the form : 
 						 *  [ guard ] , goal 
						 *  [ guard ] , goal, duration , ... 
						 */
//	 				 	System.out.println("		=== QActorHttpServer inputToEventMsg guard=" + guard);
	 				 	if( guard == null ){
	 				 		//moveToDo="print('ERROR: missing guard')";
	 				 		moveToDo= "[true],"+moveToDo;
	 				 		mt = Term.createTerm(moveToDo);
	 				 		ms = (Struct) mt;
	 				 		guard = getGuard( ms );
	 				 	}	 				 	
	 				 	/*
	 				 	 * Syntax checking is done at Prolog level
	 				 	 */
//						Term a1 = ms.getArg(1); 
//						System.out.println("		=== QActorHttpServer a1=" + a1  );
//						Term td = null;
//						if(a1 instanceof Struct){ //a1 SHOULD BE guard, move, duration ...
//							Struct a1s = (Struct) a1;						 
//							if( a1s.getName().equals(",")){						 
//								//[ guard ] , goal , something
//								td = getDuration( a1s.getArg(1));	
//								if( td == null ){
//			 				 		moveToDo="print('ERROR: missing duration')";
//			 				 		eventMsg = "usercmd(executeInput("+moveToDo+"))";
//			 				 		return eventMsg;								
//	 							}//fib(7,V),1000,x,[a],[b] 	
//								// a1 SHOULD BE 
//								// guard, move, duration 					or
//								// guard, move, duration , event 			or
//								// guard, move, duration , [ ... ] , [ ... ] 
//								td = a1s.getArg(1);  
//								if( td instanceof Struct){	//,(DURATION, ...
//									Struct a2s = (Struct) td;
//									td=a2s.getArg(1);
//									if( td instanceof Struct){	//must be a list
//										Struct a3s = (Struct) td;	
//										System.out.println("		=== QActorHttpServer a2s=" + a3s  );
//										if( a3s.getArg(1).isList()  ){
//											moveToDo= a1+"''"+a2s;	//insert ENDEVENT=''
//											System.out.println("+++++++++++++++++++++ " + moveToDo);
//										}else{
//					 				 		moveToDo="print('ERROR: missing lists')";
//					 				 		eventMsg = "usercmd(executeInput("+moveToDo+"))";
//					 				 		return eventMsg;																		
//										}
//										
//									}
//								}
// 							}
//						}
 						moveToDo="do("+moveToDo+")";
 						eventMsg = "usercmd(executeInput("+moveToDo+"))";
					}else{		
						/* The term  has the form : 
						 *  f(...)
		 				 */ 				
						//System.out.println("		=== QActorHttpServer inputToEventMsg GOAL="+moveToDo + " " + (mt instanceof Struct) );
						//moveToDo="do("+moveToDo+")";
						eventMsg = "usercmd(executeInput("+moveToDo+"))";	
 						return eventMsg;
					}			
 			}else{
				/* The term  has the form : 
				 *  a
 				 */ 		
 				//System.out.println("		=== QActorHttpServer inputToEventMsg ATOM="+moveToDo);
 				eventMsg = "usercmd(executeInput("+moveToDo+"))";
 				return eventMsg;
			}
			}catch( Exception e1){
				/* The input  is NOT a Prolog term
  				 */ 				
				System.out.println("		=== QActorHttpServer  inputToEventMsg ERROR no term ="+moveToDo);
				//Not a Prolog term => Surround with ''
				try{
					moveToDo = moveToDo.replace("'", "\"");
					moveToDo = "'" + moveToDo + "'";
					eventMsg = "usercmd(executeInput("+moveToDo+"))";
					return eventMsg;
				}catch( Exception e2){
					//System.out.println("		=== QActorHttpServer inputToEventMsg ERROR ="+e2.getMessage());
					throw e2;
				}
			}
			return eventMsg;
	}
	
	protected static Term getGuard(Struct ms){
	//ms = a,b...
		Term t1 = ms.getArg(0);
		if( t1.isList() ){
//			Struct t1s = (Struct)t1;
//			Term g = t1s.getArg(0);
			return t1;
		}else return null;
	}
	
	protected static Term getDuration(Term t1){
		Term td = t1;
		if( t1 instanceof Struct && ((Struct)t1).getName().equals(",")){
			td = ((Struct)t1).getArg(0);
		}
 		if( td instanceof alice.tuprolog.Number ){
			return td;
		}else return null;		
	}
	protected static Term getE(Term t1){
		Term td = t1;
		if( t1 instanceof Struct && ((Struct)t1).getName().equals(",")){
			td = ((Struct)t1).getArg(0);
		}
 		if( td instanceof alice.tuprolog.Number ){
			return td;
		}else return null;		
	}
 	
/*
 * 	
 */
	protected WebSocket myws = null;
	protected int count = 0;
	protected String cmd = null;
	
	public class Ws extends WebSocket{
		public Ws(IHTTPSession handshakeRequest) {
			super(handshakeRequest);
  			outEnvView.addOutput("		=== QActorHttpServer connected ");
  			myws = this;
  			 
		}
		@Override
		protected void onPong(WebSocketFrame pongFrame) {
			outEnvView.addOutput("QActorHttpServer Ws  user pong.......");
		}
		@Override
		protected void onMessage(WebSocketFrame messageFrame) {
			try {
				cmd = messageFrame.getTextPayload();
				String eventMsg="noevent";
				try{
					handleUserCmd(cmd);
 				}catch(Exception e ){
 					//No platform available
 					outEnvView.addOutput("QActorHttpServer ERROR (Perhaps platform null) " + e.getMessage());
 					platform = ContactEventPlatform.getPlatform();
 					handleUserCmd(cmd);
				}
//				send("done " + cmd);				
			} catch (Exception e) {
				e.printStackTrace();
			}			
		}
		@Override
		protected void onClose(CloseCode code, String reason,
				boolean initiatedByRemote) {
			outEnvView.addOutput("QActorHttpServer Ws  user disconnected.....");			
		}
		@Override
		protected void onException(IOException e) {
			outEnvView.addOutput("QActorHttpServer Ws  ERROR "+e.getMessage());
		}		
	}
	
/*
* 	
*/
		public class EvhQActorHttpServer extends EventHandlerComponent{
			protected int ccc = 0;
			
			public EvhQActorHttpServer(String name, ActorContext myctx, String eventId,
					IOutputEnvView view) throws Exception {
				super(name, myctx, eventId, view);
				println("EvhCb CREATED name=" + getName()  );
			}
			
			public String getCmd(){
				return cmd;
			}
			public WebSocket getWs(){
				return myws;
			}
 
			@Override
			public void doJob() throws Exception {
				IEventItem event = getEventItem();
				if( event == null ){
					println("EvhCb doJob no event "  );
					return;
				}
				String msg = event.getEventId() + "|" + event.getMsg() + " from " + event.getSubj()   ;
				showMsg( msg );		
// 				println("EvhQActorHttpServer logo=" + cmd );
// 				myws.send("count=" + count++); 
 				
			}
			
		}
	
	

/*
 * For fast demo / debug	
 */
	
	public  static void main(String[] args)  {
		try {
			IOutputEnvView outEnvView = SituatedSysKb.standardOutEnvView;
			 		/*
			 * Create the server
			 */
			QActorHttpServer server = new QActorHttpServer(outEnvView,"./srcMore",8080);
			/*
			 * Create a Context to give a platform to QActorHttpServer
			 */
			InputStream sysKbStream    = new FileInputStream(
					"./src/it/unibo/qactors/web/sysWebTestKb.pl");
			InputStream sysRulesStream = new FileInputStream("sysRules.pl");
			new CtxWebTest("ctxwebtest",outEnvView,sysKbStream,sysRulesStream, server).configure();
			server.start();
			Thread.sleep(60000);
		} catch (Exception e) {
 				e.printStackTrace();
		}
 	}//main
}
