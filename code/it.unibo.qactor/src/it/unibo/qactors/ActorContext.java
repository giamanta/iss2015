/*
 * Defines the  properties of any context and utility methods
 * 
 * KEY-POINTS:
 * -) An application system is made of a collections of N subsystems (nodes) called Contexts
 * -) The application system is described by a declarative set of rules written in Prolog syntax. For example:
		context( ctx1, "192.168.43.229", "TCP", "8010" ).
		context( ctx2, "192.168.43.229", "TCP", "8020" ).		 
		qactor( qa1, ctx1 ).
		qactor( qa2, ctx2 ).
 * .) A context is the environment for QActors (Quasi-Actors)
 * .) A QActor has an unique name (lowercase) in the system and belongs to just one context
 * .) A context has a unique name (lowercase)  in the system and a unique communication port 
 * .) The context communication port is used by a connection-based communication protocol (TCP/UDP,...)
 * .) Each context activates one CtxServerAgent to receive messages from the other contexts
 * .) Each context activates N-1 SenderAgent to send qactor-messages to the other contexts
 * .) A qactor-message is structured as follows
   			msg( MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM )
 * .) The MSGTYPE  can be: dispatch request answer
 * .) The message payload (CONTENT) is a String built according to the rules of an application-dependent concrete syntax
 * .) The SEQNUM  is (the string representation of) a natural number increased by a context each time a new message is sent
 */
package it.unibo.qactors;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Iterator;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Theory;
import it.unibo.contactEvent.interfaces.IContactComponent;
import it.unibo.contactEvent.platform.ContactEventPlatform;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.is.interfaces.protocols.IConnInteraction;
import it.unibo.system.SituatedPlainObject;
import it.unibo.system.SituatedSysKb;

public abstract class ActorContext  extends SituatedPlainObject implements IActorContext{
/*
 * DEFINITIONS	
 */
	private  static int ncount = 0; 
	public static final String dispatch = "dispatch";
	public static final String request  = "request";
	public static final String answer   = "answer";
//-------------------------------------------------------------	
protected  boolean terminated = false;	
protected int ctxPort = 0;
public final InputStream sysKbStream;
public final InputStream sysRulesStream;
protected MsgInterpreter interpreter;	
protected Prolog prologEngine;	
protected int msgNum = 0;
protected Theory configTh ;
protected  Hashtable<String,SenderObject> ctxTable;
protected  Hashtable<String,QActor> actorTable;
protected  Hashtable<String,IConnInteraction> connectionTable;	 
protected  Hashtable<String,IContactComponent> contactComponentTable;
protected  String localTheoryRep= "";

	public ActorContext(String name, IOutputEnvView outEnvView,  
			InputStream sysKbStream, InputStream sysRulesStream ) throws Exception{
		super(name.toLowerCase(), outEnvView);
		this.sysKbStream      = sysKbStream;
 		this.sysRulesStream   = sysRulesStream;
 		init();
		println("ACTIVATED " + name + " ncores=" +  SituatedSysKb.numberOfCores + " port=" + ctxPort );
		println("===========================================================");
  	}	
	/*
	 * return a new count in mutual exclusive way
	 */
	public static synchronized int getCounter() throws Exception{
//		System.out.println(" ActorContext " + ncount);
		ncount++;
// 		Thread.sleep(ncount*1);
		return ncount;
	}

	protected void init() throws Exception{
//		println("ActorContext init" );
		SituatedSysKb.init();	//Allows us to restart an application like happens in Android
		ContactEventPlatform.getPlatform(this);	//initialize the event platform
		actorTable 				= new Hashtable<String,QActor>();
		contactComponentTable 	= new Hashtable<String,IContactComponent>();
		connectionTable			= new Hashtable<String,IConnInteraction>();
 		loadSystemTheory();		//TO RUN FIRST	 
		assertEvloopQActors( );
		activateTheServerAgent();
		activateSenderAgents();
	}
  	public  synchronized  void terminate() {
  		terminated = true;
//		SituatedSysKb.cleanAll();
  		ContactEventPlatform.reset();	//To avoid the reuse of the same platform in testing
  		println(""+ getName() + " terminated ") ;
    	notifyAll();
  	}
   	public synchronized boolean waitForTermination() {
  		try	{
			while( ! terminated ) wait();
			println(" waitForTermination done "+ getName()  ) ;
			outEnvView.getEnv().close();
			return true;
  		} catch (Exception e) {
  			e.printStackTrace();
   		};
			return false;
  	}
	public IOutputView getOutputView(){
		return this.outView;
	}
	public void resetConns(){
		connectionTable			= new Hashtable<String,IConnInteraction>();
	}
	public int newMsgnum(){
		msgNum++;
		return msgNum;
	}
	public Prolog getPrologEngine(){
		return prologEngine;
	}
	public void registerActor(String actorId, QActor actor){
// 		System.out.println("ActorContext REGISTERED " + actorId );
		actorTable.put(actorId, actor);
//		/* =====================================================
//		 * The qactors could work one at the time ...
//		 * =====================================================
//		 */
//		
//		if( actor instanceof EventAbstractComponent ){
//	  		println("ActorContext REGISTERED and ACTIVATED in one thhred " + actorTable.get(actorId).getName() );
//			actor.activate(SituatedSysKb.executorOneThread);
//		}
//		/* =====================================================
//		 * ... otherwise the qactors could work "in parallel"
//		/* =====================================================
//		 */
//		else{
//			actor.activate(SituatedSysKb.executorManyThread);
//	  		System.out.println("ActorContext REGISTERED and ACTIVATED " + actorTable.get(actorId).getName() );
//		}
	}
	public void registerContactComponent(String compId, IContactComponent comp){
		contactComponentTable.put(compId, comp);
// 		println("ActorContext REGISTERED IContactComponent " + compId  );
	}
	public MsgInterpreter getMsgInterpreter(){
		return interpreter;
	}
	protected void assertEvloopQActors( ) throws Exception{
//		println("assertEvloopQActors" );
 		Prolog prologEngine = this.getPrologEngine();
		SolveInfo sol   = prologEngine.solve("getCtxNames( CTXNAMES ).");	
		if( ! sol.isSuccess() ){
			updateEvlpa("ctxofhello");
		}else{
			Struct ctxList  = (Struct) sol.getVarValue("CTXNAMES");
			Iterator<? extends Term> it = ctxList.listIterator();
			while( it.hasNext() ){
				String curCtx      = ""+it.next();
				updateEvlpa(curCtx);
			}
		}
	}
	
	protected void updateEvlpa(String ctxName ) throws Exception{
		String evalLoopName = "evlpa"+ctxName;
		String goal = "insertActorOnce( " + evalLoopName + "," + ctxName + ").";
  		prologEngine.solve(goal);		
 	}

 	protected void activateTheServerAgent(){
//		println("activateTheServerAgent" );
		interpreter =  new MsgInterpreter(this,outView);//MsgInterpreter.getInstance(this,outView); //create the singleton
  		new CtxServerAgent(name+"_Server", this, outView,  ctxPort );		
	}
 	/*
 	 * For each known context activate a sender
 	 */
	public void activateSenderAgents() throws Exception{
//		println("activateSenderAgents" );
		ctxTable 	    = new Hashtable<String,SenderObject>();
		SolveInfo sol   = prologEngine.solve("getCtxNames( CTXNAMES ).");	
		if( !sol.isSuccess()){
			activateSenderToCtx("ctxofhello",true);
		}else{
			Struct ctxList  = (Struct) sol.getVarValue("CTXNAMES");
			Iterator<? extends Term> it = ctxList.listIterator();
			while( it.hasNext() ){
				String curOtherCtx =  ""+it.next();
				activateSenderToCtx(curOtherCtx,true);
			}
		}
	}
	protected void activateSenderToCtx(String ctxName, boolean propagate ) throws Exception{
//		println("activateSenderToCtx" );
		//contextFact = context( CTX, HOST,  PROTOCOL, PORT )
		//activate the sender object for the new remote context
 		int curPort   	   =  this.getCtxPort( ctxName );
// 		println("activateSenderToCtx " + ctxName +":" + curPort  );
		String curHostName =  this.getCtxHost( ctxName );
		curHostName 	   =  curHostName.replaceAll("'", "");
		String protocol    =  this.getCtxProtocol( ctxName );
		protocol           =  protocol.replaceAll("'", "");
		SenderObject sobj  =  ctxTable.get(ctxName) ;
		if( sobj != null ){ //sender already created
//			println(" *** ActorContext has found a sender for " + ctxName  );
			if( propagate ) sendUpdateSysKbpMsg( sobj );
			return;
		}
		if( ctxPort != curPort ){ // CHECK WHY (for actors in the same ctx?)
//			println(" *** ActorContext creates a new sender for " + ctxName  + 
//					" at " + curHostName+":"+ curPort + " with propagate="+propagate);
			println("activateSenderAgents towards the context -> " + ctxName);
			SenderObject sa = new SenderObject("sa_"+this.ctxPort, this, outView, protocol, curHostName, curPort );
			this.ctxTable.put(ctxName, sa);		
			if( propagate ) sendUpdateSysKbpMsg( sa ); 
		}
	}
	protected void sendUpdateSysKbpMsg(SenderObject sa) throws Exception{ 		
		String mout ="msg( MSGID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM )".
				replace("MSGID","updatesyskb").replace("MSGTYPE","dispatch").replace("SENDER","remotectx_"+name).
				replace("RECEIVER","qasystem").replace("CONTENT",localTheoryRep).replace("SEQNUM","0");
//		println("			*** sendUpdateSysKbpMsg " + mout);
		Thread.sleep(100);	//Just the time to activate sa
		sa.sendMsg(mout);
 	}

	protected void loadSystemTheory() throws Exception{
//		println("ActorContext loadSystemTheory" );
		prologEngine 	 = new Prolog();
  		configTh 		 = new Theory( sysKbStream );
  		Theory rulesTh   = new Theory( sysRulesStream );
		prologEngine.addTheory(configTh);
		prologEngine.addTheory(rulesTh);
		ctxPort          = getCtxPort( getName() );
		println("ActorContext " + getName() + ":" + ctxPort + " loadSystemTheory");
		createLocalTheoryRep( );
 	}
	protected void createLocalTheoryRep( ) throws Exception{
//		println("ActorContext createLocalTheoryRep" );
		localTheoryRep = "\"";
		SolveInfo sol   = prologEngine.solve("getTheContexts(CTXS).");	
//		println("ActorContext createLocalTheoryRep contexts sol=" + sol );
		if( ! sol.isSuccess() ) {
			localTheoryRep = localTheoryRep + "@context(ctxofhello,localhost,'TCP','8010')"   ;
		}else{
			Struct ctxList  = (Struct) sol.getVarValue("CTXS");
			Iterator<? extends Term> it = ctxList.listIterator();
			while( it.hasNext() ){
				String ctx = ""+it.next();
	   			println("			+++ " + ctx);
				localTheoryRep = localTheoryRep + "@" + ctx  ;
			}
		}
		sol   = prologEngine.solve("getTheActors(ACTORS).");	
//		println("ActorContext createLocalTheoryRep actors sol=" + sol );
		if( ! sol.isSuccess() ) {
			localTheoryRep = localTheoryRep + "@actor(qahello,ctxofhello)"   ;			
		}else{
			Struct actorList  = (Struct) sol.getVarValue("ACTORS");
			Iterator<? extends Term> ita = actorList.listIterator();
			while( ita.hasNext() ){
				String actor = ""+ita.next();
				if( actor.contains("evlpa")) continue;	//do not include  qactor( evlpaxxx, ... )
				println("			+++ " + actor);
				localTheoryRep = localTheoryRep + "@" + actor  ;
			}
		}
//		println("ActorContext createLocalTheoryRep localTheoryRep=" + localTheoryRep );
		localTheoryRep = localTheoryRep + "\"";
	}
	
	public abstract void configure();
	/*
	 * Utility methods
	 */
	public QActor getActor(String actorId){
		return actorTable.get(actorId);
	}
	public IContactComponent getContactComponent(String compId){
 		IContactComponent cc = contactComponentTable.get(compId);
		return cc;
	}
	public String getMsgReceiverActorId( String msg ) throws Exception{
//		println("getMsgReceiverActorId of " + msg  );
		SolveInfo sol  = prologEngine.solve( "getMsgReceiverId("+ msg +", ACTORID ).");	
		if( ! sol.isSuccess() ) return "unknown";
		String actorId = ""+sol.getVarValue("ACTORID");
// 		System.out.println("receiveactorId of " + msg + " = " + actorId);
		return actorId;		
	}
	public String getMsgSenderActorId( String msg ) throws Exception{
		//println("getMsgSenderActorId of " + msg  );
		SolveInfo sol  = prologEngine.solve( "getMsgSenderId("+ msg +", ACTORID ).");	
		if( ! sol.isSuccess() ) return "unknown";
		String actorId = ""+sol.getVarValue("ACTORID");
		//println("actorId of " + msg + " = " + actorId);
		return actorId;		
	}
	public String getMsgId( String msg ) throws Exception{
		//println("getMsgId of " + msg  );
		SolveInfo sol  = prologEngine.solve( "getMsgId("+ msg +", MSGID ).");	
		String msgId = ""+sol.getVarValue("MSGID");
		//println("msgType of " + msg + " = " + msgId);
		return msgId;		
	}
	public String getMsgType( String msg ) throws Exception{
//		println("getMsgType of " + msg + " prologEngine=" + prologEngine );
		SolveInfo sol  = prologEngine.solve( "getMsgType("+ msg +", MSGTYPE ).");	
		if( ! sol.isSuccess() ) return "unknown";
		String msgType = ""+sol.getVarValue("MSGTYPE");
//		println("msgType of " + msg + " = " + msgType);
		return msgType;		
	}
	public QActor getReceiverActor( String msg ) throws Exception{
		String actorId = getMsgReceiverActorId(   msg );
// 		System.out.println("receiveactorId of " + msg + " = " + actorId + " " + actorTable.size()  );
		return getActor(actorId);
	}
	public String getContentMsg( String msg ) throws Exception{
		//println("getContentMsg of " + msg  );
		SolveInfo sol  = prologEngine.solve( "getMsgContent("+ msg +", MSGCONTENT ).");	
		if( ! sol.isSuccess() ) return "unknown";
		String msgContent = ""+sol.getVarValue("MSGCONTENT");
 		return msgContent;		
	}
	public IConnInteraction getConnection( String key ) throws Exception{
 		return connectionTable.get(key);
	}
	public SenderObject getSenderAgent(String actorId) throws Exception{
		String ctxName = getActorCtx(actorId);
		return ctxTable.get(ctxName);
	}
	protected String getActorCtx(String actorId) throws Exception{
//		println("			getActorCtx of " + actorId  );
		SolveInfo sol = prologEngine.solve("qactor( " + actorId +", CTX ).");	
		if( ! sol.isSuccess() ) return "unknown";
		String ctxName = ""+sol.getVarValue("CTX");
		//println("ctxName of " + actorId + " = " + ctxName);
		return ctxName;
 	}
	protected String getCtxProtocol(String ctxId) throws Exception{
		SolveInfo sol = prologEngine.solve("getCtxProtocol( " + ctxId +", PROTOCOL ).");	
		if( ! sol.isSuccess() ) return "unknown";
		String protocolName = ""+sol.getVarValue("PROTOCOL");		
		//println("protocolName of " + ctxId + " = " + protocolName);
		return protocolName;
 	}
	protected String getCtxHost(String ctxId) throws Exception{
		SolveInfo sol = prologEngine.solve("getCtxHost( " + ctxId +", HOSTNAME ).");	
		if( ! sol.isSuccess() ) return "unknown";
		String hostName = ""+sol.getVarValue("HOSTNAME");
		//println("hostName of " + ctxId + " = " + hostName);
		return hostName;
 	}
 	protected int getCtxPort(String ctxId) throws Exception{
// 		println("getCtxPort of " + ctxId  );
		SolveInfo sol = prologEngine.solve("getCtxPort( " + ctxId +", PORTNAME ).");	
		if( ! sol.isSuccess() ) return 8010;
		String portStr = ""+sol.getVarValue("PORTNAME");
		portStr = portStr.replaceAll("'", "");
		int port = Integer.parseInt(portStr);
//		println("port of " + ctxId + " = " + port);
		return port;
 	}
}
