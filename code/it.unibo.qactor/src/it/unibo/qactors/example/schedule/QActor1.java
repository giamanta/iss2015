/*
 * This actors prints a sequence of messages on the standard output port.
 */
package it.unibo.qactors.example.schedule;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.QActor;

public class QActor1 extends QActor{

	public QActor1(String actorId, ActorContext myCtx, IOutputEnvView outEnvView) {
		super(actorId, myCtx, outEnvView);
 	}

 	@Override
	protected void doJob() throws Exception {
 		for(int i=1; i<=5; i++){
 			autoMsg(i);
 			Thread.yield();
 		}
  	}	
	protected void autoMsg(int i) throws Exception{
		println(getName() + " sends " + i );
		sendMsg("info", getName(), ActorContext.dispatch, "autoMsg" );
		String msg = receiveMsg();
		String senderId = myCtx.getMsgSenderActorId(msg);
		println(getName() + " received " + msg + " from " + senderId );
		
	}
}
