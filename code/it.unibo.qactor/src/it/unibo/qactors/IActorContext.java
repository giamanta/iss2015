package it.unibo.qactors;

public interface IActorContext {
	public SenderObject getSenderAgent( String destAgent ) throws Exception;
}
