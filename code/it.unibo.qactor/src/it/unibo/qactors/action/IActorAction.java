package it.unibo.qactors.action;
import it.unibo.contactEvent.platform.ContactEventPlatform;
 
public interface IActorAction extends IAsynchAction<AsynchActionGenericResult<String>>{	 
	public final String endBuiltinEvent=ContactEventPlatform.locEvPrefix+"endBtIn";
	public final boolean suspendPlan  = false;
	public final boolean continuePlan = true;
 	
	public enum ActionRunMode{
		terminationEventAtEnd(0, "terminationEventAtEnd"),
		terminationEventImmediate(1, "terminationEventImmediate");
		private int runMode;
		private String name;	
 		private ActionRunMode(int runMode, String name) {
			this.runMode = runMode;
			this.name = name;	
 		}
	}
	public enum ActionExecMode{
		synch(0, "synch"),
		asynch(1, "aynch");
		private int value;
		private String name;	
 		private ActionExecMode(int value, String name) {
			this.value = value;
			this.name = name;
 		}
	}
	public enum ActorActionType{
		 photo(0,   "photo"),
		 video(1,   "video"), 
		 sound(2,   "sound"),	
		 receive(3, "receive"),	
		 move(4,    "move"),	
		 basic(5,   "basic"),
		 application(6,   "application"),
		 emit(7,   "emit"),
		 userdef(8, "userdef"),
		 forward(9,    "forward"),	
		 request(10,    "request"),
		 solve(11,    "solve");
		 
			private int value;
			private String name;	
			private ActorActionType(int value, String name) {
				this.value = value;
				this.name = name;
			}
			public int getValue() {
				return value;
			}
			public String getName() {
				return name.toLowerCase();
			}	
			public static ActorActionType getByName(String name){
				for (ActorActionType cValue : ActorActionType.values()) {
					if (cValue.getName().equalsIgnoreCase(name.trim())) {
						return cValue;
					}
				}
				return null;
			}
		}//enum
	

}
