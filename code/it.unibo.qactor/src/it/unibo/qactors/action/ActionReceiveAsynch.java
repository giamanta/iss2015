package it.unibo.qactors.action;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Term;
import it.unibo.contactEvent.interfaces.IActorMessage;
import it.unibo.contactEvent.platform.ContactEventPlatform;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActor;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.web.GuiUiKb;

public class ActionReceiveAsynch extends ActorAction implements IActorAction{ 
protected static int n = 1;
protected  QActor actor;
protected  String msg = null;
protected String msgTermStrToReceive;
protected Prolog pengine;
protected Term termToReceive = null;
protected boolean msgReceived;
protected boolean timeOut;
protected Thread threadOfReceive;

	public ActionReceiveAsynch(String name, QActor actor, String msgTermStrToReceive, IOutputEnvView outView,
			long maxduration ) throws Exception {
		super(name, false, ContactEventPlatform.locEvPrefix+"EndEv"+n++, "", outView, maxduration);
		this.actor            = actor;
		/*
		 * msgTermToReceive could be:
		 * 	msg( MID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM  )
		 *  or
		 *  [mid1, ..., midn]
		 */
		this.msgTermStrToReceive = msgTermStrToReceive;
		pengine                  = actor.getPrologEngine();
 		msgReceived              = false;
 		timeOut 				 = false;
		if( ! msgTermStrToReceive.trim().equals("null")){
			this.msgTermStrToReceive = msgTermStrToReceive;
			termToReceive         = Term.createTerm(msgTermStrToReceive);
		}
//		println("--- ActionReceiveAsynch msgTermStrToReceive= "  + msgTermStrToReceive );
  	}
	
	
	public String getReceivedMsg() throws Exception{
		if( timeOut ) return "msg(none,none,none,none,none,0)";
		removeTheMessage( termToReceive.toString() );
		return termToReceive.toString(); 
	}
	/*
	 * This operation is called by TimedActionGeneric when activated
	 * The action at application level performs two main tasks
	 * 1) waits in the current Callable for maxDuration
	 * 2) activates a new Thread that attempts to capture the required
	 *    (msgTermStrToReceive) message by Inspect the world and the
	 *    actor message queue. If no message exists, it waits for
	 *    messages on the actor message queue and stored (memo) any not unifiable
	 *    message in the actor WorldThery
	 *    
  	 */
	@Override
	protected void execTheAction() throws Exception {
	 try{
//  	 	    println("--- ActionReceiveAsynch creates and starts a receiver thread "   );
	 		//Create a new thread for the blocking action
		 
			 		threadOfReceive = new Thread(){
			 			public  void run(){
		 		 			try {
		 		 				//Inspect the world 		 			
		  		 			if( lookInTheWorld() ) return;
		  		 				//Inspect the actor message queue
		 		 			if( lookInTheQueue() ) return;
			 		 		while( ! msgReceived ){
// 		 		 		 			println("--- ActionReceiveAsynch threadOfReceive waits for a message for:" +  actor.getName()  );
									msg = actor.receiveMsg();
// 		 		 		 			println("--- ActionReceiveAsynch threadOfReceive msg=" + msg  ); 		 		 			
		 		 		 			if( handleMsg( msg ) ){
		 		 		 				msgReceived = true;
  		   		 		 				suspendAction(); //suspend the sleep before the tout
		 		 		 			};		 		 			
			 		 		}//while	 				
							} catch (Exception e) {
								//println("--- ActionReceiveAsynch INTERRUPTED "  + e.getMessage() );
								return;
							}  
//			 		 		println("--- ActionReceiveAsynch has found a message "   );
//			 		 		suspendAction(); //suspend the dosleep before the tout
			 			}//run
			 		};
//
// 			println("--- ActionReceiveAsynch starts   "   );
 			threadOfReceive.start();	 		
	 /*
	 * Sleep to handle the timeout 
	 * This sleeping should be interrupted if a messages arrives
	 */	

 	 		//println("--- ActionReceiveAsynch SLEEPS "  +maxduration );
	 		if( this.dosleep(maxduration) ){ 
 	 			//println("--- ActionReceiveAsynch " + msgTermStrToReceive + " WAKES UP after "  +maxduration );
	 			if( ! msgReceived ){
		 			//the action has terminated the time msgReceived should be false
	 		 		timeOut = true;
 			 		threadOfReceive.interrupt();
	 			}
	 		}
	 		;
   	 }catch(Exception e){
   		 e.printStackTrace();
		 println("--- ActionReceiveAsynch ERROR " +  e.getMessage() );
	 }
    } 
	
	protected  synchronized boolean lookInTheWorld() throws Exception{
		if( checkAMsgInWorld() ){
//   	 		println("--- ActionReceiveAsynch has found a message in the world "   );
			suspendAction();
			return true;
		}else return false;		
	}
	protected  synchronized boolean lookInTheQueue() throws Exception{
 		if( removeAMsg( ) ){
//  	 		println("--- ActionReceiveAsynch has found a message in the queue "   );
 			suspendAction();
 			return true;
 		}else return false;			
	}
	
	protected boolean checkAMsgInWorld( ) throws Exception{
		SolveInfo sol = pengine.solve("msg( MID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM  ).");
//    	println("--- ActionReceiveAsynch checkAMsgInWorld " +  sol.isSuccess() );
		if( sol.isSuccess() && 
			(termToReceive==null || pengine.unify(sol.getSolution(), termToReceive) )){
//			removeTheMessage(msgTermStrToReceive);
//			println("--- ActionReceiveAsynch checkAMsgInWorld msgTermStrToReceive=" + msgTermStrToReceive + " termToReceive="+ termToReceive );
			msgReceived = true;
			return true;
		}
		return false;
	}
	/*
	 * Removes FIFO all the messages (and puts them in the actor's world theory)
	 * from  the msgqueue  until the required one is found (it it exists)
	 */
	protected synchronized boolean removeAMsg( ) throws Exception{
		while( this.actor.getQueueSise()>0 ){
			msg = this.actor.receiveMsg(); //blocking => not reactive REMOVES the message
			if( handleMsg(msg) ){
				msgReceived = true;
				return true ;
			}
 		}
//		println("--- ActionReceiveAsynch removeAMsg fails with msgqueue size=" +  actor.getQueueSise() );
		return  false ;
	}
 
 

	protected  boolean handleMsg(String msg){ //to avoid re-rentrance by  ExecReceive
		Term msgTerm = null;
		try{
//  			println("--- ActionReceiveAsynch handleMsg1 "  +  msg  );	
	 		if( msg == null ) return false;
 	 		msg = GuiUiKb.buildCorrectPrologString(msg);
//			println("--- ActionReceiveAsynch handleMsg2 "  +  msg  );	
  			IActorMessage m = 
					new QActorMessage(msg); //to check the syntax
  			
  			addTheMessageInWorldTheory( msg );		//puts the removed messages in the actor world theory
 			msgTerm = Term.createTerm(msg);			 
//  			println("--- ActionReceiveAsynch handleMsg msgTerm="  +  msgTerm  );	
//  			println("--- ActionReceiveAsynch handleMsg termToReceive="  +  termToReceive + " " + pengine.unify(msgTerm, termToReceive)  );	
			return( termToReceive==null || checkIfMessageExpected(msgTerm, termToReceive) ) ;
  		}catch( Exception e){
			println("--- ActionReceiveAsynch ERROR "  +  e.getMessage() + " msg=" + msg + " msgTerm=" + msgTerm);	
			return false;
		}
	}
	
	protected boolean checkIfMessageExpected(Term msgTerm, Term termToReceive) throws Exception{
		if( termToReceive.isList() ){
			SolveInfo sol = pengine.solve( "checkMsg("+ msgTerm +"," + termToReceive + ")." );
 			return sol.isSuccess();
		}
		else return pengine.unify( msgTerm, termToReceive );
	}
	
	protected void addTheMessageInWorldTheory( String msg  ){
 		try{
//  			println("addTheMessage:" + msg  );
 			msg = msg.trim();
 			if( msg.equals("true")) return;
//  			SolveInfo sol = 
  					pengine.solve( "addRule( " + msg + " ).");
//  			println("			--- ActionReceiveAsync addTheMessageInWorldTheory done " + msg  );
   		}catch(Exception e){
  			println("addTheMessageInWorldTheory " + msg + " ERROR:" + e.getMessage() );
   		}
   	}
	protected  void removeTheMessage( String msg  ){
 		try{
// 			if( msg.startsWith("'") || msg.startsWith("\"")) 	msg = msg.substring(1, msg.length()-1);
//  			println("removeTheMessage:" + msg  );
 			msg = msg.trim();
 			if( msg.equals("true")) return;
//  			SolveInfo sol = 
  					pengine.solve( "removeRule( " + msg + " ).");
// 			println("removeTheMessage done " + msg  );
   		}catch(Exception e){
  			println("removeTheMessage " + msg + " ERROR:" + e.getMessage() );
   		}
   	}

//	@Override
//	public void suspendAction(){
//		try{
//  			println("--- ActionReceiveAsynch  suspendAction "   );
//			if(threadOfReceive != null) threadOfReceive.interrupt();
//		}catch(Exception e){
//			System.out.println("--- ActionReceiveAsynch  suspendAction ERROR " +  e.getMessage() );
//		}
//	}


	@Override
	protected String getApplicationResult() throws Exception {
//		println("--- ActionReceiveAsynch getApplicationResult = " + msg );	
		return msg;
	}
	
 }
