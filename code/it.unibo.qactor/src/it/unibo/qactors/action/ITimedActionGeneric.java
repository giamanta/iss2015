package it.unibo.qactors.action;
import it.unibo.iot.interfaces.ITimedAction;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public interface ITimedActionGeneric<T> extends Callable<T>, ITimedAction {
	/*
	 * Activate the action by using some Executor
	 */
	public Future<T> activate() throws Exception;
	/*
	 * Return a representation of the action definition
	 */
 	public String getActionRep();
	/*
	 * Return a representation of the action definition
	 * together with a a representation of the result
	 */
 	public String getActionAndResultRep() throws Exception;
}
