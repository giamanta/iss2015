package it.unibo.qactors.action;
import it.unibo.contactEvent.platform.ContactEventPlatform;
import it.unibo.is.interfaces.IOutputEnvView;
 
 
 
/*
 * Each ActionDummy action must generate a different termination event
 */
public class ActionDummy extends ActorAction implements IActorAction{
private static int ncount = 0;
protected boolean timeOut;
 	public ActionDummy( IOutputEnvView outView, int maxduration  ) throws Exception {
 		super("adummy"+getCounter(), false, ContactEventPlatform.locEvPrefix+"dmyend"+getCounter(), "", outView, maxduration); //adterminate never emitted null IS A MUST
//		super("admy"+ncount++, false, ContactEventPlatform.locEvPrefix+"dmyend"+ncount++, "null", outView, maxduration); //adterminate never emitted null IS A MUST
 	}
	public static synchronized int getCounter() throws Exception{
// 		System.out.println("=== ActionDummy " + ncount);
 		ncount++;
		return ncount;
	}
	@Override
	public void execAction() throws Exception {
 // 	 	println("%%% "+getName() + " delay " + maxduration);
 			boolean exit = dosleep(maxduration);	
 			timeOut = exit;
   			//println("%%% "+getName() +  " exit " + exit + "  durationMillis=" + durationMillis );
  	}
	@Override
	protected void execTheAction() throws Exception {
		execAction();
		
	}
	@Override
	protected String getApplicationResult() throws Exception {
//		println("%%% ActionDummy "+getName() +  " getApplicationResult "  );
		return "timeRemained(" + durationMillis +")";
	}
 

}
