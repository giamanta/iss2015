package it.unibo.qactors.action;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Term;
import it.unibo.contactEvent.interfaces.IActorMessage;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActor;
import it.unibo.qactors.QActorMessage;

/*
 * A receive action that looks for a unifiable message in the world theory
 * If there is no message, it looks at the actor message queue
 * If there a message in the actor message queue it extracts the message
 * and puts the message in the world theory, by ending when the queue is empty
 * or when a unifiable message is found
 * If no message is found (the message queue is empty) it starts a receiver thread
 * that handles the incoming messages abd
 * terminates the action as soon an unifiable message is received
 * 
 * NOTE: the unify operation modifies the arguments
 */
public class ActionReceiveNaive extends ActorAction{
protected ExecReceive executor;
protected QActor actor;
protected Prolog pengine;
protected String msgToReceive = null;
protected Term termToReceive = null;
protected boolean nomsgYet= true;
	public ActionReceiveNaive(String name, QActor actor, boolean cancompensate,
			String terminationEvId, String answerEvId, IOutputEnvView outView,
			long maxduration , String msgToReceive ) throws Exception {
		super(name, cancompensate, terminationEvId, answerEvId, outView, maxduration);
//		println(getName() + " ActionReceive CREATING for "  + 
//				actor.getName() + " termToReceive=" + termToReceive + " maxduration=" + maxduration);
		this.actor = actor;
		pengine    = actor.getPrologEngine();
		if( ! msgToReceive.trim().equals("null")){
			this.msgToReceive = msgToReceive;
			termToReceive     = Term.createTerm(msgToReceive);
		}
//		println(getName() + " ActionReceive CREATED for "  + 
//				actor.getName() + " termToReceive=" + termToReceive + " maxduration=" + maxduration);
   	}
	/*
	 * The action cannot block itself on a actor.receiveMsg()
	 * since it is no more reactive
	 * Thus, we delegate a blocking receive to another Thread that
	 * musts be interrupted as soon as the action is suspended
	 *  
	 */
	@Override
	public void execAction() throws Exception {
//		println(" ActionReceive STARTS " + termToReceive + " msgqueue size=" + actor.getQueueSise()  );
		/*
		 * Inspect the world
		 */
		if( checkAMsgInWorld() ) return;
		/*
		 * Inspect the actor message queue
		 */
 		if( removeAMsg( ) ){
// 			println("ActionReceive has found a message in the queue "   );
 			return;
 		}
 		/*
 		 * No message available.
 		 * Create a new thread in order to admit timeout
 		 */
		executor = new ExecReceive();
 		executor.exec(this, this.actor );
 		dosleep(maxduration);
 		workAfterTimeoutPossiblyInterrupted();
  	}
	protected synchronized void workAfterTimeoutPossiblyInterrupted(){
		if( ! suspended ) {
//			println("ActionReceive END OF TIME " +  maxduration + " suspended=" + suspended);
	 		nomsgYet=false;
			this.notifyAll();
			termToReceive = null;
	 		suspendAction();	
		}
	}
	/*
	 * Called by an entity that does not use asyncActions (events) and always sets msgToReceive
	 * See receiveMsg of QActor
	 */
	public synchronized String getTheReceivedMessage() throws Exception{
		while( nomsgYet ) wait();
		//Here termToReceive should be unified with a message
		//println("ActionReceive getTheReceivedMessage after wait " +  termToReceive );
		if( termToReceive != null ){
			//Consume the message and return
			removeTheMessage(termToReceive.toString());
			return termToReceive.toString();
		}
		else{
 			return null;
		}
	}
	
	protected boolean checkAMsgInWorld( ) throws Exception{
		SolveInfo sol = pengine.solve("msg( MID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM  ).");
//  		println("ActionReceive checkAMsgInWorld " +  sol.isSuccess() );
		if( sol.isSuccess() && 
			(termToReceive==null || pengine.unify(sol.getSolution(), termToReceive) )){
			return true;
		}
		return false;
	}
	protected boolean removeAMsg( ) throws Exception{
		while( this.actor.getQueueSise()>0 ){
			String msg = this.actor.receiveMsg(); //blocking => not reactive
			if( handleMsg(msg) ) return true ;
 		}
//		println("ActionReceive removeAMsg fails with msgquae size=" +  actor.getQueueSise() );
		return  false ;
	}
 
	protected synchronized boolean handleMsg(String msg){ //to avoid re-rentrance by  ExecReceive
		Term msgTerm = null;
		try{
//  		  	println("ActionReceive handleMsg "  +  msg  );	
	 		if( msg == null ) return false;
		 	if( msg.startsWith("'")) 	msg = msg.substring(1, msg.length()-1);
			IActorMessage m = new QActorMessage(msg); //to check the syntax
			//copy the message in WorldTheory
			addTheMessage( msg );	
			msgTerm = Term.createTerm(msg);			 
			if( termToReceive==null || pengine.unify(msgTerm, termToReceive) ){
//			  	println("ActionReceive matches "  +  termToReceive );	
	  			nomsgYet = false;
	  			this.notifyAll();
				return true;
			}
			else return false;
		}catch( Exception e){
			println("ActionReceive ERROR "  +  e.getMessage() + " msg=" + msg + " msgTerm=" + msgTerm);	
			return false;
		}
	}
	
 
	
	protected  synchronized void addTheMessage( String msg  ){
 		try{
// 			println("addTheMessage:" + msg  );
 			msg = msg.trim();
 			if( msg.equals("true")) return;
//  			SolveInfo sol = 
  					pengine.solve( "addRule( " + msg + " ).");
// 			println("addTheMessage done " + msg  );
   		}catch(Exception e){
  			println("addTheMessage " + msg + " ERROR:" + e.getMessage() );
   		}
   	}
	protected void removeTheMessage( String msg  ){
 		try{
  			//println("removeTheMessage:" + msg  );
 			msg = msg.trim();
 			if( msg.equals("true")) return;
//  			SolveInfo sol = 
  					pengine.solve( "removeRule( " + msg + " ).");
 			//println("removeTheMessage done " + msg  );
   		}catch(Exception e){
  			println("removeTheMessage " + msg + " ERROR:" + e.getMessage() );
   		}
   	}
	@Override
	protected void execTheAction() throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected String getApplicationResult() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

 }
/*
 * -------------------------------------
 * ExecReceive
 * -------------------------------------
 */
class ExecReceive extends Thread{
	private ActionReceiveNaive ar;
	private QActor actor;
 	public void exec(ActionReceiveNaive ar, QActor actor ){
		this.ar    = ar;
		this.actor = actor;
  		start();
	}
	/*
	 * A msg sent to QActor is inserted in the actor queue
	 * and extracted by a receiveMsg, that is a blocking operation
	 *  
	 */
	public void run(){
		while(true){
			try { 
   				if( isInterrupted() || ar.isSuspended() ) break;
//				System.out.println( "ExecReceive WAITS for a message via actor= " + actor.getName() );
   				String msg = actor.receiveMsg(); //blocking => not reactive
// 				System.out.println("ExecReceive received ... "  + msg + " isInterrupted " + isInterrupted() );	
   				if( isInterrupted() || ar.isSuspended() ) break;
				boolean b = ar.handleMsg(msg) ;
//				System.out.println("ExecReceive " + msg + " b ="  + b );
				if( b ){
					ar.suspendAction();
					break;
				}				
			} catch (Exception e) {
				System.out.println("ExecReceive " + this + " ERROR " +  e.getMessage() );
				//Attempts another receive if not interrupted/suspended
			}	
		}//while
//		System.out.println( "ExecReceive " + this + " TERMINATES "   );
	}
}