package it.unibo.qactors.action;

public interface IActionHandler {

	public String getHandlerName(); 
	public void addEventId(String evId );

}
