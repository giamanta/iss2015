package it.unibo.qactors.action;
import java.util.concurrent.Future;

import it.unibo.qactors.action.IActorAction.ActionRunMode;

public interface IAsynchAction<T> extends ITimedActionGeneric<T> {
	public String getActionName(); 
	public void setTheName(String name); 
	public void setAnswerEventId(String evId); 
	public void setCanCompensate(boolean b); 
	public void setTerminationEventId(String evId);
	public void setMaxDuration(int d);	
 	public boolean canBeCompensated();
 	public void suspendAction();
 	public boolean isSuspended();
	//return the name of event to be emitted at completion
	public String getTerminationEventId(); 
	//return the name of event to be emitted as answer
	public String geAnswerEventId(); 
	//return the default representation of the action imported from ITimedActionGeneric
	public int  getMaxDuration();
	public void showMsg(String msg);
	public T execSynch() throws Exception;
	public Future<T> execASynch() throws Exception;
	public T waitForTermination() throws Exception ;
 	public ActionRunMode getExecMode();

}
