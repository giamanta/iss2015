package it.unibo.qactors.action;
import java.util.Calendar;
import java.util.concurrent.Future;

import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.action.IActorAction.ActionRunMode;
/*
 * -------------------------------------------------------------------------
 * 					AsynchActionGeneric
 * Author: AN DISI
 * Goal:    define a (compensated) TimedActionGeneric action with a prefixed maximum duration that 
 * 			at the end  emits a (unique) termination event  or a (unique) answer event
 * 			
 * Usage:  
 * 			if answerEvId is empty("") it emits terminationEvId at the end
 * 			if answerEvId is not empty it emits IMMEDIATEDELY terminationEvId and answerEvId at the end
 * 		execSynch:		executes the action in synchronous way (waits for termination)
 * 		execAsynch:		executes the action in asynchronous way
 * 		suspendAction: 	suspends the execution of the action if not already terminated
 * 		waitForTermination: waits until the action is terminated (mainly for execAsynch)
 *  	getExecMode:	returns an instance of ActionRunMode (termination with answer or not)
 * 
 * Implementation details:
 * 		Callable<T>, Future<T>, SituatedSysKb.executorManyThread
 *  	execTheAction: 	protected abstract operation that defines the action behavior
 *      getApplicationResult: protected abstract operation that defines the result (of type T9 of the action
 *  	getResult:		returns an instance of AsynchActionGenericResult<T> the wraps the application
 *  					result of the action into a structure that gives aother information about the action
 *  					(interrupted or not, execution time remained)
 *  					
 * -------------------------------------------------------------------------
 */

public abstract class AsynchActionGeneric<T> extends TimedActionGeneric<AsynchActionGenericResult<T>> 
							implements IAsynchAction<AsynchActionGenericResult<T>>{ 
	protected String arg; 
 	protected boolean actionTerminated = false;
	protected String answerEvId = "";
	protected String answer     = "";
	protected boolean cancompensate;
	protected  int maxduration;
	protected boolean suspended = false;
	protected String emptys = "";
	
	public AsynchActionGeneric(String name, boolean cancompensate ,
			String terminationEvId, String answerEvId, IOutputEnvView outEnvView, long  maxduration) throws Exception {
		super(name, terminationEvId, outEnvView);
		if( terminationEvId==null || answerEvId==null ) throw new Exception("AsynchActionGeneric cannont have event names == null");
		else if( terminationEvId.equals("null") || answerEvId.equals("null") ) throw new Exception("AsynchAction cannont have event names == null");
 		this.answerEvId    = answerEvId.trim();
		this.cancompensate = cancompensate;
 		this.maxduration   = (int) maxduration;
 	}
/*
 *  START
*/
	@Override
	protected void startOfAction() throws Exception {
		super.startOfAction();
		suspended = false;
		if( answerEvId.length() > 0) emitEvent(terminationEvId);	//terminates immediately
	}
/*
 * EXECUTE
 * To be defined by the application designer
 */
	@Override
	protected abstract void execTheAction() throws Exception;
//	@Override
	protected abstract T getApplicationResult() throws Exception;
/*
 *  END OF ACTION
 */
	/*
	 * Calculate action execution time
	 */
    @Override
	protected AsynchActionGenericResult<T> endActionInternal() throws Exception{
		long tEnd = Calendar.getInstance().getTimeInMillis();
		durationMillis =  tEnd - tStart ;	
		if( answerEvId.length() == 0 ) platform.raiseEvent(getName(), terminationEvId, ""+durationMillis );
		return endOfAction();
	}
	@Override
	protected AsynchActionGenericResult<T> endOfAction() throws Exception {
 		return getResult();
	}

	protected AsynchActionGenericResult<T> getResult() throws Exception {
		int timeRemained = (int) (maxduration - durationMillis) ;
		if( timeRemained < 0 ) timeRemained = 0;
		AsynchActionGenericResult<T> aar;
		if( answerEvId.length()>0 ){
			platform.raiseEvent(getName(), answerEvId, ""+durationMillis );   
			aar= new AsynchActionGenericResult<T>( this, getApplicationResult(), timeRemained, this.suspended );
		}else
			aar= new AsynchActionGenericResult<T>(this, getApplicationResult(), timeRemained, this.suspended );
		//println( getName() + "AsycnActionGeneric  getResult " + aar );
		return aar;
	}
/*
* ======================================================================== 
* METHODS
* ======================================================================== 
*/
	@Override 
	public AsynchActionGenericResult<T> execSynch() throws Exception { 
		fResult = activate();
		return fResult.get();
 	}

	@Override
	public Future<AsynchActionGenericResult<T>> execASynch() throws Exception {
		fResult = activate();
		return fResult;
 	}
	
	/*
	* ======================================================================== 
	* (BEAN) METHODS
	* ======================================================================== 
	*/
	@Override 
	public void setTheName(String name){
		this.name=name;
	}
	@Override
	public void setMaxDuration(int d){
		maxduration =  d;
		suspended = false; //the action is resumed => it is no more interrupted
	}
	@Override
	public void setAnswerEventId(String evId){
		answerEvId = evId;
	}
	@Override
	public void setCanCompensate(boolean b){
		cancompensate = b;
	}
	@Override
	public void setTerminationEventId(String evId){
		terminationEvId = evId;
	}
	@Override
	public String geAnswerEventId(){
		return answerEvId;
	}
	@Override
	public boolean canBeCompensated(){
		return cancompensate;
	}
	@Override
	public boolean isSuspended(){
		return suspended;
	}
	public void showMsg(String msg){
		this.println(msg);
	}
 	@Override
	public String getActionName() {
		return name;
	}
	@Override
	public int getMaxDuration() {
 		return this.maxduration;
	}
 	@Override
	public ActionRunMode getExecMode() {
 		if( answerEvId.length() == 0) return ActionRunMode.terminationEventAtEnd;
 		else return ActionRunMode.terminationEventImmediate;
	}
/*
 * 	REPRESENTATION
 */
 	@Override
	public String getActionAndResultRep() throws Exception{
	AsynchActionGenericResult<T> result = fResult.get();
 		return "action(NAME,TERMINATION,DURATION, RESULT, CANCOMPENSATE, ANSWEREVENT, MAXTIME, TERMINATED, SUSPENDED, ACTIONROUT)".
				replace("NAME",  getName()).
				replace("TERMINATION", terminationEvId ).
				replace("DURATION", ""+durationMillis ).
				replace("RESULT", "result('"+result+"')").
				replace("CANCOMPENSATE", "compensate("+this.cancompensate+")").
 				replace("ANSWEREVENT", "answerEv('"+ (answerEvId.length()>0 ? answerEvId : "noevent") +"')").
				replace("MAXTIME", "maxduration("+this.maxduration+")").
				replace("TERMINATED", "actionTerminated("+this.actionTerminated+")").
				replace("SUSPENDED", "suspended("+this.suspended+")").
				replace("ACTIONROUT", "answer('"+this.answer+"')") 
				;
	}
	@Override
	public String getActionRep()  {
		return "action(NAME,TERMINATION,CANCOMPENSATE,ANSWEREVENT,MAXTIME)".
				replace("NAME",  getName()).
				replace("TERMINATION", terminationEvId ).
				replace("CANCOMPENSATE", "compensate("+this.cancompensate+")").
 				replace("ANSWEREVENT", "answerEv("+ (answerEvId.length()>0 ? answerEvId : "noevent") +")").
				replace("MAXTIME", "maxduration("+this.maxduration+")")
 				;
	}	 	
/*
 * =========================================================================== 
 * Called by another thread
 * ===========================================================================
 */
	@Override
	public AsynchActionGenericResult<T> waitForTermination() throws Exception{
 		return fResult.get();
	}
	public void suspendAction(){
		if( ! actionTerminated && ! suspended && myself != null ){
 			//println("%%% AsynchAction " + getName() + " suspendAction in"  + myself.getName() );		
 			myself.interrupt();    			
  			suspended = true;
		}
	}

}
