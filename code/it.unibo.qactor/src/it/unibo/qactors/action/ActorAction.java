package it.unibo.qactors.action;

import it.unibo.is.interfaces.IOutputEnvView;


public abstract class ActorAction extends AsynchActionGeneric<String> implements IActorAction{

	public ActorAction(String name, boolean cancompensate,
			String terminationEvId, String answerEvId,
			IOutputEnvView outEnvView, long maxduration) throws Exception {
		super(name, cancompensate, terminationEvId, answerEvId, outEnvView, maxduration);
  	}

}
