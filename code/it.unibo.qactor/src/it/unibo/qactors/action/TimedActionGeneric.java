package it.unibo.qactors.action;
import it.unibo.contactEvent.interfaces.IContactEventPlatform;
import it.unibo.contactEvent.platform.ContactEventPlatform;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.system.SituatedSysKb;
import java.util.Calendar;
import java.util.concurrent.Future;
/*
 * -------------------------------------------------------------------------
 * 					TimedActionGeneric
 * Author: AN DISI
 * Goal:   define a TERMINATING action that emits a (unique) event when it ends
 * Usage:  
 * 		activate 		starts the action as an asynchronous operation
 *  	execAction: 	executes the action and waits for the result
 *  	getActionRep:	returns a representation of the action definition
 *  	getActionAndResultRep: returns a representation of the action and of its result
 *  	getExecTime:	return the execution time of the action
 * 
 * Implementation details:
 * 		Callable<T>, Future<T>, SituatedSysKb.executorManyThread
 * 		protected boolean dosleep(long dt) : sleep for dt by capturing the InterruptedException
 * 
 * -------------------------------------------------------------------------
 */

public abstract class TimedActionGeneric<T> implements ITimedActionGeneric<T> {
	protected String name;
	protected IOutputEnvView outEnvView;
	protected String terminationEvId;
	protected long tStart  = 0;
	protected long durationMillis;	
	protected IContactEventPlatform platform ;
	protected Thread myself;
	protected Future<T>  fResult;
	protected T result;
	
	public TimedActionGeneric(String name, String terminationEvId, IOutputEnvView outEnvView) throws Exception{
		this.name 			 = name.trim();
 		this.terminationEvId = terminationEvId.trim();
		this.outEnvView 	 = outEnvView;
		platform             = ContactEventPlatform.getPlatform();
	}
	/*
	 * 1) ACTIVATE THE   ACTION
	 */
	//From it.unibo.iot.interfaces.IAction
	@Override
	public void execAction() throws Exception {
		activate();
		fResult.get();	//to force the caller to wait
	}
	public Future<T> activate() throws Exception{
//  		println("%%% TimedActionGeneric " + getName() + " ACTIVATED"     );
		fResult = SituatedSysKb.executorManyThread.submit(this);	
 		return fResult;
 	}
	/*
	 * 2) Entry point for the Executor
	 */
	@Override
	public T call() throws Exception {
		startOfAction();
		execTheAction();
		result = endActionInternal();
		return result;
	}
	protected void startOfAction() throws Exception{
		tStart = Calendar.getInstance().getTimeInMillis();
		myself = Thread.currentThread();		
	}
	/*
	 * TO BE DEFINED BY THE APPLICATION DESIGNER
	 */
	protected abstract void execTheAction() throws Exception; 
	protected abstract T endOfAction() throws Exception; 
	/*
	 * Calculate action execution time
	 */
	protected T endActionInternal() throws Exception{
		long tEnd = Calendar.getInstance().getTimeInMillis();
		durationMillis =  tEnd - tStart ;	
		emitEvent( terminationEvId );
		return endOfAction();
	}
	protected void emitEvent(String event){
		long tEnd = Calendar.getInstance().getTimeInMillis();
		durationMillis =  tEnd - tStart ;	
//    	println("%%% TimedActionGeneric " + getName() +" emit " + event + " exectime=" +  durationMillis);
   		platform.raiseEvent(getName(), event, ""+durationMillis );		
	}
/*
 * --------------------------------------
 * METHODS
 * --------------------------------------
 */
	public String getName(){
		return name;
	}
	public long getExecTime(){
		return durationMillis;
	}
	public String getTerminationEventId(){
		return this.terminationEvId;
	}
	protected boolean dosleep(long execTime) {
 		try {
 			Thread.sleep( execTime  );
 			return true;
 		} catch (InterruptedException e) {
// 			println("%%% TimedActionGeneric " + getName() + " dosleep " +  execTime + " has been interrupted "  );
  			return false;
 		}
	}
//	public T getActionAndResult() throws Exception{
//		T result = fResult.get();	//Blocking
//		return result;
//	}
	public String getActionAndResultRep() throws Exception{
 		T result = fResult.get();
 		return "action(NAME,EVENT,DURATION, RESULT)".
				replace("NAME",  getName()).
				replace("EVENT", terminationEvId ).
				replace("DURATION", ""+durationMillis ).
				replace("RESULT", "'"+result+"'");
	}
	public String getActionRep()  {
		return "action(NAME,EVENT,DURATION)".
				replace("NAME",  getName()).
				replace("EVENT", terminationEvId ).
				replace("DURATION", ""+durationMillis );
	}	
	protected void println( String msg ){
		if(outEnvView != null) outEnvView.addOutput(msg);
		else System.out.println(msg);
	}
}