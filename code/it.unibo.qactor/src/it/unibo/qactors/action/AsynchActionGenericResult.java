package it.unibo.qactors.action;

public class AsynchActionGenericResult<T> {
protected AsynchActionGeneric<T> action;
protected long timeRemained ;
protected  T result;
protected  boolean suspended; 

	public AsynchActionGenericResult( AsynchActionGeneric<T> action, T result, long time, boolean suspended){
		this.action			= action;
		this.result         = result;
		this.timeRemained   = time;
		this.suspended      = suspended;
	}
	public long getTimeRemained(){
		return timeRemained;
	}
	public T getResult(){
		return result;
	}
	public boolean getInterrupted(){
		return suspended;
	}
	public void  setResult(T result){
		this.result = result;;
	}
	@Override
	public String toString(){
		return "asynchActionResult(ACTION, RESULT,SUSPENDED,TIMEREMAINED)".
				replace("ACTION","action("+action.name+")").
				replace("RESULT","result("+result+")").
				replace("SUSPENDED","suspended("+suspended+")").
				replace("TIMEREMAINED","timeRemained("+timeRemained+")");
	}
}
