package it.unibo.qactors.action;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.contactEvent.platform.ContactEventPlatform;
/*
 * Result of a IActorAction executed in asynchronous way
 * IEventItem is the event (if any) that has interrupted the action
 */
public class AsynchActionResult implements IAsynchActionResult{
protected long timeRemained ;
protected  String result;
protected  boolean interrupted;
protected  boolean goon;
protected IEventItem interruptEvent;
protected IActorAction action;

	public AsynchActionResult(IActorAction action, long time, 
			boolean interrupted, boolean goon, String result, IEventItem interruptEvent){
		this.action			= action;
		this.timeRemained   = time;
		this.goon			= goon;
		this.interrupted    = interrupted;
		this.result 	    = result;
		this.interruptEvent	= interruptEvent;
	}
	public long getTimeRemained(){
		return (timeRemained >= 0) ?timeRemained  : 0 ;
	}
	public String getResult(){
		return result;
	}
	public boolean getGoon(){
		return goon;
	}
	public boolean getInterrupted(){
		return interrupted;
	}
	public IEventItem getEvent(){
		return interruptEvent;
	}
	public void  setResult(String result){
		this.result = result;;
	}
	public void  setGoon(boolean goon){
		this.goon = goon;;
	}
	@Override
	public String toString(){
		String actionOuts= action==null ? "-" : action.getActionName();
		return "asynchActionResult(ACTION,RESULT,SUSPENDED,TIMEREMAINED,GOON)".
				replace("ACTION","action("+ actionOuts +")").
				replace("RESULT","result("+ (result.length()==0?"result(noresult)":result) +")").
				replace("SUSPENDED","interrupted("+interrupted+")").
				replace("TIMEREMAINED","timeRemained("+timeRemained+")").
				replace("GOON","goon("+goon+")"
				);
	}
}
