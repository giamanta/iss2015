package it.unibo.qactors.action;
import it.unibo.is.interfaces.IOutputEnvView;

public class ActionUtil {
	private static int nameCount = 1;
	/*
	 * Could be called by several actors at 'the same time' => run in mutual exclusion
	 */
	public static synchronized IActorAction buildSoundAction( IOutputEnvView outEnvView, int duration,  String answerEvent, String fName) throws Exception{
		String terminationEvId = IActorAction.endBuiltinEvent+nameCount++;
 		IActorAction action = new ActionSound(
 				"sound", false, terminationEvId, answerEvent, outEnvView, duration, fName );  
  		return action;
  	}
}
	 
 
