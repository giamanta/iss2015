package it.unibo.qactors.action;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActor;

public class ActionApplication extends ActionDummy {
private QActor qa;
private  String actionName;
private  String arg1;
private  String arg2;
 	public ActionApplication( IOutputEnvView outView, int maxduration , QActor qa, String actionName, String arg1, String arg2 ) throws Exception {
 		super( outView, maxduration);  
 		this.qa = qa;
 		this.actionName = actionName;
 		this.arg1 = arg1;
 		this.arg2 = arg2;
  	}
 
	@Override
	public void execAction() throws Exception {
		try{
//  	 		println("%%% "+getName() + " EXECUTE action:" + actionName + " arg1=" + arg1 + " arg2=" + arg2 + " maxduration=" + maxduration );
 			qa.execApplicationActionByReflection( qa.getClass() , actionName, arg1,arg2);
//  			println("%%% "+getName() +  " ENDS "  );
		}catch(Exception e){
//			println("%%% "+getName() +  "  ActionDummy " + e.getMessage());
//			throw e;
		}
 	}
 }
