package it.unibo.qactors.action;

public class TimedActionGenericResult {
protected long timeRemained ;
protected  String result;
protected  boolean suspended;

	public TimedActionGenericResult(long time, boolean suspended){
		this.timeRemained   = time;
		this.suspended      = suspended;
	}
	public long getTimeRemained(){
		return timeRemained;
	}
	public String getResult(){
		return result;
	}
	public boolean getInterrupted(){
		return suspended;
	}
	public void  setResult(String result){
		this.result = result;;
	}
	@Override
	public String toString(){
		return "timedActionResult(SUSPENDED,TIMEREMAINED)".
				replace("SUSPENDED","suspended("+suspended+")").
				replace("TIMEREMAINED","timeRemained("+timeRemained+")");
	}
}
