package it.unibo.qactors.action;
import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import it.unibo.contactEvent.platform.ContactEventPlatform;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActor;
import it.unibo.system.SituatedSysKb;

public class ActionSolveGoal extends ActorAction implements IActorAction{
	protected static int n = 1;
private QActor qa;
private  String goal;
private SolveInfo sol    = null;
private boolean goalDone = false;
private boolean solveInterrupted   = false;
private Thread solveThread, timer;
private Thread sleepThread;
private Prolog pengine ;

 	public ActionSolveGoal( IOutputEnvView outView, int maxduration , String answerEvId, QActor qa,  String goal  ) throws Exception {
 		super( "solveGoal",false,ContactEventPlatform.locEvPrefix+"EndEv"+n++, answerEvId, outView, maxduration);  
 		this.qa   = qa;
  		this.goal = goal;
  		pengine   = qa.getPrologEngine();
   	}
	/*
	 * This operation is called by TimedActionGeneric when activated
	 * The action at application level performs two main tasks
	 * 1) waits in the current Callable for maxDuration
	 * 2) activates a new Thread that attempts to solve the goal
	 *    (msgTermStrToReceive) message by Inspect the world and the
 	 * The goal solution-found must stop the sleep, while the end-of.sleep
 	 * must stop the goal solution thread
  	 */

	@Override
	public void execTheAction() throws Exception {
 
		//EXPERIMENT to show that the pengine is loacked
 			if( maxduration == 0 ){
// 				println("%%% ActionSolveGoal time 0 goal="+ goal +  " STARTS "   );
 				sol = qa.getPrologEngine().solve(goal+".");	
//				println("%%% ActionSolveGoal time 0 sol="+ sol.isSuccess()  );
				return;
			}
 

			 
//			startTimer(maxduration);			
// 			println("%%% execTheAction "+ goal +  " STARTS "   );
//			sol = pengine.solve(goal+".");			
//			println("%%% execTheAction "+ goal +  " sol=" + sol.isSuccess() );				

 			/* 
 			 * Solve the goal in ANOTHER THREAD while this thread (action) waits for maxduration
 			 * The new thread could interrupt the sleeping
 			 * While the action is sleeping, it can be interrupted by some external event
 			 * since it is a TinmedAction
 			 */
			startSolveThread(); 	//could interrupt  sleepThread
			 
// 			println("ActionSolveGoal START SLEEP " +  maxduration );
			boolean b = dosleep(maxduration);
// 			println("ActionSolveGoal END SLEEP " +  maxduration + " ended-normally=" + b + " goalDone="  + goalDone);
 			
 			if(  ! goalDone ){ //dosleep expired or interrupted  
				pengine.solveHalt();
	 			solveInterrupted = true;
// 				println("ActionSolveGoal pengine halt " + ( solveThread != null) );
				if(solveThread != null ) solveThread.interrupt();		
			} 
  			
  	}
	

	/*
	 * The goal is solved in the another thread
	 * but it is not a interruptable TimedAction
	 */
	protected void startSolveThread(){
 		solveThread = new Thread(){
			public void run(){
			try {
				goalDone = false;
//  				println("%%% solveThread "+ goal +  " STARTS "   );
  				//The pengine is locked if it already solving a goal (e.g. interpreter) 
 				sol = pengine.solve(goal+".");
//  				println("%%% solveThread "+ goal +  " ENDS endJob=" + solveInterrupted + " sol=" + sol.getSolution() );				
				
				if( ! solveInterrupted ) {  //sleep not interrupted
					suspendAction(); 
					goalDone = false;
				}else goalDone = true;
 			}catch(Exception e) {
				goalDone = false;
//				System.out.println("%%% solveThread INTERRUPTED "  + e.getMessage() );
//				pengine.solveHalt();
 			}
			}//run
		};
//		println("%%% startSolveThread "+ goal +  " STARTS "   );
		SituatedSysKb.executorManyThread.submit(solveThread);
  	}
/*
 * Used if we want run asynch the timer
 */
//	protected void startTimer(int execTime){
//		Thread timer = new Thread(){
//			public void run(){
//			try {
//				Thread.sleep( execTime  );
// 				println("%%% timer EDS sol=" + sol );
//				if(sol == null ) pengine.solveHalt();
//  			}catch(Exception e) {
// 				println("%%% solveThread INTERRUPTED "  + e.getMessage() );
//  			}
//			}//run
//		};
//		println("%%% timer "+ execTime +  " STARTS "   );
//		SituatedSysKb.executorManyThread.submit(timer);
//		
//	}

	public SolveInfo getSolveResult(){
		return sol;
	}

 /*
  * Called by getResult
   */
	@Override
	protected String getApplicationResult() throws Exception {
 		return goalDone ? ""+sol.getSolution() : "failure";	//"failure" is the standard worng result
	}
 }
