package it.unibo.qactors;
import java.util.Iterator;
import java.util.Vector;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.is.interfaces.protocols.IConnInteraction;
import it.unibo.supports.FactoryProtocol;
import it.unibo.system.SituatedActiveObject;
import it.unibo.system.SituatedSysKb;
/*
 * Waits for messages from the connected nodes
 */
public class ReceiverAgent extends SituatedActiveObject{
	protected static Vector<String> newCtxs = new Vector<String>(); 
	protected IConnInteraction conn;
	protected String hostName;
	protected int port;
	protected MsgInterpreter interpreter;	
	protected FactoryProtocol factoryP;
	protected ActorContext ctx;
	protected Prolog prologEngine ;

	public ReceiverAgent(String name, ActorContext ctx, IOutputView outView, IConnInteraction connIn ) {
		super(outView, name);
		this.ctx = ctx;
		prologEngine = ctx.getPrologEngine();
		conn 	 	 = connIn;
 		interpreter  = ctx.getMsgInterpreter();
   		activate( SituatedSysKb.executorManyThread );
 	}
	@Override
	protected void startWork()  {
 	}
	@Override
	protected void doJob()  {
// 		println("ReceiverAgent " + getName() + " STARTS");
		try {
			Thread.sleep(500); //give the time to the local actors to register
	 		while(true){ 
	  				//println("ReceiverAgent waits for a message ... " );
					final String msg = conn.receiveALine();
//  	      			println("			ReceiverAgent received " + msg  );
////  	      					+ " msgId=" + ctx.getMsgId(msg)
////  	      					+ " sender=" + ctx.getMsgSenderActorId(msg ) );
					if( msg != null ){
						if( ctx.getMsgId(msg).contains("updatesyskb")
								&& ctx.getMsgSenderActorId(msg).contains("remotectx") ){
	 						updateLocalTheory(ctx.getContentMsg(msg));
						} 
						else 
							interpreter.elab(msg);			
					} else{
//						println(" *** ReceiverAgent received null "     );
 						break;
					}
			}		
		} catch (Exception e) {
			println( getName() + " ReceiverAgent ERROR " + e.getMessage() );
				
		}			
	}
	/*
	 * Dynamic MONOTONIC updating of the system knowledge base
	 */
	protected void updateLocalTheory(String thRep) throws Exception{
//  		println(" *** updateLocalTheory   " + thRep );
		int curCtxNum = newCtxs.size();
		thRep = thRep.substring(1, thRep.length()-1); //delete ' '
		//println(" *** updateLocalTheory  thRep : " + thRep   );
		String[] facts = thRep.split("@");
		for( int i=0; i<facts.length; i++){
			if( facts[i].length() > 0 ){
				checkInsertFact( facts[i] );
			}
 		}
//		println(" *** updateLocalTheory   " + curCtxNum+"/"+newCtxs.size());
		if( newCtxs.size() > curCtxNum ){
			//There are new contexts: update the theory rep
			println(" *** ReceiverAgent  UPDATES THE SYSTEM THEORY from current num of new contexts " + curCtxNum+" to "+newCtxs.size());
			ctx.createLocalTheoryRep();
			createSupports();
 		}
	}
			
	protected void createSupports() throws Exception{
		//Create a sender connection to new contexts and propagate the theory rep
		Iterator<String> it = newCtxs.iterator();	
		while( it.hasNext() ){
			String curCtx = it.next();
			ctx.activateSenderToCtx( curCtx, true );
			ctx.updateEvlpa(curCtx);
		}		
	}
	/*
	 * Insert a new system fact if not already present
	 */
	protected void checkInsertFact(String fact) throws Exception{
//		println(" *** ReceiverAgent  checkInsertFact : " + fact   );
 		SolveInfo sol  = prologEngine.solve( fact + ".");
		if( sol.isSuccess() ) return; //already in
		else{
 			println(" *** ReceiverAgent checkInsertFact new fact : " + fact   );
			prologEngine.solve( "assertz(" + fact + ").");
			if( fact.startsWith("context")){
				//Dynamic MONOTONIC extension of the context interaction support
				String curCtx = fact.substring(fact.indexOf("(")+1, fact.indexOf(","));
				newCtxs.add(curCtx);
			}
		}
	}
	@Override
	protected void endWork() throws Exception {		
	}

 
}
