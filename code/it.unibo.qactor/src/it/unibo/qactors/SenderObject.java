/*
  * Provides a method to send messages to a remote context
  * (by creating a connection with that remote context)
 */
package it.unibo.qactors;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.is.interfaces.protocols.IConnInteraction;
import it.unibo.supports.FactoryProtocol;
import it.unibo.system.SituatedPlainObject;

public class SenderObject extends SituatedPlainObject{	
	protected IConnInteraction conn = null;
	protected String hostName;
	protected int port;
 	protected FactoryProtocol factoryP;
 	protected ActorContext ctx;
	protected String protocol;
	protected SenderObject myself;
	
	public SenderObject(String name, ActorContext ctx, IOutputView outView,  String protocol, String hostName, int port ) {
		super(name, outView);
		this.ctx = ctx;
		this.hostName 	= hostName;
		this.port     	= port;
		this.protocol	= protocol;
		factoryP 		= new FactoryProtocol(outView, protocol, "fp");
		myself 			= this;
		tryTheConnection();
		
    } 
	protected void tryTheConnection(){
		//Attempt to connect with the remote node
		new Thread(){
			public void run(){
				boolean res = false;
				while( ! res ){
					res = setConn( );
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
							e.printStackTrace();
					}
				}//while
				try {
					myself.updateWaiting();
				} catch (Exception e) {
 					e.printStackTrace();
				}
			}
		}.start();		
	}
	protected synchronized void updateWaiting() throws Exception{
		conn = ctx.getConnection(hostName+port);
		this.notifyAll();
	}
	public synchronized void sendMsg(QActor sender, String receiverName, String msgId, String msgType, String m) throws Exception {
//		println("SenderObject sendMsg/5 to " + hostName + " conn=" + conn );
		conn = ctx.getConnection(hostName+port);
		while( conn == null ){
//			println("SenderObject waits since NO CONNECTION to " + hostName + port  );
			wait();
		}
		if( conn != null ){
//			println("SenderObject sends " + m + " on " + conn  );
			sendTheMsg(  sender, receiverName,  msgId, msgType, m) ;	
		} else{
			println("SenderObject NO CONNECTION to " + hostName + ":" + port   );
 		}
  	}
	protected void sendTheMsg(QActor sender, String receiverName,  String msgId, String msgType, String m) throws Exception {
		try {
			int msgNum = ctx.newMsgnum();
			//msg( MSGID, MSGTYPE SENDER, RECEIVER, CONTENT, SEQNUM )
			String senderName  = sender.getName().toLowerCase();
			//if( ! m.startsWith("'")) m = "'"+m+"'";
	 		String mout = "msg(" + msgId +","+ msgType + ","+ senderName +","+ receiverName +","+ m+","+ msgNum +")";
//  	 		println("SenderObject sends " + mout + " " + hostName + ":" + port  + " conn=" + (conn != null) );
	 		conn.sendALine( mout );
		} catch (Exception e) {
			println("SenderObject sends ERROR " + e.getMessage()   );
			ctx.connectionTable.remove(hostName+port);
			throw e;
		}									
	}
	public synchronized void sendMsg(String mout) throws Exception {
		try {
			conn = ctx.getConnection(hostName+port);
			while( conn == null ){
//				println("SenderObject waits since NO CONNECTION to " + hostName + port   );
				wait();
			}
			if( conn != null ){
				conn.sendALine( mout );	
			} else{
				println("SenderObject NO CONNECTION to " + hostName + ":" + port   );
			
		}
		} catch (Exception e) {
			ctx.connectionTable.remove(hostName+port);
			throw e;
		}									
	}
	protected boolean setConn( ){
		try {
//  			println("SenderObject " + name + " try conn " +  protocol + " " + hostName + " " + port);
			IConnInteraction conn = factoryP.createClientProtocolSupport(hostName, port);
//  			println("SenderObject " + name + "  conn " +  conn + " " + hostName + " " + port);
			ctx.connectionTable.put(hostName+port, conn);
			return true;
		} catch (Exception e) {
			 //println("SenderObject setConn ERROR "  + e.getMessage() );
		}				
		return false;
 	}
	/*
	 * Called by ActorContext
	 */
	public String receiveWakeUpAnswer() throws Exception{
		String line = conn.receiveALine();
		println(" *** SenderObject receiveWakeUpAnswer " + line );
		return line;
	}
}
