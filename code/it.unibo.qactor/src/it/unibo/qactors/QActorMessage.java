package it.unibo.qactors;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.contactEvent.interfaces.IActorMessage;

public class QActorMessage implements IActorMessage{
//msg( MSGID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM )
	String msgId  ;
	String msgType  ;
	String msgSender  ;
	String msgReceiver  ;
	String msgContent  ;
	int msgNum  ;
	
	public QActorMessage(String MSGID, String MSGTYPE, String SENDER, String RECEIVER, String CONTENT, String SEQNUM){
		msgId = MSGID;
		msgType=MSGTYPE;
		msgSender=SENDER;
		msgReceiver=RECEIVER;
		msgContent=	CONTENT;
//		System.out.println(" +++ QActorMessage msgContent= " + msgContent);
		try{
			msgNum=	Integer.parseInt( SEQNUM );
		}catch(Exception e){
			System.out.println(" +++ QActorMessage CONVERSION ERROR FOR " + SEQNUM);
		}
	}
	
	public QActorMessage (String msg){
		try{
// 			System.out.println(" +++ QActorMessage constructor TERM for " + msg);
			Struct msgStruct = (Struct) Term.createTerm(msg);
//			if( ! msgStruct.toString().contains("noresult")) 
				setFields(msgStruct);
		}catch(Exception e){
			System.out.println(" +++ QActorMessage TERM ERROR FOR " + msg);
			e.printStackTrace();
		}
 	}
	
	private void setFields(Struct msgStruct){
		msgId		= 		msgStruct.getArg(0).toString();
		msgType		=		msgStruct.getArg(1).toString();
		msgSender	=		msgStruct.getArg(2).toString();
		msgReceiver	=		msgStruct.getArg(3).toString();
		msgContent	=		msgStruct.getArg(4).toString();
		//System.out.println(" +++ QActorMessage setFields msgContent= " + msgContent);
		try{
			msgNum=		Integer.parseInt(msgStruct.getArg(5).toString());
		}catch(Exception e){
			System.out.println(" +++ QActorMessage setFields CONVERSION ERROR msgNum " + msgStruct.getArg(5).toString() );
			msgNum = 0; //TOCHECK
		}
	}
	@Override
	public String msgId() {
 		return msgId;
	}
	@Override
	public String msgType() {
 		return msgType;
	}
	@Override
 	public String msgSender() {
 		return msgSender;
	}
	@Override
 	public String msgReceiver() {
 		return msgReceiver;
	}
	@Override
	public String msgContent() {
 		return msgContent;
	}
	@Override
	public String msgNum() {
 		return ""+msgNum;
	}

	@Override
	public String getDefaultRep() {
		String ccc = "";
		try{
			Term t = Term.createTerm(msgContent);
			ccc = t.toString();
			if( ccc.startsWith("'")) ccc = ccc.substring(1,ccc.length()-1);
		}catch(Exception e){
			ccc = msgContent;
		}
		if( msgType == null ) return "msg(none,none,none,none,none,0)";
 		return "msg(A,B,C,D,E,F)".replace("A", msgId).replace("B", msgType).
 				replace("C", msgSender).replace("D", msgReceiver).replace("E", ccc).replace("F", msgNum() );
	}
	
	public Term getTerm() {
		if( msgType == null ) return Term.createTerm("msg(none,none,none,none,none,0)");
 		else return Term.createTerm(msgContent);
	}
}
