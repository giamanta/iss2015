
/*
 * ApplSystem is written by the Application designer
 * in order to define operations that can be used
 * from Prolog rules (see prolog-files/applTheory.pl)
 *
    [FORWARD]

         turn 180   ^
avanti   avanti     |
indietro indietro   |
destra   destra     |
sinistra sinistra   |
stop

    [BACKWARD]

avanti   indietro   ^
indietro avanti     |
destra   sx         |
sinistra destra     |
stop

 */
import it.unibo.contactEvent.platform.EventPlatformKb;
import it.unibo.is.interfaces.IOutputView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;

import java.util.ArrayList;
import java.util.Collections;

public class ApplSystem {
	private static IOutputView outView = EventPlatformKb.stdOutView;
	private static boolean forward = true;
	private static boolean backward = false;
	private static boolean backhome_direction = forward;
	private static int current_mossa = 0;
	private static ArrayList<Mossa> mosse = new ArrayList<Mossa>(15);

	public static void setOutView(IOutputView outViewArg) {
		outView = outViewArg;
	}

	public void test() {
		outView.addOutput(" [ApplSystem] ApplSystem no package test ok");
	}

	public static void testStatic() {
		outView.addOutput(" [ApplSystem] testStatic no package ok");
	}

	public static String readFile(String fName) {
		String outS = "";
		try {
			outView.addOutput(" [ApplSystem] ApplSystem  readFile " + fName);
			InputStream fs = new java.io.FileInputStream(fName);
			InputStreamReader inpsr = new InputStreamReader(fs);
			BufferedReader br = new BufferedReader(inpsr);
			Iterator<String> lsit = br.lines().iterator();
			// outS="\"";
			while (lsit.hasNext()) {
				outS = outS + lsit.next();
				if (lsit.hasNext())
					outS = outS + "\n";
			}
			br.close();
			// outS= outS + "\"";
		} catch (Exception e) {
			outView.addOutput(" [ApplSystem] ApplSystem  ERROR " + e.getMessage());
			// e.printStackTrace();
		}
		return outS;
	}

	public static void writeInFile(String fName, String content) {
		ApplSystem.writeInFile(fName, content, "0", "true");
	}

	public static void writeInFile(String fName, String content, String time) {
		ApplSystem.writeInFile(fName, content, time, "true");
	}

	public static void writeInFile(String fName, String content, String time, String append) {
		try {
			outView.addOutput(" [ApplSystem] ApplSystem  writeInFile " + fName);
			FileOutputStream fsout = new FileOutputStream(new File(fName),
					append.toLowerCase() == "true" ? true : false);
			fsout.write(("move(" + content + ", '" + time + "').\n").getBytes());
			fsout.close();
		} catch (Exception e) {
			outView.addOutput(" [ApplSystem] ApplSystem  ERROR " + e.getMessage());
		}
	}

	public static void cleanAutoTheoryFile(String fName) {
		try {
			FileOutputStream fsout = new FileOutputStream(new File(fName));
			fsout.write(("").getBytes());
			fsout.close();
		} catch (Exception e) {
			outView.addOutput(" [ApplSystem] ApplSystem  ERROR " + e.getMessage());
		}

	}

	public static void setBackhomeDirection(String cmd) {
		// f-Auto = true = forward
		// !f-Auto = false = backward
		backhome_direction = cmd.contains("f(Auto");
	}

	public static boolean getBackhomeDirection() {
		outView.addOutput(" [ApplSystem] getBackhomeDirection ");
		return backhome_direction;
	}

	public static void createMove(String cmd, String time) {
		if (backhome_direction == backward) // We must change each move to his
											// dual
		{
			char[] ccmd = cmd.toCharArray();
			switch (ccmd[0]) {
			case 'w':
				ccmd[0] = 's';
				break;
			case 'a':
				ccmd[0] = 'd';
				break;
			case 's':
				ccmd[0] = 'w';
				break;
			case 'd':
				ccmd[0] = 'a';
				break;
			case 'h':
			case 't':
				break;
			}
			cmd = String.valueOf(ccmd);
		}
		mosse.add(new Mossa(cmd, Long.parseLong(time, 16)));
	}

	public static void setDiffTime() {
		for (int i = 0; i < mosse.size() - 1; i++) {
			mosse.get(i).setTime(mosse.get(i + 1).getTime() - mosse.get(i).getTime());
			outView.addOutput(" [DIFF] " + mosse.get(i).getTime());
		}
		// Remove terminate element (useless from now on)
		mosse.remove(mosse.size() - 1);

		if (backhome_direction == backward)
			Collections.reverse(mosse);
	}

	public static String getCurrentMove() {
		return getMove(current_mossa++);
	}

	public static String getNextMove() {
		return getMove(current_mossa + 1);
	}

	public static Long getCurrentTime() {
		return getTime(current_mossa);
	}

	public static String getCurrentCmd() {
		return getCmd(current_mossa);
	}

	public static void setNextMove() {
		current_mossa++;
	}

	public static String getMove(int idx) {
		/*
		 * String mossa = "move(usercmd(\"" + mosse.get(idx).getCommand() +
		 * "\")," + Long.toString(mosse.get(idx).getTime()) + ")";
		 */
		String mossa = "usercmd(\"" + mosse.get(idx).getCommand() + "\")," + Long.toString(mosse.get(idx).getTime());
		outView.addOutput(" [ApplSystem] getCurrentMove " + mossa);

		return mossa;
	}

	public static Long getTime(int idx) {
		Long time = mosse.get(idx).getTime();
		outView.addOutput(" [ApplSystem] getCurrentTime " + time);
		return time;
	}

	public static String getCmd(int idx) {
		String cmd = mosse.get(idx).getCommand();
		outView.addOutput(" [ApplSystem] getCurrentCmd " + cmd);
		return cmd;
	}

	public static String isMovesFinished() {
		outView.addOutput(" [ApplSystem] Is MoveFinished -> backhome_direction = " + backhome_direction);
		outView.addOutput(" [ApplSystem] Is MoveFinished -> current_mossa = " + current_mossa);

		if (current_mossa >= mosse.size()) {
			outView.addOutput(" [ApplSystem] Is MoveFinished -> TRUE");
			return "endmemo";
		}

		outView.addOutput(" [ApplSystem] Is MoveFinished -> FALSE");
		return "keepgoin";
	}

}
