
public class Mossa {
	private long time;
	private String command;
	
	public Mossa(String cmd, long time){
		this.command = cmd;
		this.time = time;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}
}
