/*** -------------------------------------------------------------------------*
 * applTheory (prolog-files in project it.unibo.qactor.react)
 * This theory is defined at application level
 * It exploits tuProlog to access java classes and objects
 *** ------------------------------------------------------------------------ */

% Utilities -----------------------------------------------------------------{{{
/*
-------------------------------------------------------------------
Extract the message content from the current message
-------------------------------------------------------------------
*/
msgContent( CONTENT ):-
   myname( Actor ),
   Actor <- memoCurrentMessage,
   retract( msg( MSGID, MSGTYPE, SENDER, RECEIVER, C, SEQNUM ) ),   %%retract always !!!
   %% actorPrintln( goToPlan( Actor, C , CONTENT ) ),
   C = CONTENT .

/*
-------------------------------------------------------------------
Extract the event content from the current event
-------------------------------------------------------------------
*/
eventContent( CONTENT ):-
   myname( Actor ),
   Actor <- memoCurrentEvent,
   retract( msg( MSGID, MSGTYPE, SENDER, RECEIVER, C, SEQNUM ) ),   %%retract always !!!
   %% actorPrintln( goToPlan( Actor, C , CONTENT ) ),
   C = CONTENT .

/*
-------------------------------------------------------------------
Using the registered robot to emit a usercmd event
-------------------------------------------------------------------
*/
doemit( EV,EVMSG ):-
  myname( R ),      %% myname is defined in the generated actor WorldTheroy
  actorPrintln( doemit( R, EV, EVMSG ) ),
              %% R is a registered object. See the tuProlog Manual
  R <- emit( EV, EVMSG  ).
  /*
              %% The following code can be used to emits a stop after 1 sec
              %% to interrupt the current move in the logic of Robotreactiveprolog
    R <- delayNaive( 1000 ),
    actorPrintln( doemit( R, usercmd, usercmd("h-Low") ) ),
  R <- emit( usercmd, usercmd("h-Low") ).
  */

doPrint :-
  actorPrintln( "[applTheory] doPrint" ),
  retract( move(usercmd( CMD ), TIME ) ),
  actorPrintln( "[applTheory] doPrint -> retract done." ),
  actorPrintln( move(usercmd( CMD ), TIME ) ).

% }}}

% Prolog computations, look at tuProlog Manual ------------------------------{{{

/*
------------------------------------------------------------
Injects the standard output device of the actor (robot) in
the application class it.unibo.appl.ApplSystem
------------------------------------------------------------
*/
setOutViewInApplSystem :-
  myname( Actor ),
  Actor <- getOutputView returns OutView ,
  class("ApplSystem") <- setOutView(OutView).

/*
------------------------------------------------------------
Reads a file and returns its content
by using the application class it.unibo.appl.ApplSystem
------------------------------------------------------------
*/
readFile( FName,  Content ) :-
  class("ApplSystem") <-  readFile(FName) returns Content,
  actorPrintln( readFile( FName,  Content ) ).

readFileEval( FName, callback(Content) ) :-
  %% java_object("it.unibo.appl.ApplSystem", [], Appl),
  %% Appl <- readFile(FName) returns OutS,
  class("ApplSystem") <-  readFile("./src/it/unibo/appl/program.pl") returns Content,
  actorPrintln(  readFile( FName,  Content ) ).

callback( Content )  :-  assert( stored(Content) ).

/*
------------------------------------------------------------
Write the current msg content into a file
by using the application class it.unibo.appl.ApplSystem
------------------------------------------------------------
*/
writeCurrentMsg( FName  ) :-
   myname( Actor ),
   Actor <- memoCurrentMessage,
   retract( msg( MSGID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM ) ),
   actorPrintln( writeCurrentMsg(ActorName, FName, CONTENT) ),
     class("ApplSystem") <-  writeInFile(FName,CONTENT).


/*
------------------------------------------------------------
Switch to a plan in Prolog
------------------------------------------------------------
*/

goToPlan(  CONTENT , Plan   ) :-
   myname( Actor ),
   Actor <- memoCurrentMessage,
   retract( msg( MSGID, MSGTYPE, SENDER, RECEIVER, C, SEQNUM ) ), %%retract always !!!
   %% actorPrintln( goToPlan( Actor, C , CONTENT, Plan ) ),
   C = CONTENT ,
     Actor <- switchToPlan( Plan ) returns AAR ,
   AAR <- getGoon returns B ,!,
   %% actorPrintln( aar( B ) ),
   B == true  .

% }}}

% Application logic by Giacomo, Lorenzo ed Emilio ---------------------------{{{
%

cleanAutoTheory :-
  externalPathaf(PATH),
  actorPrintln( cleaning(PATH) ),
  java_object("ApplSystem", [], AS),
  AS <- cleanAutoTheoryFile(PATH).

writeCurrentEv :-
  externalPathaf( PATH ),
  myname( Actor ),
  Actor <- memoCurrentEvent,
  retract( msg( MSGID, MSGTYPE, SENDER, RECEIVER, usercmd(robotgui(CONTENT)), TIME ) ),
  actorPrintln( writeCurrentEV(Actor, PATH, CONTENT, TIME) ),
  java_object("ApplSystem", [], Appl),
  Appl <- writeInFile(PATH, CONTENT, TIME).

fetchAutonomousSeq :-
  internalPathaf(PATH),
  consult(PATH).

setBackhomeDirection :-
  retract(msg(MSGID, MSGTYPE, SENDER, RECEIVER, usercmd(robotgui(CONTENT)), TIME) ),
  java_object("ApplSystem", [], Appl),
  Appl <- setBackhomeDirection(CONTENT).

decodeMove :-
  actorPrintln( "[applTheory] DecodeMove" ),
  retract(move(CMD, TIME )),
  actorPrintln( CMD ),
  java_object("ApplSystem", [], Appl),
  Appl <- createMove(CMD, TIME).

deltaMosse :-
  java_object("ApplSystem", [], Appl),
  Appl <- setDiffTime.

checkEnd :-
  java_object("ApplSystem", [], Appl),
  Appl <- isMovesFinished returns FINE,
  actorPrintln( FINE ),
  asserta(FINE),
  actorPrintln("[applTheory] asserta checkEnd DONE").

memoMove :-
  actorPrintln( "[applTheory] memoMove" ),
  java_object("ApplSystem", [], Appl),
  Appl <- getCurrentTime returns TIME,
  Appl <- getCurrentCmd returns CMD,
  Appl <- setNextMove,
  assertz(move(usercmd(CMD),TIME)), %% add at the end
  actorPrintln("[applTheory] assertz memoMove DONE").

elabMove(EV,EVMSG,EVTIME) :-
  myname( R ),
  actorPrintln( elabMove( R, EV, EVMSG, EVTIME ) ).

execMove(AlarmEvent, ReactionPlan) :-
  actorPrintln("[applTheory] execMove" ),
  actorPrintln(execMove(AlarmEvent, reactionPlan)),
  retract( move(usercmd( CMD ), TIME )),
  actorPrintln( move(usercmd( CMD ), TIME ) ),
  doMove( AlarmEvent, CMD, TIME, ReactionPlan ).

doMove( AlarmEvent, EVMSG, EVTIME, RP ) :-
  actorPrintln(doMove( AlarmEvent, EVMSG, EVTIME, RP )),
  myname( R ),
  R <- getCurrentPlan returns CurPlanName,
  robotMove(R, CurPlanName, EVMSG, EVTIME, AlarmEvent, RP ),
  actorPrintln( doMove( R, CurPlanName, AlarmEvent, EVMSG, EVTIME, RP ) ).

robotMove( Robot, CurPlanName, CONTENT, MoveTime, AlarmEvent, ReactionPlan ):-
  actorPrintln("[applTheory] Robot Move"),
  actorPrintln(robotMove( CurPlanName, CONTENT, MoveTime,  AlarmEvent, ReactionPlan )),
  moveCmdTable(CONTENT, RobotCmd, Speed ),
  Robot <- execRobotMove(CurPlanName, RobotCmd, Speed, 0, MoveTime, AlarmEvent, ReactionPlan) returns B,
  actorPrintln( robotMove( CurPlanName, RobotCmd , Speed,  AlarmEvent, ReactionPlan, B ) ),
  B == true.

robotMoveFromUsercmdEvent( MoveTime, AlarmEvent, ReactionPlan ) :-
   actorPrintln( robotMoveFromUsercmdEvent( Robot, AlarmEvent, ReactionPlan ) ),
   myname( Robot ),
   Robot <- memoCurrentEvent,
   retract( msg( MSGID, event, SENDER, RECEIVER, usercmd(robotgui(CONTENT)), SEQNUM ) ),
   actorPrintln( robotMoveFromUsercmdEvent( Robot,  CONTENT ) ),
   Robot <- getCurrentPlan returns CurPlanName,
   robotMove(Robot, CurPlanName, CONTENT, MoveTime, AlarmEvent, ReactionPlan )  .

% }}}

% Maps and look-up tables ---------------------------------------------------{{{
/*
-------------------------------------------------------------------
Map a user command into a move and a speed
-------------------------------------------------------------------
*/
moveCmdTable( w(low), forward, 40 ).
moveCmdTable( w(medium), forward, 70 ).
moveCmdTable( w(high), forward, 100 ).
moveCmdTable( a(low), left, 40 ).
moveCmdTable( a(medium), left, 70 ).
moveCmdTable( a(high), left, 100 ).
moveCmdTable( s(low), backward, 40 ).
moveCmdTable( s(medium), backward, 70 ).
moveCmdTable( s(high), backward, 100 ).
moveCmdTable( d(low), right, 40 ).
moveCmdTable( d(medium), right, 70 ).
moveCmdTable( d(high), right, 100 ).
moveCmdTable( h(low), stop, 40 ).
moveCmdTable( h(medium), stop, 70 ).
moveCmdTable( h(high), stop, 100 ).

moveCmdTable( "w(low)", forward, 40 ).
moveCmdTable( "w(medium)", forward, 70 ).
moveCmdTable( "w(high)", forward, 100 ).
moveCmdTable( "a(low)", left, 40 ).
moveCmdTable( "a(medium)", left, 70 ).
moveCmdTable( "a(high)", left, 100 ).
moveCmdTable( "s(low)", backward, 40 ).
moveCmdTable( "s(medium)", backward, 70 ).
moveCmdTable( "s(high)", backward, 100 ).
moveCmdTable( "d(low)", right, 40 ).
moveCmdTable( "d(medium)", right, 70 ).
moveCmdTable( "d(high)", right, 100 ).
moveCmdTable( "h(low)", stop, 40 ).
moveCmdTable( "h(medium)", stop, 70 ).
moveCmdTable( "h(high)", stop, 100 ).

% }}}
