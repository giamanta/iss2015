package it.unibo.qactors;

import java.util.StringTokenizer;
import java.util.Vector;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.action.IActorAction.ActionExecMode;
import it.unibo.qactors.action.TaskActionFSMExecutoResult;
import it.unibo.qactors.fsmexecutor.TaskActionFSMExecutor;

/*
 * ActorAction that can react to events by executing plans of the given actor 
 */

public class ActorActonExecutorFSM {
	public static final boolean suspendWork = false;
	public static final boolean continueWork = true;
	public static final boolean interrupted = true;
	public static final boolean normalEnd = false;

	protected QActor actor;
	protected IOutputEnvView outEnvView;
	protected IActorAction action;
	protected static int nameCount = 0; // to avoid built-in componebts with the
										// same name
	protected IEventItem currentEvent = null;
	protected QActorMessage currentMessage = null;

	public ActorActonExecutorFSM(IActorAction action, IOutputEnvView outEnvView, QActor actor) throws Exception {
		// super(action.getActionName(), action.canBeCompensated(),
		// action.getTerminationEventId(), action.geAnswerEventId(), outEnvView,
		// action.getMaxDuration());
		this.action = action;
		this.outEnvView = outEnvView;
		this.actor = actor;
	}

	/*
	 * Create and launch the FSM executor. Returns a TaskActionFSMExecutoResult
	 */
	public TaskActionFSMExecutoResult executeActionFSM(String alarmEvents, String recoveryPlans, ActionExecMode mode)
			throws Exception {
		alarmEvents = alarmEvents.trim();
		alarmEvents = alarmEvents.replaceAll("'", "");
		String[] vsa = createArray(alarmEvents);
		for (String v : vsa)
			this.outEnvView.addOutput(v);
		String[] vplans = createPlanArray(recoveryPlans, vsa.length);
		nameCount++;
		TaskActionFSMExecutor tke = new TaskActionFSMExecutor("taskactionexec_" + action.getActionName(),
				actor.getContext(), outEnvView, action, action.getMaxDuration(), vsa, vplans);
		if (mode == ActionExecMode.synch) {
			// long moveTimeNotDone =
			tke.waitForTemrmination();
		}
		return tke.getResult();
	}
	/*
	 * UTILITY
	 */

	protected void println(String msg) {
		if (outEnvView != null)
			outEnvView.addOutput(msg);
		else
			System.out.println(msg);
	}

	protected String[] createPlanArray(String plans, int n) {
		if (plans == null)
			return new String[] {};
		if (plans.length() == 0)
			return new String[0];
		Vector<String> vsPlans = new Vector<String>();
		if (plans.contains(",")) {
			StringTokenizer st = new StringTokenizer(plans, ",");
			while (st.hasMoreTokens()) {
				String t = st.nextToken();
				t.replaceAll("'", "");
				// println(" RobotActor createEventArray adding token " + t );
				vsPlans.add(t);
			}
		} else
			vsPlans.add(plans);
		String[] vsa = null;
		if (vsPlans.size() == n)
			vsa = new String[vsPlans.size()];
		else
			vsa = new String[n];
		for (int i = 0; i < vsPlans.size(); i++)
			vsa[i] = vsPlans.elementAt(i);
		for (int i = vsPlans.size(); i < n; i++)
			vsa[i] = "dummyPlan";
		// for( int i=0; i<n; i++) System.out.println(" --- ActionExecutorAsFSM
		// createPlanArray " + i + " " + vsa[i] ) ;
		return vsa;
	}

	protected String[] createArray(String evId) {
		if (evId == null)
			return new String[] {};
		if (evId.length() == 0)
			return new String[0];
		Vector<String> vs = new Vector<String>();
		if (evId.contains(",")) {
			StringTokenizer st = new StringTokenizer(evId, ",");
			while (st.hasMoreTokens()) {
				String t = st.nextToken();
				t.replaceAll("'", "");
				// println(" RobotActor createEventArray adding token " + t );
				vs.add(t);
			}
		} else
			vs.add(evId);
		String[] vsa = new String[vs.size()];
		for (int i = 0; i < vs.size(); i++)
			vsa[i] = vs.elementAt(i);
		return vsa;
	}
}