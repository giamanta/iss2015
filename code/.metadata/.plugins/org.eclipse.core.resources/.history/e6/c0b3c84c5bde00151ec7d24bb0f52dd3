RobotSystem modelRobot -regeneratesrc

Event usercmd : usercmd(X)
Event sensordata : sensordata(X)
Event obstacle : obstacle(X)

Context  ctxRobotModel  ip [ host="localhost" port=8078 ] -httpserver

QActor master context ctxRobotModel {

	Rules{

		loadTheory(FName) :-
			actorPrintln( loadTheory(FName) ),
			consult( FName ).

		cmd(usercmd(robotgui(l(Learning)))).
		cmd(usercmd(robotgui(w(low)))).
		cmd(usercmd(robotgui(h(medium)))).
		cmd(usercmd(robotgui(d(low)))).
		cmd(usercmd(robotgui(t(Terminate)))).
		cmd(usercmd(robotgui(b(Auto)))).

		//Rule that emit a usercmd by 'consuming' the RuleCmdSet
		simulateCmd :-
		 	retract( cmd( CMD ) ),
 			doemit( "usercmd", CMD ).

 		//Rule used as guard
 		simulate(no).
 	}

	Plan init normal
	 	println("> Plan INIT")  ;
		solve loadTheory("prolog-files/applTheory.pl") time(0) onFailSwitchTo prologFailure;
 		[ !? simulate(yes) ] switchToPlan simulate

	Plan simulate
		println(">> Plan Simulate")  ;
		delay time(1000);
		println(">> solve simulateCmd -> doemit usercmd ")  ;
	 	solve simulateCmd time(0) onFailSwitchTo endSimulateCmd ;
	 	delay time(1000) ;
	 	repeatPlan 0

	Plan endSimulateCmd
		println(">>> Plan endSimulateCmd" ) ;
		println(">>> Waiting a bit before TEST if robot react to arbitrary events") ;
		delay time(3000) ;
		println(">>> EMIT EVENT") ;
		emit usercmd : usercmd(stop)

	Plan prologFailure
    println(">> Plan prologFailure" )

}

Robot nano0 QActor legomoto context ctxRobotModel {
    Rules{
    	internalPathaf("autonomous.pl").
    	externalPathaf("prolog-files/autonomous.pl").
    }

    Plan init normal
	 	println("> Plan INIT")  ;
        solve consult("prolog-files/applTheory.pl") time(3000) onFailSwitchTo prologFailure;
        println("> solve consult applTheory ended without fail")  ;
        switchToPlan select

    Plan select
        println(">> Plan select") ;
        println(">> waiting .. (sense time(30000) usercmd) ")  ;
        sense time(30000) usercmd -> continue ;
        printCurrentEvent;
        println(">> (^--) memoCurrentEvent ")  ;
        memoCurrentEvent;
        [ ?? msg(usercmd, EVENT, WSOCK, NONE, usercmd(robotgui(l(Learning))), TIME) ] switchToPlan learning ;
        [ !? msg(usercmd, EVENT, WSOCK, NONE, usercmd(robotgui(b(Auto))), TIME) ] switchToPlan autonomous ;
        [ !? msg(usercmd, EVENT, WSOCK, NONE, usercmd(robotgui(f(Auto))), TIME) ] switchToPlan autonomous


    Plan learning
        println(">>> Plan learning") ;
        println(">>> waiting .. (sense time(30000) usercmd) ") ;
        solve cleanAutoTheory time(0) onFailSwitchTo prologFailure;
        sense time(30000) usercmd -> checkCommand ;
        switchToPlan memoActionsLoop

    Plan memoActionsLoop
        println(">>>> Plan memoActionsLoop") ;
        println(">>>> solve writeCurrentEv (autonomous.pl)") ;
        solve writeCurrentEv time(0) onFailSwitchTo prologFailure;
        println(">>>> solve robotMoveFromUsercmdEvent")  ;
		solve robotMoveFromUsercmdEvent(30000, usercmd, callbackMemoAction) time(0) onFailSwitchTo prologFailure;
        repeatPlan 0

    Plan callbackMemoAction resumeLastPlan
        println(">>>>> Plan callbackMemoAction") ;
        println(">>>>> empty plan, ready for complex behavior") ;
        switchToPlan checkCommand

    Plan checkCommand resumeLastPlan
        println(">>>>> Plan checkCommand") ;
        printCurrentEvent ;
        println(">>>>> (^--) memoCurrentEvent ")  ;
        memoCurrentEvent ;
        println(">>>>> Is a terminate? If so switch to select.")  ;
        [ !? msg(usercmd, EVENT, WSOCK, NONE, usercmd(robotgui(t(Terminate))), TIME) ] switchToPlan memoTerminate;
        println(">>>>> NO, It is not.");
        println(">>>>> Exit checkCommand, resumeLastPlan")

    Plan memoTerminate
    	println(">>>>>> Plan memoTerminate") ;
    	solve writeCurrentEv time(0) onFailSwitchTo prologFailure ;
    	switchToPlan select

    Plan autonomous
        println(">>> Plan autonomous") ;
        solve setBackhomeDirection time(0) onFailSwitchTo prologFailure;
        solve fetchAutonomousSeq time(0) onFailSwitchTo  prologFailure;
        switchToPlan decodeMoves

    Plan decodeMoves
    	println(">>> Plan decodeMoves") ;
    	solve decodeMove time(0) onFailSwitchTo deltaMoves ;
        repeatPlan 0

    Plan deltaMoves
    	println(">>> Plan deltaMoves") ;
    	println(">>> All move commands decoded") ;
    	println(">>> Now setting delta...") ;
    	delay time(500);
    	solve deltaMosse time(0) onFailSwitchTo prologFailure;
        switchToPlan memoMoves

    Plan memoMoves
    	println(">>> Plan memoMoves") ;
    	solve checkEnd time(0) onFailSwitchTo checkEndFailure ;
    	[ ?? endmemo ] switchToPlan autonomousMoveLoop ;
    	solve memoMove time(0) onFailSwitchTo prologFailure ;
    	repeatPlan 0

    Plan autonomousMoveLoop
    	println(">>>> Plan autonomousMoveLoop") ;
    	solve execMove("obstacle,usercmd", "evhSensor,evhCmd") time(0) onFailSwitchTo endAutonomousPhase ;
        repeatPlan 0

    Plan evhCmd resumeLastPlan
    	println(">>>>> Plan evhCmd");
    	printCurrentEvent;
    	switchToPlan endAutonomousPhase

    Plan evhSensor resumeLastPlan
    	println(">>>>> Plan evhSensor");
    	printCurrentEvent;
    	memoCurrentEvent;
    	switchToPlan halt

	Plan endAutonomousPhase
    	println(">>> [v] Plan endAutonomousPhase") ;
    	println(">>> [v] Back to Select Plan") ;
        switchToPlan select

    Plan prologFailure
        println(">>>>> [x] Plan prologFailure") ;
        println(">>>>> [x] failed to solve a Prolog goal" )

    Plan checkEndFailure
    	println(">>>> [x] Plan checkEndFailure")

    Plan halt
    	println("[X] HALTED")

}
